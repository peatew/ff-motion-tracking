﻿using System;
using System.Drawing;
using System.Xml;
using Emgu.CV;
using Emgu.CV.Features2D;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Rug.CV;
using Rug.CV.BaseClasses;
using Rug.Host;
using Rug.Host.Loading;

namespace Tracking.EmguCV
{
    [XmlName("EmguCV.CheckboardDetector")]
    public class CheckboardDetector : ImageFeatureBase, IDebugImageSource
    {
        public bool DebuggerAttached { get; set; }

        public event DebugFrameEvent NewDebugFrame;

        public string ReferenceImage { get; set; }

        private PointF[] referencePoints = new PointF[0];

        private Emgu.CV.Matrix<double> homography;

        public override void Load(LoadContext context, XmlNode node)
        {
            base.Load(context, node);

            ReferenceImage = Helper.GetAttributeValue(node, "ReferenceImage", ReferenceImage);
        }

        public override XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "ReferenceImage", ReferenceImage);

            return base.Save(context, element);
        }

        public override void Bind(Workspace workspace)
        {
            base.Bind(workspace);

            using (Bitmap bitmap = (Bitmap)Bitmap.FromFile(ReferenceImage))
            {
                CheckImageForCalibraionPattern(bitmap, out referencePoints);
            }
        }

        public override void Unbind()
        {
            base.Unbind();

            homography?.Dispose();
            homography = null;
        }

        protected override void OnNewSourceFrame(IImageSource source, DateTime timestamp, Bitmap bitmap)
        {
            if (homography != null)
            {

                //pts = CvInvoke.PerspectiveTransform(pts, homography);
            }
            else
            {
                PointF[] cameraPoints;

                CheckImageForCalibraionPattern(bitmap, out cameraPoints);

                if (referencePoints.Length != 0 && cameraPoints.Length != 0)
                {
                    Emgu.CV.Matrix<double> homography = new Matrix<double>(3, 3);
                    //HomographyMatrix homography = new HomographyMatrix();

                    //VectorOfPointF homography = new VectorOfPointF())
                    CvInvoke.FindHomography(cameraPoints, referencePoints, homography, Emgu.CV.CvEnum.HomographyMethod.Default);

                    double[,] data = homography.Data;

                    if (IsEmpty(data) == true)
                    {
                        homography.Dispose();

                        return;
                    }

                    this.homography = homography;

                    Connection?.Reporter.PrintEmphasized("Homography found");
                }
            }
            /*
            Image<Bgr, int> sourceImage = new Image<Bgr, int>("camera.jpg");
            Image<Bgr, int> destImage = new Image<Bgr, int>(640, 480);

            float[,] sourcePoints = { { 30, 470 }, { 620, 370 }, { 610, 130 }, { 30, 1 } };
            float[,] destPoints = { { 30, 470 }, { 620, 470 }, { 610, 30 }, { 30, 1 } };
            Emgu.CV.Matrix<float> sourceMat = new Matrix<float>(srcp);
            Emgu.CV.Matrix<float> destMat = new Matrix<float>(dstp);
            Emgu.CV.Matrix<float> homogMat = new Matrix<float>(3, 3);
            Emgu.CV.Matrix<float> invertHomogMat = new Matrix<float>(3, 3);
            IntPtr maskMat = new IntPtr();

            MCvScalar s = new MCvScalar(0, 0, 0);
            // .Ptr?
            CvInvoke.cvFindHomography(sourceMat, destMat, homogMat, Emgu.CV.CvEnum.HOMOGRAPHY_METHOD.DEFAULT, 3.0, maskMat);
            CvInvoke.cvWarpPerspective(sourceImage, destImage, invertHomogMat, (int)Emgu.CV.CvEnum.INTER.CV_INTER_NN, s);
            */
        }

        private bool IsEmpty(double[,] data)
        {
            double total = 0; 

            for (int i = 0; i < data.GetLength(1); i++)
            {
                for (int j = 0; j < data.GetLength(0); j++)
                {
                    total += data[j, i]; 
                }
            }

            return total == 0; 
        }

        private void CheckImageForCalibraionPattern(Bitmap bitmap, out PointF[] points)
        {
            points = new PointF[0];

            try
            {
                //Size patternSize = new Size(4, 5);
                Size patternSize = new Size(4, 11);

                double thresholdValue = 80;

                SimpleBlobDetectorParams @params = new SimpleBlobDetectorParams() { MinArea = 0.1f, MaxArea = 10e4f };

                using (Image<Gray, Byte> image = new Image<Gray, Byte>(bitmap))
                using (Image<Gray, Byte> resized = image.Resize(bitmap.Width / 4, bitmap.Height / 4, Emgu.CV.CvEnum.Inter.Cubic))
                using (Image<Gray, Byte> threshold = resized.ThresholdBinary(new Gray(thresholdValue), new Gray(255))) //  resized.ThresholdBinary(new Gray(64), new Gray(255)))
                using (Image<Gray, Byte> inverted = resized.ThresholdBinaryInv(new Gray(255 - thresholdValue), new Gray(255)))
                using (VectorOfPoint corners = new VectorOfPoint())
                using (SimpleBlobDetector detector = new SimpleBlobDetector(@params))
                using (VectorOfPointF centers = new VectorOfPointF())
                {
                    Image<Gray, Byte> imageToUse = resized;
                    /* 
                    bool patternFound = CvInvoke.FindChessboardCorners(threshold, new Size(3, 4), corners);

                    foreach (Point point in corners.ToArray())
                    {
                        Connection?.Send(Name + "/point", (float)point.X, (float)point.Y);
                    }

                    Connection?.Send(Name + "/found", patternFound);
                    */

                    bool patternFound = CvInvoke.FindCirclesGrid(threshold, patternSize, centers, Emgu.CV.CvEnum.CalibCgType.AsymmetricGrid | Emgu.CV.CvEnum.CalibCgType.Clustering, detector); //  | Emgu.CV.CvEnum.CalibCgType.Clustering

                    if (patternFound == true)
                    {
                        imageToUse = threshold;
                        //CvInvoke.DrawChessboardCorners(circlesGridImage, patternSize, centers, found);
                    }

                    if (patternFound == false)
                    {
                        patternFound = CvInvoke.FindCirclesGrid(inverted, patternSize, centers, Emgu.CV.CvEnum.CalibCgType.AsymmetricGrid | Emgu.CV.CvEnum.CalibCgType.Clustering, detector); //  

                        if (patternFound == true)
                        {
                            imageToUse = inverted;
                        }
                    }

                    if (patternFound == true)
                    {
                        points = centers.ToArray();

                        foreach (PointF point in centers.ToArray())
                        {
                            Connection?.Send(Name + "/point", point.X, point.Y);
                        }

                        CvInvoke.DrawChessboardCorners(threshold, patternSize, centers, patternFound);
                    }

                    if (NewDebugFrame != null)
                    {
                        using (Bitmap bmp = imageToUse.Bitmap)
                        {
                            NewDebugFrame?.Invoke(this, bmp);
                        }
                    }

                    Connection?.Send(Name + "/found", patternFound);
                }
            }
            catch (Exception ex)
            {
                Connection?.Send(Name + "/error", ex.Message);
            }
        }
    }
}
