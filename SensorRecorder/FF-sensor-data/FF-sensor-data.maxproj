{
	"name" : "FF-sensor-data",
	"version" : 1,
	"creationdate" : -745696525,
	"modificationdate" : -741358413,
	"viewrect" : [ 33.0, 76.0, 309.0, 861.0 ],
	"autoorganize" : 1,
	"hideprojectwindow" : 0,
	"showdependencies" : 1,
	"autolocalize" : 0,
	"contents" : 	{
		"patchers" : 		{
			"FF-sensor-data.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"toplevel" : 1
			}

		}
,
		"data" : 		{
			"scenarios.txt" : 			{
				"kind" : "textfile",
				"local" : 1
			}

		}
,
		"other" : 		{
			"scen-6-attention-pos-x" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-6-attention-pos-y" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-5-attention-pos-x" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-13-blobs" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-13-attention-pos-y" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-13-attention-pos-x" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-11-attention-pos-x" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-11-blobs" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-11-attention-pos-y" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-5-attention-pos-y" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-1-attention-pos-x" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-1-attention-pos-y" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-2-blobs" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-2-attention-pos-x" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-2-attention-pos-y" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-3-attention-pos-x" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-3-attention-pos-y" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-3-blobs" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-4-attention-pos-x" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-4-attention-pos-y" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-4-blobs" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-5-blobs" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-6-blobs" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-7-attention-pos-x" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-7-attention-pos-y" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-7-blobs" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-8-attention-pos-y" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-8-blobs" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-8-attention-pos-x" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-9-attention-pos-x" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-9-attention-pos-y" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-9-blobs" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-10-attention-pos-x" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-10-attention-pos-y" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"scen-10-blobs" : 			{
				"kind" : "file",
				"local" : 1
			}

		}

	}
,
	"layout" : 	{

	}
,
	"searchpath" : 	{

	}
,
	"detailsvisible" : 0,
	"amxdtype" : 1633771873,
	"readonly" : 0,
	"devpathtype" : 0,
	"devpath" : "."
}
