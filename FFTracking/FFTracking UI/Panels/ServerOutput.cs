﻿using System.Windows.Forms;
using FFTracking_UI.Console;
using Rug.Cmd;

namespace FFTracking_UI.Panels
{
    public partial class ServerOutput : UserControl
    {
        public int HistoryLength
        {
            get
            {
                return consoleControl.Buffer.BufferHeight;
            }
            set
            {
                consoleControl.Buffer.BufferHeight = value;
            }
        }

        public ServerOutput()
        {
            if ((RC.Sys is NullConsole) == false)
            {
                RC.Sys = new NullConsole();
            }

            InitializeComponent();

            FormReporter.ServerInstance.Console.Console = consoleControl.Buffer;
        }
    }
}
