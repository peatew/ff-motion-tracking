﻿namespace FFTracking_UI.Panels
{
    partial class GraphEditorPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // GraphEditorPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.DoubleBuffered = true;
            this.Name = "GraphEditorPanel";
            this.Size = new System.Drawing.Size(539, 530);
            this.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.GraphEditorPanel_MouseDoubleClick);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GraphEditorPanel_MouseDown);
            this.MouseEnter += new System.EventHandler(this.GraphEditorPanel_MouseEnter);
            this.MouseLeave += new System.EventHandler(this.GraphEditorPanel_MouseLeave);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.GraphEditorPanel_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.GraphEditorPanel_MouseUp);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
