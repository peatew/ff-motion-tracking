﻿using System.Drawing;
using System.Windows.Forms;

namespace FFTracking_UI.Panels
{
    public partial class ImagePreviewPanel : UserControl
    {
        private readonly object syncObject = new object();
        private Bitmap bitmap;

        public ImagePreviewPanel()
        {
            InitializeComponent();

            SetStyle(
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.UserPaint |
                ControlStyles.DoubleBuffer,
                true);
        }

        public void CopyToImage(Bitmap bmp)
        {
            lock (syncObject)
            {
                bitmap?.Dispose();

                bitmap = AForge.Imaging.Image.Clone(bmp);
            }

            Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
            e.Graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighSpeed; 

            e.Graphics.Clear(BackColor);

            lock (syncObject)
            {                
                if (bitmap == null)
                {
                    return;
                }

                float aspect = (float)bitmap.Height / (float)bitmap.Width;

                float width = Width;
                float height = Height;

                if (width * aspect > height)
                {
                    width = height / aspect;
                }
                else
                {
                    height = width * aspect; 
                }

                float offsetX = (Width - width) * 0.5f;
                float offsetY = (Height - height) * 0.5f;

                e.Graphics.DrawImage(bitmap, offsetX, offsetY, width, height);
            }
        }
    }
}
