﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFTracking_UI.Panels
{
    struct HsbColor
    {
        public float H;
        public float S;
        public float B;
        public float A;

        public HsbColor(float h, float s, float b, float a)
        {
            H = h;
            S = s;
            B = b;
            A = a;
        }

        public HsbColor(float h, float s, float b)
        {
            H = h;
            S = s;
            B = b;
            A = 1f;
        }

        public HsbColor(Color col)
        {
            HsbColor temp = FromColor(col);

            H = temp.H;
            S = temp.S;
            B = temp.B;
            A = temp.A;
        }

        public static HsbColor FromColor(Color color)
        {
            HsbColor ret = new HsbColor(0f, 0f, 0f, (float)color.A / 255f);

            float r = (float)color.R / 255f;
            float g = (float)color.G / 255f;
            float b = (float)color.B / 255f;

            float max = Math.Max(r, Math.Max(g, b));

            if (max <= 0)
            {
                return ret;
            }

            float min = Math.Min(r, Math.Min(g, b));

            float dif = max - min;

            if (max > min)
            {
                if (g == max)
                {
                    ret.H = (b - r) / dif * 60f + 120f;
                }
                else if (b == max)
                {
                    ret.H = (r - g) / dif * 60f + 240f;
                }
                else if (b > g)
                {
                    ret.H = (g - b) / dif * 60f + 360f;
                }
                else
                {
                    ret.H = (g - b) / dif * 60f;
                }
                if (ret.H < 0)
                {
                    ret.H = ret.H + 360f;
                }
            }
            else
            {
                ret.H = 0;
            }

            ret.H *= 1f / 360f;
            ret.S = (dif / max) * 1f;
            ret.B = max;

            return ret;
        }

        public static Color ToColor(HsbColor hsbColor)
        {
            float r = hsbColor.B;
            float g = hsbColor.B;
            float b = hsbColor.B;

            if (hsbColor.S != 0)
            {
                float max = hsbColor.B;
                float dif = hsbColor.B * hsbColor.S;
                float min = hsbColor.B - dif;

                float h = hsbColor.H * 360f;

                h %= 360;
                h *= -1f;
                h %= 360;
                h *= -1f;
                h += 360f;
                h %= 360f;

                if (h < 60f)
                {
                    r = max;
                    g = h * dif / 60f + min;
                    b = min;
                }
                else if (h < 120f)
                {
                    r = -(h - 120f) * dif / 60f + min;
                    g = max;
                    b = min;
                }
                else if (h < 180f)
                {
                    r = min;
                    g = max;
                    b = (h - 120f) * dif / 60f + min;
                }
                else if (h < 240f)
                {
                    r = min;
                    g = -(h - 240f) * dif / 60f + min;
                    b = max;
                }
                else if (h < 300f)
                {
                    r = (h - 240f) * dif / 60f + min;
                    g = min;
                    b = max;
                }
                else if (h <= 360f)
                {
                    r = max;
                    g = min;
                    b = -(h - 360f) * dif / 60 + min;
                }
                else
                {
                    r = 0;
                    g = 0;
                    b = 0;
                }
            }

            return Color.FromArgb(Clamp255(hsbColor.A), Clamp255(r), Clamp255(g), Clamp255(b));
        }

        private static int Clamp255(float value)
        {
            value = Math.Max(0f, Math.Min(1f, value));

            value *= 255;

            return (int)value; 
        }

        public Color ToColor()
        {
            return ToColor(this);
        }

        public override string ToString()
        {
            return "H:" + H + " S:" + S + " B:" + B;
        }
    }
}
