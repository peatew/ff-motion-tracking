﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using FFTracking_UI.Console;
using Rug.Cmd;
using Rug.Osc;
using Helper = Rug.Host.Helper;

namespace FFTracking_UI.Panels
{
    public partial class Terminal : UserControl, IOscPacketReceiver
	{
		bool hasValidSendString = false;
		string helpText = "Type OSC message here and press 'Enter' to send."; 
		
		Font inputFont = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
		Font backupFont;

		public event OscPacketEvent PacketRecived;

		public OscCommunicationStatistics Statistics { get; set; }

		public int HistoryLength
		{
			get
			{
				return consoleControl.Buffer.BufferHeight; 
			}
			set
			{
				consoleControl.Buffer.BufferHeight = value;				
			}
		}

		public Terminal()
		{
            if ((RC.Sys is NullConsole) == false)
            {
                RC.Sys = new NullConsole();
            }

			InitializeComponent();

			RC.App = consoleControl.Buffer;
        }

		private void Terminal_Load(object sender, EventArgs e)
		{
            FormReporter.ClientInstance.Console.Console = consoleControl.Buffer; 

            backupFont = sendMessageBox.Font; 

			sendMessageBox.Text = helpText;
            sendMessageBox.DropDown += SendMessageBox_DropDown;
            sendMessageBox.DropDownClosed += SendMessageBox_DropDownClosed;

			this.AllowDrop = true;
			this.DragEnter += new DragEventHandler(Terminal_DragEnter);
			this.DragDrop += new DragEventHandler(Terminal_DragDrop);
		}

		private void Send()
		{
			if (hasValidSendString == false)
			{
				return;
			}

			string oscString = sendMessageBox.Text.Trim();

			int selectionStart = sendMessageBox.SelectionStart;
			int selectionLength = sendMessageBox.SelectionLength; 

			if (sendMessageBox.Items.Contains(oscString) == true)
			{
				sendMessageBox.Items.Remove(oscString);
			}

			sendMessageBox.Items.Insert(0, oscString);
			
			sendMessageBox.SelectedIndex = 0;

			sendMessageBox.SelectionStart = selectionStart;
			sendMessageBox.SelectionLength = selectionLength; 

			if (PacketRecived == null)
			{
				return;			
			}

			if (File.Exists(Helper.ResolvePath(oscString)) == true)
			{
				using (OscFileReader reader = new OscFileReader(Helper.ResolvePath(oscString), OscPacketFormat.String))
				{
					while (reader.EndOfStream == false)
					{
                        OscPacket packet = reader.Read();

                        if (packet.Error != OscPacketError.None)
                        {
                            FormReporter.ClientInstance.PrintError(packet.ErrorMessage);

                            continue; 
                        }
                       
                        PacketRecived(packet);
					}
				}
			}
			else
			{
				PacketRecived(OscPacket.Parse(oscString));
			}
		}

		private void SendMessageBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			
		}

		private void SendMessageBox_TextChanged(object sender, EventArgs e)
		{
			if (sendMessageBox.Text == helpText)
			{
				sendMessageBox.Font = backupFont;
				sendMessageBox.ForeColor = Color.LightGray;
				
				hasValidSendString = false; 

				return; 
			}

			sendMessageBox.Font = inputFont;

			if (File.Exists(Helper.ResolvePath(sendMessageBox.Text)) == true)
			{
				hasValidSendString = true; 

				sendMessageBox.ForeColor = Control.DefaultForeColor;
			}
			else
			{
				OscPacket packet;

				hasValidSendString = OscPacket.TryParse(sendMessageBox.Text.Trim(), out packet);

				sendMessageBox.ForeColor = hasValidSendString ? Control.DefaultForeColor : Color.Red;
			}
		}

		private void SendMessageBox_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter || 
				e.KeyCode == Keys.Return)
			{
				if (hasValidSendString == false)
				{
					return; 
				}

				Send();

				e.SuppressKeyPress = true;
			}
		}

		private void SendMessageBox_Enter(object sender, EventArgs e)
		{
			sendMessageBox.AutoCompleteMode = AutoCompleteMode.None; 

			if (sendMessageBox.Text == helpText)
			{
				sendMessageBox.Text = ""; 
			}
		}

		private void SendMessageBox_Leave(object sender, EventArgs e)
		{
			sendMessageBox.AutoCompleteMode = AutoCompleteMode.None; 

			if (Helper.IsNullOrEmpty(sendMessageBox.Text) == true)
			{
				sendMessageBox.Text = helpText;
			}
		}

		public void Clear()
		{
			consoleControl.Clear(); 
		}

		void Terminal_DragEnter(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.FileDrop))
			{
				e.Effect = DragDropEffects.Copy;
			}
		}

		void Terminal_DragDrop(object sender, DragEventArgs e)
		{
			string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

			foreach (string file in files)
			{
                sendMessageBox.Text = file;

                Send(); 
			}
		}

        void SendMessageBox_DropDown(object sender, EventArgs e)
        {
            sendMessageBox.Font = inputFont;
            sendMessageBox.ForeColor = Control.DefaultForeColor;
        }

        void SendMessageBox_DropDownClosed(object sender, EventArgs e)
        {
            SendMessageBox_TextChanged(sender, e);
        }
	}
}
