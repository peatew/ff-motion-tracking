﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using FFTracking_UI.Console;
using Rug.Cmd;
using Rug.Host;
using Rug.Osc;

namespace FFTracking_UI.Panels
{
    public partial class FilterPanel : UserControl, IOscMessageFilter
    {
        private List<string> muted = new List<string>();
		private List<string> soloed = new List<string>();
		
        private CheckBox muteAll = new CheckBox();
        private CheckBox soloAll = new CheckBox();
        
		private Dictionary<string, AddressInstance> addressLookup = new Dictionary<string, AddressInstance>();

		private ManualResetEvent addNewAddress = new ManualResetEvent(false);
		private DateTime lastUpdate;
 
		public FilterPanel()
		{
			InitializeComponent();

            FormReporter.ClientInstance.OscMessageFilter = this; 
		}
		
		#region Load Event

		private void FilterPanel_Load(object sender, EventArgs e)
		{
			lastUpdate = DateTime.Now;

            SetupCheckBox(muteAll, "Mute");
            SetupCheckBox(soloAll, "Solo"); 

            m_DataView_ColumnWidthChanged(this, null);

            muteAll.CheckedChanged += MuteAll_CheckedChanged;
            soloAll.CheckedChanged += SoloAll_CheckedChanged;
            
            dataView.ColumnWidthChanged += m_DataView_ColumnWidthChanged;

            // Add the CheckBox into the DataGridView
            dataView.Controls.Add(muteAll);
            dataView.Controls.Add(soloAll);

            DoubleBuffered = true; 
		}

        private void SetupCheckBox(CheckBox checkBox, string name)
        {
            checkBox.AutoEllipsis = true;
            checkBox.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            checkBox.Location = new System.Drawing.Point(299, 195);
            checkBox.Name = name;
            checkBox.Size = new System.Drawing.Size(85, 19);
            checkBox.TabIndex = 1;
            checkBox.Text = name;
            checkBox.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            //checkBox.AutoCheck = false; 
            //checkBox.UseVisualStyleBackColor = true;
        }

        internal void InvalidateAndAlign()
        {
            //MuteAll.Visible = false;
            //SoloAll.Visible = false; 

            Invalidate(true);

            m_DataView_ColumnWidthChanged(this, null);
        }

        void m_DataView_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            AlignCheckBox(muteAll, 0);
            AlignCheckBox(soloAll, 1);
        }

        private void AlignCheckBox(CheckBox checkBox, int column)
        {
            // Get the column header cell bounds
            Rectangle rect = this.dataView.GetCellDisplayRectangle(column, -1, true);

            checkBox.Size = new Size(Math.Min(rect.Width - 10, 50), 18); //);
            checkBox.Padding = new System.Windows.Forms.Padding(0);
            checkBox.Margin = new System.Windows.Forms.Padding(0);

            // Change the location of the CheckBox to make it stay on the header
            checkBox.Location = new Point(
                rect.X + 2, //((rect.Width - checkBox.Width) / 2) + rect.X + 2,
                ((rect.Height - checkBox.Height) / 2) + rect.Y + 1
                );

            checkBox.UseVisualStyleBackColor = false;
            checkBox.BackColor = Color.Transparent;

            checkBox.Invalidate();
        }

        void MuteAll_CheckedChanged(object sender, EventArgs e)
        {
            List<string> allAddresses = new List<string>(addressLookup.Keys); 

            foreach (string address in allAddresses)
            {
                SetMute(address, muteAll.Checked);
            }
        }

        void SoloAll_CheckedChanged(object sender, EventArgs e)
        {
            List<string> allAddresses = new List<string>(addressLookup.Keys);

            foreach (string address in allAddresses)
            {
                SetSolo(address, soloAll.Checked);
            }
        }

		#endregion

		public void CalculateRate()
		{
			DateTime date = DateTime.Now;

			TimeSpan span = date - lastUpdate;

			foreach (AddressInstance instance in addressLookup.Values)
			{
				instance.CalculateRate(span);
			}

			lastUpdate = date; 
		}

		#region Add Filter

		public void AddFilter(OscMessage message)
		{
			try
			{
				AddressInstance instance;

				if (addressLookup.TryGetValue(message.Address, out instance) == true)
				{
					instance.OnMessage(message);
					return;
				}

				if (this.InvokeRequired == true)
				{
					addNewAddress.Reset();

					this.BeginInvoke(new Action<string>(AddFilter), message.Address);

					//addNewAddress.WaitOne(); 
				}
				else
				{
					AddFilter(message.Address);
				}
			}
			catch (Exception ex)
			{
				// explicitly tell the user why the address failed to parse
				RC.WriteException(001, "Error parsing osc message", ex);
			}
		}

		public void AddFilter(string address)
		{
			try
			{
				AddressInstance instance;
				if (addressLookup.TryGetValue(address, out instance) == true)
				{
					return;
				}

                DataGridViewRow row = dataView.Rows[dataView.Rows.Add(false, false, address, 0L, 0d)];

				instance = new AddressInstance(address, row);

				addressLookup.Add(address, instance);

                // set initial mute solo state
                SetMute(address, false);
                SetSolo(address, false); 
            }
			catch (Exception ex)
			{
				// explicitly tell the user why the address failed to parse
				RC.WriteException(002, "Error parsing osc address", ex);
			}
			finally
			{
				addNewAddress.Set(); 
			}
		}

		#endregion

		#region Data Grid Events

		private void m_DataView_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
			try
			{
				if (e.RowIndex == -1)
				{
					return; 
				}

				string address = dataView.Rows[e.RowIndex].Cells["Address"].Value.ToString();

				DataGridViewColumn column = dataView.Columns[e.ColumnIndex];

				if (column.Name == "Mute")
				{
					ToggleMute(address);
				}
				else if (column.Name == "Solo")
				{
					ToggleSolo(address);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}


		private void SetSolo(string address, bool value)
		{
			try
			{
				AddressInstance instance = addressLookup[address];

				instance.Solo = value;

				if (instance.Solo == true)
				{
					soloed.Add(address);
				}
				else
				{
					soloed.Remove(address);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}


		private void ToggleSolo(string address)
		{
			try
			{
				AddressInstance instance = addressLookup[address];

				instance.Solo = !instance.Solo;

				if (instance.Solo == true)
				{
					soloed.Add(address);
				}
				else
				{
					soloed.Remove(address);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void SetMute(string address, bool value)
		{
			try
			{
				AddressInstance instance = addressLookup[address];

				instance.Mute = value;

				if (instance.Mute == true)
				{
					muted.Add(address);
				}
				else
				{
					muted.Remove(address);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void ToggleMute(string address)
		{
			try
			{
				AddressInstance instance = addressLookup[address];

				instance.Mute = !instance.Mute;

				if (instance.Mute == true)
				{
					muted.Add(address);
				}
				else
				{
					muted.Remove(address);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void DataView_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
		{
			try
			{
				string address = e.Row.Cells["Address"].Value.ToString();

				Delete(address);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void Delete(string address)
		{
			try
			{
				AddressInstance instance = addressLookup[address];

				muted.Remove(address);
				soloed.Remove(address);

				addressLookup.Remove(address);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		#endregion

		internal void Clear()
		{
			dataView.Rows.Clear();

			List<string> allAddresses = new List<string>(addressLookup.Keys);

			foreach (string address in allAddresses)
			{
				Delete(address);
			}
		}

		internal bool ShouldWriteMessage(string address)
		{
			if (soloed.Count > 0)
			{
				return soloed.Contains(address);
			}
			else if (muted.Count > 0 && muted.Contains(address) == true)
			{
				return false;
			}
			else
			{
				return true; 
			}
		}

        public bool ShouldPrintMessage(OscMessage message)
        {
            AddFilter(message);

            return ShouldWriteMessage(message.Address); 
        }
    }
}
