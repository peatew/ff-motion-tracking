﻿namespace FFTracking_UI.Panels
{
    partial class ServerOutput
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.consoleControl = new Rug.Forms.Console.ConsoleControl();
            this.SuspendLayout();
            // 
            // consoleControl
            // 
            this.consoleControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.consoleControl.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.consoleControl.Input = null;
            this.consoleControl.Location = new System.Drawing.Point(0, 0);
            this.consoleControl.Name = "consoleControl";
            this.consoleControl.Size = new System.Drawing.Size(530, 395);
            this.consoleControl.TabIndex = 1;
            this.consoleControl.Text = "consoleControl";
            this.consoleControl.UpdateInterval = 10;
            this.consoleControl.WordWrap = false;
            // 
            // ServerOutput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.consoleControl);
            this.Name = "ServerOutput";
            this.Size = new System.Drawing.Size(530, 395);
            this.ResumeLayout(false);

        }

        #endregion

        private Rug.Forms.Console.ConsoleControl consoleControl;
    }
}
