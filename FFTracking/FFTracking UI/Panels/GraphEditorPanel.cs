﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using Rug.Host;
using Rug.Host.Commands;
using Rug.Host.Loading;

namespace FFTracking_UI.Panels
{
    public partial class GraphEditorPanel : UserControl
    {
        private enum EditorObjectPinType
        {
            None,
            Object,
            Property,
            Binding,
        }

        private enum InputState
        {
            None,
            Panning,
            AreaSelecting,
            Dragging,
            PinHighlight,
        }

        private readonly List<Binding> bindings = new List<Binding>();
        private readonly List<EditorObjectPin> hoverables = new List<EditorObjectPin>();
        private readonly Dictionary<string, EditorObjectLayout> objects = new Dictionary<string, EditorObjectLayout>();
        private readonly List<EditorObjectLayout> selectedObjects = new List<EditorObjectLayout>();
        private readonly Dictionary<string, ThreadStyle> threadStyles = new Dictionary<string, ThreadStyle>();

        class ThreadStyle
        {
            public class State
            {
                public Pen Outline;
                public Brush Fill;
            }

            public State Unselected;

            public State Selected;

            public Pen ConnectionPen;

            public ThreadStyle Clone()
            {
                ThreadStyle style = new ThreadStyle();

                style.Unselected = new State()
                {
                    Outline = (Pen)this.Unselected.Outline.Clone(),
                    Fill = (Brush)this.Unselected.Fill.Clone(),
                };

                style.Selected = new State()
                {
                    Outline = (Pen)this.Selected.Outline.Clone(),
                    Fill = (Brush)this.Selected.Fill.Clone(),
                };

                style.ConnectionPen = (Pen)this.ConnectionPen.Clone();

                return style; 
            }

            public void Colorise(Color color)
            {
                SetPenColor(Unselected.Outline, color);
                SetBrushColor(ref Unselected.Fill, color);

                SetPenColor(Selected.Outline, color);
                SetBrushColor(ref Selected.Fill, color);

                SetPenColor(ConnectionPen, color);
            }

            private void SetBrushColor(ref Brush fill, Color color)
            {
                if (fill is SolidBrush && IsBlack((fill as SolidBrush).Color) == false) (fill as SolidBrush).Color = color;
                if (fill is HatchBrush && IsBlack((fill as HatchBrush).ForegroundColor) == false)
                {
                    HatchBrush hatchBrush = fill as HatchBrush;

                    HatchBrush newBrush = new HatchBrush(hatchBrush.HatchStyle, color);

                    hatchBrush.Dispose();

                    fill = newBrush; 
                }
            }

            private bool IsBlack(Color color)
            {
                if (color == Color.Black)
                {
                    return true; 
                }

                if (color.R == 0 && color.G == 0 && color.B == 0)
                {
                    return true; 
                }

                return false; 
            }

            private void SetPenColor(Pen outline, Color color)
            {
                if (IsBlack(outline.Color) == false) outline.Color = color;
           }
        }

        private Pen connectionPen1 = new Pen(Color.Black) { Width = 1.5f };
        private Pen connectionPen2 = new Pen(Color.White) { Width = 2, DashPattern = new float[] { 2f, 2f } };
        private Brush hoveringFillBrush = new SolidBrush(Color.White);
        private Pen hoveringOutlinePen = new Pen(Color.Black) { Width = 1, DashPattern = new float[] { 2f, 2f } };
        private EditorObjectPin hoveringPin;
        private Brush hoveringTextBrush = new SolidBrush(Color.Black);
        private InputState inputState = InputState.None;
        private PointF mouseLocationInWorkspace;
        private Point panStart;
        private PointF panStartViewOffset;
        private bool recacheWorkspace = true;
        private PointF selectedObjectOffset;

        private Styles selectedStyles = new Styles()
        {
            BoxFill = new SolidBrush(Color.White),
            BoxOutline = new Pen(Color.White) { Width = 1 },

            PinFill = new HatchBrush(HatchStyle.Percent50, Color.White),
            PinOutline = new Pen(Color.Black) { Width = 1 },

            PinHighlightFill = new SolidBrush(Color.White),
            PinHighlightOutline = new Pen(Color.Black) { Width = 1 },

            LabelFill = new SolidBrush(Color.White),
            LabelOutline = new Pen(Color.Black) { Width = 1, DashPattern = new float[] { 1f, 1f } },
            LabelText = new SolidBrush(Color.Black),
        };

        private RectangleF selectionRectangle;
        private Brush selectionRectangleFill = new HatchBrush(HatchStyle.Percent05, Color.White);
        private Pen selectionRectangleOutline = new Pen(Color.White) { Width = 1, DashPattern = new float[] { 4f, 4f } };
        private PointF selectionStart;

        private StringFormat stringFormat = new StringFormat()
        {
            Alignment = StringAlignment.Near,
            LineAlignment = StringAlignment.Center,
        };

        private RectangleF totalBounds;
        private Pen totalBoundsOutline = new Pen(Color.White) { Width = 2, DashPattern = new float[] { 2f, 2f } };

        private Styles unselectedStyles = new Styles()
        {
            BoxFill = new SolidBrush(Color.Black),
            BoxOutline = new Pen(Color.White) { Width = 1 }, // , DashPattern = new float[] { 2f, 2f } },

            PinFill = new HatchBrush(HatchStyle.Percent50, Color.White),
            PinOutline = new Pen(Color.White) { Width = 1 },

            PinHighlightFill = new SolidBrush(Color.Black),
            PinHighlightOutline = new Pen(Color.White) { Width = 1 },

            LabelFill = new SolidBrush(Color.Black),
            LabelOutline = new Pen(Color.White) { Width = 1, DashPattern = new float[] { 1f, 1f } },
            LabelText = new SolidBrush(Color.White),
        };

        private PointF viewOffset = new PointF(0, 0);
        private float viewScale = 1f;
        private ThreadStyle nullThreadStyle;

        [Browsable(false)]
        public Workspace Workspace { get; set; }

        public GraphEditorPanel()
        {
            nullThreadStyle = new ThreadStyle()
            {
                Unselected = new ThreadStyle.State()
                {
                    Fill = unselectedStyles.BoxFill,
                    Outline = unselectedStyles.BoxOutline,
                },

                Selected = new ThreadStyle.State()
                {
                    Fill = selectedStyles.BoxFill,
                    Outline = selectedStyles.BoxOutline,
                },

                ConnectionPen = connectionPen2,
            };

            InitializeComponent();

            MouseWheel += GraphEditorPanel_MouseWheel;
        }

        public void Rebuild()
        {
            recacheWorkspace = true;

            Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            if (Workspace == null)
            {
                return;
            }

            float hueInc = 1f / (float)threadStyles.Count;

            HsbColor hsb = new HsbColor(Color.FromArgb(255, 0, 255, 255));
            float hue = hsb.H;

            foreach (ThreadStyle style in threadStyles.Values)
            {
                hsb.H = hue;

                style.Colorise(hsb.ToColor()); 

                hue += hueInc;

                hue %= 1f; 
            }

            Graphics g = e.Graphics;

            SmoothingMode smoothingMode = viewScale < 1f ? SmoothingMode.AntiAlias : SmoothingMode.None;

            g.SmoothingMode = smoothingMode;

            if (recacheWorkspace == true)
            {
                CacheWorkspace();
            }

            g.ResetTransform();

            g.TranslateTransform(viewOffset.X, viewOffset.Y);
            g.ScaleTransform(viewScale, viewScale);

            if (selectionRectangle != RectangleF.Empty)
            {
                g.FillRectangle(selectionRectangleFill, selectionRectangle);
            }

            foreach (EditorObjectLayout @object in objects.Values)
            {
                if (selectedObjects.Contains(@object) == true)
                {
                    continue;
                }

                RenderObject(g, stringFormat, @object, @object.Selected);
            }

            foreach (EditorObjectLayout @object in selectedObjects)
            {
                RenderObject(g, stringFormat, @object, true);
            }

            g.SmoothingMode = SmoothingMode.AntiAlias;

            foreach (Binding binding in bindings)
            {
                DrawConnection(g, binding);
            }

            g.SmoothingMode = smoothingMode;

            if (selectionRectangle != RectangleF.Empty)
            {
                g.DrawRectangle(selectionRectangleOutline, selectionRectangle.X, selectionRectangle.Y, selectionRectangle.Width, selectionRectangle.Height);
            }

            g.DrawRectangle(totalBoundsOutline, totalBounds.X, totalBounds.Y, totalBounds.Width, totalBounds.Height);

            if (hoveringPin != null)
            {
                string text = hoveringPin.GetText();

                SizeF size = g.MeasureString(text, Font, Width);

                RectangleF tooltipBounds = new RectangleF(hoveringPin.Bounds.Right + 2, hoveringPin.Bounds.Y - 22, size.Width, 20);

                g.FillRectangle(hoveringFillBrush, tooltipBounds);
                g.DrawRectangle(hoveringOutlinePen, tooltipBounds.X, tooltipBounds.Y, tooltipBounds.Width, tooltipBounds.Height);
                g.DrawString(text, Font, hoveringTextBrush, tooltipBounds, stringFormat);
            }
        }

        private void CacheMembers(EditorObjectLayout editorObjectLayout)
        {
            List<IOscObjectBinding> oscObjectBindings = new List<IOscObjectBinding>();
            List<IOscProperty> oscProperties = new List<IOscProperty>();
            List<IOscObjectList> oscObjectLists = new List<IOscObjectList>();

            foreach (IOscMember member in editorObjectLayout.Object.OscMembers)
            {
                if (member is IOscObjectList)
                {
                    oscObjectLists.Add(member as IOscObjectList);
                }
                else if (member is IOscProperty)
                {
                    oscProperties.Add(member as IOscProperty);
                }
                else if (member is IOscObjectBinding)
                {
                    oscObjectBindings.Add(member as IOscObjectBinding);
                }
            }

            editorObjectLayout.BindingPins.Clear();

            foreach (IOscObjectBinding oscObjectBinding in oscObjectBindings)
            {
                EditorObjectPin pin = new EditorObjectPin();

                pin.ThreadName = editorObjectLayout.ThreadName; 
                pin.PinType = EditorObjectPinType.Binding;
                pin.PinObjectType = oscObjectBinding.BindingType;
                pin.Member = oscObjectBinding;

                editorObjectLayout.BindingPins.Add(oscObjectBinding.MemberName, pin);

                if (oscObjectBinding.IsBound == true)
                {
                    bindings.Add(new Binding() { From = pin, ToAddress = oscObjectBinding.BindToName });
                }

                hoverables.Add(pin);
            }

            editorObjectLayout.PropertyPins.Clear();

            foreach (IOscProperty oscProperty in oscProperties)
            {
                EditorObjectPin pin = new EditorObjectPin();

                pin.PinType = EditorObjectPinType.Property;
                pin.PinObjectType = oscProperty.PropertyType;
                pin.Member = oscProperty;

                editorObjectLayout.PropertyPins.Add(oscProperty.MemberName, pin);

                hoverables.Add(pin);
            }

            foreach (IOscObjectList oscObjectList in oscObjectLists)
            {
                List<EditorObjectLayout> nestedList;

                if (editorObjectLayout.NestedObjects.TryGetValue(oscObjectList.MemberName, out nestedList) == false)
                {
                    nestedList = new List<EditorObjectLayout>();
                    editorObjectLayout.NestedObjects.Add(oscObjectList.MemberName, nestedList);
                }

                nestedList.Clear();

                foreach (IObject @object in oscObjectList)
                {
                    EditorObjectLayout nestedObject = new EditorObjectLayout()
                    {
                        Address = @object.Name,
                        Object = @object,
                        Touched = true,
                    };


                    nestedObject.ThreadName = @object.ThreadName;
                    nestedObject.ObjectConnectionPin.PinObjectType = @object.GetType();
                    nestedObject.ObjectConnectionPin.OscAddress = @object.Name;
                    nestedObject.ObjectConnectionPin.ThreadName = @object.ThreadName;

                    nestedList.Add(nestedObject);

                    CacheMembers(nestedObject);

                    hoverables.Add(nestedObject.ObjectConnectionPin);
                }
            }
        }

        private void CacheWorkspace()
        {
            recacheWorkspace = false;
            hoverables.Clear();
            bindings.Clear();
            hoveringPin = null;

            foreach (EditorObjectLayout editorObjectLayout in objects.Values)
            {
                editorObjectLayout.Touched = false;
            }

            foreach (IObject @object in Workspace.Objects)
            {
                EditorObjectLayout editorObjectLayout;

                if (objects.TryGetValue(@object.Name, out editorObjectLayout) == true)
                {
                    editorObjectLayout.Object = @object;
                    editorObjectLayout.Touched = true;
                }
                else
                {
                    editorObjectLayout = new EditorObjectLayout()
                    {
                        Address = @object.Name,
                        Object = @object,
                        Touched = true,
                    };

                    objects.Add(editorObjectLayout.Address, editorObjectLayout);
                }

                editorObjectLayout.ThreadName = @object.ThreadName; 
                editorObjectLayout.ObjectConnectionPin.PinObjectType = @object.GetType();
                editorObjectLayout.ObjectConnectionPin.OscAddress = @object.Name;
                editorObjectLayout.ObjectConnectionPin.ThreadName = @object.ThreadName;

                hoverables.Add(editorObjectLayout.ObjectConnectionPin);

                CacheMembers(editorObjectLayout);
            }

            List<EditorObjectLayout> allObjectsCache = new List<EditorObjectLayout>(objects.Values);

            foreach (EditorObjectLayout editorObjectLayout in allObjectsCache)
            {
                if (editorObjectLayout.Touched == true)
                {
                    continue;
                }

                objects.Remove(editorObjectLayout.Address);
            }

            FindBindings();

            LayoutObjects();
        }

        private PointF CenterOf(RectangleF rectangle)
        {
            return new PointF(rectangle.Width * 0.5f + rectangle.X, rectangle.Height * 0.5f + rectangle.Y);
        }

        private void DrawConnection(Graphics g, Binding binding)
        {
            if (binding.To == null)
            {
                return;
            }

            ThreadStyle threadStyle;
            
            if (string.IsNullOrEmpty(binding.ThreadName) == true)
            {
                threadStyle = nullThreadStyle;
            }
            else
            {
                threadStyle = GetThreadStyle(binding.ThreadName);
            }

            PointF node1 = binding.To.Center;
            PointF node2 = binding.From.Center;
            PointF p1, p2, c1, c2;

            p1 = new PointF(node1.X, node1.Y);
            p2 = new PointF(node2.X, node2.Y);

            float distanceX = ((p2.X - p1.X) * 0.5f);
            float distanceY;

            float curveFactor = Math.Abs(p2.Y - p1.Y);

            if (distanceX > 0)
            {
                distanceY = 0;
            }
            else
            {
                curveFactor = 100;

                distanceY = p2.Y - p1.Y;

                if (Math.Abs(distanceY) > 100)
                {
                    distanceY *= 0.5f;
                }
            }

            distanceX = Math.Max(Math.Min(curveFactor, 100), distanceX);

            c1 = new PointF(node1.X + distanceX, node1.Y + distanceY);
            c2 = new PointF(node2.X - distanceX, node2.Y - distanceY);

            g.DrawBezier(connectionPen1, p1, c1, c2, p2);
            g.DrawBezier(threadStyle.ConnectionPen, p1, c1, c2, p2);
        }

        private void FindBindings()
        {
            foreach (Binding binding in bindings)
            {
                EditorObjectLayout editorObjectLayout;

                if (objects.TryGetValue(binding.ToAddress, out editorObjectLayout) == false)
                {
                    continue;
                }

                binding.ThreadName = editorObjectLayout.ObjectConnectionPin.ThreadName; 
                binding.To = editorObjectLayout.ObjectConnectionPin;
            }
        }

        private bool FindHoverable(PointF point, out EditorObjectPin found)
        {
            found = null;

            foreach (EditorObjectPin pin in hoverables)
            {
                RectangleF bounds = pin.Bounds;

                if (bounds.Contains(point) == false)
                {
                    continue;
                }

                found = pin;

                return true;
            }

            return false;
        }

        private bool FindObject(PointF point, out EditorObjectLayout found, out PointF offset)
        {
            found = null;
            offset = default(PointF);

            foreach (EditorObjectLayout @object in objects.Values)
            {
                RectangleF bounds = @object.Bounds;

                if (bounds.Contains(point) == false)
                {
                    continue;
                }

                offset = new PointF(bounds.X - (float)point.X, bounds.Y - (float)point.Y);
                found = @object;

                return true;
            }

            return false;
        }

        private EditorObjectLayout[] FindObjects(RectangleF rectangle)
        {
            List<EditorObjectLayout> foundObjects = new List<EditorObjectLayout>();

            foreach (EditorObjectLayout @object in objects.Values)
            {
                RectangleF bounds = @object.Bounds;

                if (rectangle.Contains(bounds) == false)
                {
                    continue;
                }

                foundObjects.Add(@object);
            }

            return foundObjects.ToArray();
        }

        private void GraphEditorPanel_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            PointF center = CenterOf(totalBounds);

            viewScale = 1f;
            PointF screenOffset = new PointF(center.X - (Width * 0.5f), center.Y - (Height * 0.5f));

            viewOffset = new PointF(-screenOffset.X, -screenOffset.Y);

            recacheWorkspace = true;

            Invalidate();
        }

        private void GraphEditorPanel_MouseDown(object sender, MouseEventArgs e)
        {
            mouseLocationInWorkspace = ScreenSpaceToWorkspace(e.Location);

            if (e.Button == MouseButtons.Right)
            {
                if (Control.ModifierKeys != Keys.Shift &&
                    Control.ModifierKeys != Keys.Control)
                {
                    selectedObjects.Clear();
                }

                selectionStart = mouseLocationInWorkspace;
                inputState = InputState.AreaSelecting;
            }
            else
            {
                EditorObjectLayout found;
                PointF offset;

                if (FindObject(mouseLocationInWorkspace, out found, out offset) == false)
                {
                    inputState = InputState.Panning;
                    panStart = e.Location;
                    panStartViewOffset = viewOffset;

                    if (Control.ModifierKeys != Keys.Shift &&
                        Control.ModifierKeys != Keys.Control)
                    {
                        selectedObjects.Clear();
                    }
                }
                else
                {
                    inputState = InputState.Dragging;

                    if (Control.ModifierKeys != Keys.Shift &&
                        Control.ModifierKeys != Keys.Control)
                    {
                        selectedObjects.Clear();
                    }

                    selectedObjects.Remove(found);

                    if (Control.ModifierKeys != Keys.Control)
                    {
                        selectedObjects.Add(found);
                    }

                    selectedObjectOffset = offset;
                }
            }

            Invalidate();
        }

        private void GraphEditorPanel_MouseEnter(object sender, EventArgs e)
        {
        }

        private void GraphEditorPanel_MouseLeave(object sender, EventArgs e)
        {
        }

        private void GraphEditorPanel_MouseMove(object sender, MouseEventArgs e)
        {
            mouseLocationInWorkspace = ScreenSpaceToWorkspace(e.Location);

            if (inputState == InputState.Panning)
            {
                viewOffset.X = panStartViewOffset.X + (e.Location.X - panStart.X);
                viewOffset.Y = panStartViewOffset.Y + (e.Location.Y - panStart.Y);
                //panStart = e.Location;

                Invalidate();
            }
            else if (inputState == InputState.Dragging)
            {
                PointF initialPosition = selectedObjects[selectedObjects.Count - 1].Bounds.Location;
                PointF finalPosition = initialPosition;

                finalPosition.X = (int)((mouseLocationInWorkspace.X + selectedObjectOffset.X) / 10) * 10;
                finalPosition.Y = (int)((mouseLocationInWorkspace.Y + selectedObjectOffset.Y) / 10) * 10;

                PointF offset = new PointF(finalPosition.X - initialPosition.X, finalPosition.Y - initialPosition.Y);

                foreach (EditorObjectLayout @object in selectedObjects)
                {
                    RectangleF bounds = @object.Bounds;

                    bounds.Offset(offset);

                    @object.Bounds = bounds;
                    @object.Object.Bounds.Value = bounds;
                }

                recacheWorkspace = true;

                Invalidate();
            }
            else if (inputState == InputState.AreaSelecting)
            {
                float minX, maxX, minY, maxY;

                minX = Math.Min(selectionStart.X, mouseLocationInWorkspace.X);
                maxX = Math.Max(selectionStart.X, mouseLocationInWorkspace.X);

                minY = Math.Min(selectionStart.Y, mouseLocationInWorkspace.Y);
                maxY = Math.Max(selectionStart.Y, mouseLocationInWorkspace.Y);

                selectionRectangle = new RectangleF(minX, minY, maxX - minX, maxY - minY);

                foreach (EditorObjectLayout @object in objects.Values)
                {
                    @object.Selected = false;
                }

                foreach (EditorObjectLayout @object in FindObjects(selectionRectangle))
                {
                    @object.Selected = true;
                }

                Invalidate();
            }
            else
            {
                EditorObjectPin selectedPin;

                if (FindHoverable(mouseLocationInWorkspace, out selectedPin) == false)
                {
                    if (hoveringPin == null)
                    {
                        return;
                    }

                    UnhighlightPins();

                    return;
                }

                HighlightPins(selectedPin);
            }
        }

        private void GraphEditorPanel_MouseUp(object sender, MouseEventArgs e)
        {
            mouseLocationInWorkspace = ScreenSpaceToWorkspace(e.Location);

            if (inputState == InputState.AreaSelecting)
            {
                if (Control.ModifierKeys != Keys.Shift &&
                    Control.ModifierKeys != Keys.Control)
                {
                    selectedObjects.Clear();
                }

                foreach (EditorObjectLayout @object in objects.Values)
                {
                    if (@object.Selected == true)
                    {
                        selectedObjects.Remove(@object);

                        if (Control.ModifierKeys != Keys.Control)
                        {
                            selectedObjects.Add(@object);
                        }
                    }

                    @object.Selected = false;
                }
            }

            inputState = InputState.None;

            selectionRectangle = RectangleF.Empty;

            Invalidate();
        }

        private void GraphEditorPanel_MouseWheel(object sender, MouseEventArgs e)
        {
            //PointF initialWorkspaceCenter = ScreenSpaceToWorkspace(new PointF(Width * 0.5f, Height * 0.5f)); 

            viewScale += e.Delta > 0 ? 0.125f : -0.125f;
            viewScale = Math.Min(2, Math.Max(0.125f, viewScale));

            //PointF workspaceCenter = ScreenSpaceToWorkspace(new Point(Width / 2, Height / 2));

            //viewOffset.X = initialWorkspaceCenter.X + (((float)Width * 0.5f) / viewScale);
            //viewOffset.Y = initialWorkspaceCenter.Y + (((float)Height * 0.5f) / viewScale);

            //PointF workspaceCenter = ScreenSpaceToWorkspace(new PointF(Width * 0.5f, Height * 0.5f));

            Invalidate();
        }

        private void HighlightPins(EditorObjectPin selectedPin)
        {
            foreach (EditorObjectPin pin in hoverables)
            {
                pin.Highlight = pin.IsCompatableWith(selectedPin);
            }

            selectedPin.Highlight = true;

            hoveringPin = selectedPin;

            Invalidate();
        }

        private void LayoutObject(EditorObjectLayout editorObjectLayout, ref PointF offset, ref RectangleF masterBounds)
        {
            float pinSize = 10;

            RectangleF bounds = masterBounds;

            bounds.Offset(offset.X, offset.Y);

            float yOffset = 0;

            #region Layout Property Pins

            {
                RectangleF pin = new RectangleF(0, 0, 8, 8);

                pin.Offset(bounds.Location);

                pin.Offset(bounds.Width - pinSize, yOffset + 2);

                foreach (EditorObjectPin propertyPin in editorObjectLayout.PropertyPins.Values)
                {
                    propertyPin.Bounds = pin;
                    propertyPin.Center = CenterOf(pin);

                    pin.Offset(-pinSize, 0);
                }

                yOffset += pinSize;
            }

            #endregion Layout Property Pins

            float pinYOffset = yOffset;

            #region Layout Binding Pins

            {
                RectangleF pin = new RectangleF(0, 0, 8, 8);

                pin.Offset(bounds.Location);

                pin.Offset(-pinSize, 1);

                foreach (EditorObjectPin bindingPin in editorObjectLayout.BindingPins.Values)
                {
                    bindingPin.Bounds = pin;
                    bindingPin.Center = CenterOf(pin);

                    pin.Offset(0, pinSize);

                    pinYOffset += pinSize;
                }
            }

            #endregion Layout Binding Pins

            #region Layout Label

            RectangleF labelBounds = bounds;
            labelBounds.Y += yOffset;
            labelBounds.Height = pinSize * 2f;
            labelBounds.Inflate(-2f, -2f);

            editorObjectLayout.LabelBounds = labelBounds;

            RectangleF connectionPinBounds = labelBounds;
            connectionPinBounds.X = masterBounds.Right + 4f + offset.X;
            connectionPinBounds.Width = connectionPinBounds.Height;
            connectionPinBounds.Inflate(-2, -2);

            editorObjectLayout.ObjectConnectionPin.Bounds = connectionPinBounds;
            editorObjectLayout.ObjectConnectionPin.Center = CenterOf(connectionPinBounds);

            yOffset += pinSize * 2f;

            yOffset = Math.Max(yOffset, pinYOffset);

            #endregion Layout Label

            #region Layout Nested Objects

            foreach (List<EditorObjectLayout> nestedList in editorObjectLayout.NestedObjects.Values)
            {
                PointF nestedOffset = new PointF(offset.X + 10, offset.Y + yOffset);

                nestedOffset.Y += 4f;

                foreach (EditorObjectLayout nestedLayout in nestedList)
                {
                    LayoutObject(nestedLayout, ref nestedOffset, ref masterBounds);

                    nestedOffset.Y += 4f;
                }

                yOffset = nestedOffset.Y - offset.Y;
            }

            #endregion Layout Nested Objects

            bounds.Height = yOffset;

            editorObjectLayout.Bounds = bounds;

            masterBounds.Height = offset.Y + yOffset;

            offset.Y += yOffset;
        }

        private void LayoutObjects()
        {
            totalBounds = RectangleF.Empty;

            foreach (EditorObjectLayout editorObjectLayout in objects.Values)
            {
                editorObjectLayout.Bounds = editorObjectLayout.Object.Bounds.Value;

                PointF offset = new PointF(0, 0);

                LayoutObject(editorObjectLayout, ref offset, ref editorObjectLayout.Bounds);

                if (totalBounds == RectangleF.Empty)
                {
                    totalBounds = editorObjectLayout.Bounds;
                }
                else
                {
                    totalBounds = RectangleF.Union(totalBounds, editorObjectLayout.Bounds);
                }
            }

            totalBounds.Inflate(40, 40);
        }

        private void RenderObject(Graphics g, StringFormat stringFormat, EditorObjectLayout @object, bool selected)
        {
            Styles styles = selected == true ? selectedStyles : unselectedStyles;
            Styles altStyles = selected == false ? selectedStyles : unselectedStyles;

            RectangleF bounds = @object.Bounds;

            RectangleF labelBounds = @object.LabelBounds;

            ThreadStyle threadStyle;
            ThreadStyle.State threadStyleState;

            if (string.IsNullOrEmpty(@object.ThreadName) == true)
            {
                threadStyle = nullThreadStyle;
            }
            else
            {
                threadStyle = GetThreadStyle(@object.ThreadName);
            }

            threadStyleState = selected ? threadStyle.Selected : threadStyle.Unselected;

            g.FillRectangle(threadStyleState.Fill, bounds);
            g.FillRectangle(styles.LabelFill, labelBounds);

            g.DrawRectangle(threadStyleState.Outline, bounds.X, bounds.Y, bounds.Width, bounds.Height);
            g.DrawRectangle(styles.LabelOutline, labelBounds.X, labelBounds.Y, labelBounds.Width, labelBounds.Height);

            {
                RectangleF objectConnectionPinBounds = @object.ObjectConnectionPin.Bounds;

                Styles s = @object.ObjectConnectionPin.Highlight ? altStyles : styles;

                if (@object.ObjectConnectionPin.Highlight == true)
                {
                    objectConnectionPinBounds.Inflate(-1, -1);
                }

                g.FillRectangle(s.LabelFill, objectConnectionPinBounds.X, objectConnectionPinBounds.Y, objectConnectionPinBounds.Width, objectConnectionPinBounds.Height);
                g.DrawRectangle(s.LabelOutline, objectConnectionPinBounds.X, objectConnectionPinBounds.Y, objectConnectionPinBounds.Width, objectConnectionPinBounds.Height);
            }

            g.DrawString(@object.Address, Font, styles.LabelText, labelBounds, stringFormat);

            foreach (EditorObjectPin bindingPin in @object.BindingPins.Values)
            {
                RectangleF pin = bindingPin.Bounds;

                Pen pen = bindingPin.Highlight ? styles.PinHighlightOutline : styles.PinOutline;
                Brush brush = bindingPin.Highlight ? styles.PinHighlightFill : styles.PinFill;

                g.FillRectangle(brush, pin);
                g.DrawRectangle(pen, pin.X, pin.Y, pin.Width, pin.Height);
            }

            foreach (EditorObjectPin propertyPin in @object.PropertyPins.Values)
            {
                RectangleF pin = propertyPin.Bounds;

                Pen pen = propertyPin.Highlight ? styles.PinHighlightOutline : styles.PinOutline;
                Brush brush = propertyPin.Highlight ? styles.PinHighlightFill : styles.PinFill;

                g.FillRectangle(brush, pin);
                g.DrawRectangle(pen, pin.X, pin.Y, pin.Width, pin.Height);
            }

            foreach (List<EditorObjectLayout> nestedObjectList in @object.NestedObjects.Values)
            {
                foreach (EditorObjectLayout nestedObject in nestedObjectList)
                {
                    RenderObject(g, stringFormat, nestedObject, !selected);
                }
            }
        }

        private ThreadStyle GetThreadStyle(string threadName)
        {
            ThreadStyle style;

            if (threadStyles.TryGetValue(threadName, out style) == false)
            {
                style = nullThreadStyle.Clone();

                threadStyles.Add(threadName, style);
            }

            return style;
        }

        private PointF ScreenSpaceToWorkspace(PointF location)
        {
            float scaleReciprocal = 1f / viewScale;

            //return new PointF(((float)location.X / viewScale) - viewOffset.X, ((float)location.Y / viewScale) - viewOffset.Y);
            return new PointF((location.X - viewOffset.X) * scaleReciprocal, (location.Y - viewOffset.Y) * scaleReciprocal);
        }

        private PointF WorkspaceToScreenSpace(PointF location)
        {
            return new PointF((location.X + viewOffset.X) * viewScale, (location.Y + viewOffset.Y) * viewScale);
        }

        private void UnhighlightPins()
        {
            foreach (EditorObjectPin pin in hoverables)
            {
                pin.Highlight = false;
            }

            hoveringPin = null;

            Invalidate();
        }

        private class Binding
        {
            public string ThreadName;

            public EditorObjectPin From;

            public EditorObjectPin To;
            public string ToAddress;
        }

        private class EditorObjectLayout
        {
            public string ThreadName;

            public readonly Dictionary<string, EditorObjectPin> BindingPins = new Dictionary<string, EditorObjectPin>();
            public readonly Dictionary<string, List<EditorObjectLayout>> NestedObjects = new Dictionary<string, List<EditorObjectLayout>>();
            public readonly Dictionary<string, EditorObjectPin> PropertyPins = new Dictionary<string, EditorObjectPin>();

            public string Address;

            public RectangleF Bounds;
            public RectangleF LabelBounds;
            public IObject Object;

            public EditorObjectPin ObjectConnectionPin = new EditorObjectPin() { PinType = EditorObjectPinType.Object };
            public IObject Parent;

            public bool Selected;
            public bool Touched;
        }

        private class EditorObjectPin
        {
            public string ThreadName;

            public RectangleF Bounds;
            public PointF Center;
            public bool Highlight;

            public IOscMember Member;
            public string OscAddress;
            public Type PinObjectType;
            public EditorObjectPinType PinType;

            public string GetText()
            {
                switch (PinType)
                {
                    case EditorObjectPinType.Object:
                        return "[" + LoadManager.GetTypeName(PinObjectType) + "] " + OscAddress;

                    case EditorObjectPinType.Property:
                        return "[" + LoadManager.GetTypeName(PinObjectType) + "] " + Member.ToString();

                    case EditorObjectPinType.Binding:
                        return "[" + LoadManager.GetTypeName(PinObjectType) + "] " + Member.ToString();

                    default:
                        return "[None] " + OscAddress;
                }

                //if (Member != null)
                //{
                //    return Member.ToString();
                //}
                //else
                //{
                //    return OscAddress;
                //}
            }

            public bool IsCompatableWith(EditorObjectPin pin)
            {
                switch (PinType)
                {
                    case EditorObjectPinType.None:
                        return false;

                    case EditorObjectPinType.Object:
                        switch (pin.PinType)
                        {
                            case EditorObjectPinType.Binding:
                                return PinObjectType == pin.PinObjectType || PinObjectType.IsSubclassOf(pin.PinObjectType) || pin.PinObjectType.IsAssignableFrom(PinObjectType);

                            default:
                                return false;
                        }
                    case EditorObjectPinType.Binding:
                        switch (pin.PinType)
                        {
                            case EditorObjectPinType.Object:
                                return pin.PinObjectType == PinObjectType || pin.PinObjectType.IsSubclassOf(PinObjectType) || PinObjectType.IsAssignableFrom(pin.PinObjectType);

                            default:
                                return false;
                        }
                    case EditorObjectPinType.Property:
                        return false;

                    default:
                        return false;
                }
            }
        }

        private class Styles
        {
            public SolidBrush PinHighlightFill { get; set; }

            public Pen PinHighlightOutline { get; set; }

            public Brush BoxFill { get; set; }

            public Pen BoxOutline { get; set; }

            public Brush LabelFill { get; set; }

            public Pen LabelOutline { get; set; }

            public Brush LabelText { get; set; }

            public Brush PinFill { get; set; }

            public Pen PinOutline { get; set; }
        }
    }
}