﻿namespace FFTracking_UI.Panels
{
    partial class Terminal
	{
		/// <summary> 
		/// Required designer command.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.sendMessageBox = new System.Windows.Forms.ComboBox();
			this.consoleControl = new Rug.Forms.Console.ConsoleControl();
			this.SuspendLayout();
			// 
			// sendMessageBox
			// 
			this.sendMessageBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
			this.sendMessageBox.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.sendMessageBox.FormattingEnabled = true;
			this.sendMessageBox.Location = new System.Drawing.Point(0, 343);
			this.sendMessageBox.Name = "sendMessageBox";
			this.sendMessageBox.Size = new System.Drawing.Size(610, 21);
			this.sendMessageBox.TabIndex = 1;
			this.sendMessageBox.SelectedIndexChanged += new System.EventHandler(this.SendMessageBox_SelectedIndexChanged);
			this.sendMessageBox.TextChanged += new System.EventHandler(this.SendMessageBox_TextChanged);
			this.sendMessageBox.Enter += new System.EventHandler(this.SendMessageBox_Enter);
			this.sendMessageBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SendMessageBox_KeyDown);
			this.sendMessageBox.Leave += new System.EventHandler(this.SendMessageBox_Leave);
			// 
			// consoleControl1
			// 
			this.consoleControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.consoleControl.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.consoleControl.Input = null;
			this.consoleControl.Location = new System.Drawing.Point(0, 0);
			this.consoleControl.Name = "consoleControl";
			this.consoleControl.Size = new System.Drawing.Size(610, 343);
			this.consoleControl.TabIndex = 0;
			this.consoleControl.Text = "consoleControl";
			this.consoleControl.UpdateInterval = 10;
			this.consoleControl.WordWrap = false;
			// 
			// Terminal
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.consoleControl);
			this.Controls.Add(this.sendMessageBox);
			this.Name = "Terminal";
			this.Size = new System.Drawing.Size(610, 364);
			this.Load += new System.EventHandler(this.Terminal_Load);
			this.ResumeLayout(false);

		}

		#endregion

		private Rug.Forms.Console.ConsoleControl consoleControl;
		private System.Windows.Forms.ComboBox sendMessageBox;
	}
}
