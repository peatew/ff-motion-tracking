﻿using System;
using System.Collections.Generic;
using System.Xml;
using FFTracking_UI.Console;
using Rug.Host;
using Rug.Host.Comms;
using Rug.Host.Loading;

namespace FFTracking_UI.Config
{
    internal class ConfigLoader
    {
        public readonly List<Command> Commands = new List<Command>();

        public readonly List<GraphWindow> Graphs = new List<GraphWindow>();

        public readonly List<ImageDebugWindow> ImageDebugWindows = new List<ImageDebugWindow>();

        public void Attach(IOscConnection connection)
        {
            foreach (GraphWindow graph in Graphs)
            {
                foreach (GraphWindow.Trace trace in graph.Traces)
                {
                    connection?.Listener.Attach(trace.OscAddress, trace.OnMessage);
                }
            }
        }

        public void AttachLocal(DemoServer server)
        {
            if (server == null)
            {
                return;
            }

            foreach (ImageDebugWindow window in ImageDebugWindows)
            {
                IDebugImageSource source = server.Workspace.FindObject<IDebugImageSource>(window.Name);

                if (source == null)
                {
                    continue;
                }

                source.NewDebugFrame += window.OnNewDebugFrame;
            }
        }

        public void Detach(IOscConnection connection)
        {
            foreach (GraphWindow graph in Graphs)
            {
                foreach (GraphWindow.Trace trace in graph.Traces)
                {
                    connection?.Listener.Detach(trace.OscAddress, trace.OnMessage);
                }
            }
        }

        public void DetachLocal(DemoServer server)
        {
            if (server == null)
            {
                return;
            }

            foreach (ImageDebugWindow window in ImageDebugWindows)
            {
                IDebugImageSource source = server.Workspace.FindObject<IDebugImageSource>(window.Name);

                if (source == null)
                {
                    continue;
                }

                source.NewDebugFrame -= window.OnNewDebugFrame;
            }
        }

        public void Load()
        {
            LoadContext context = new LoadContext(FormReporter.ClientInstance);

            try
            {
                XmlDocument doc = new XmlDocument();

                doc.Load(Helper.ResolvePath("~/Config.xml"));

                Commands.AddRange(Loader.LoadObjects<Command>(context, doc.DocumentElement.SelectSingleNode("Commands")));

                Graphs.AddRange(Loader.LoadObjects<GraphWindow>(context, doc.DocumentElement.SelectSingleNode("Graphs")));

                ImageDebugWindows.AddRange(Loader.LoadObjects<ImageDebugWindow>(context, doc.DocumentElement.SelectSingleNode("ImageDebugWindows")));

            }
            catch (Exception ex)
            {
                context.Error(ex.Message); 
            }
            finally
            {
                context.ReportErrors();
            }
        }
    }
}