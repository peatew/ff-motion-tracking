﻿using System.Drawing;
using System.Xml;
using Rug.Host;
using Rug.Host.Loading;

namespace FFTracking_UI.Config
{
    [XmlName("ImageDebugWindow")]
    class ImageDebugWindow : ILoadable
    {
        public string Name { get; set; }

        public event DebugFrameEvent NewDebugFrame;

        public void Load(LoadContext context, XmlNode node)
        {
            Name = Helper.GetAttributeValue(node, "Name", string.Empty);
        }

        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "Name", Name);

            return element; 
        }

        public void OnNewDebugFrame(IDebugImageSource source, Bitmap bitmap)
        {
            NewDebugFrame?.Invoke(source, bitmap); 
        }
    }
}
