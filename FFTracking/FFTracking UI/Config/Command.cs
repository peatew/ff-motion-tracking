﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Rug.Host;
using Rug.Host.Loading;
using Rug.Osc;

namespace FFTracking_UI.Config
{
    [XmlName("Command")]
    internal sealed class Command : ILoadable
    {
        public string Name { get; private set; }

        public OscPacket[] Script { get; private set; }

        public void Load(LoadContext context, XmlNode node)
        {
            Name = Helper.GetAttributeValue(node, "Name", string.Empty);

            if (string.IsNullOrEmpty(Name) == true)
            {
                context.Error("Missing or blank Name attribute on Command.");
                return;
            }

            List<OscPacket> commandPackets = new List<OscPacket>();

            string[] lines = node.InnerText.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string line in lines)
            {
                OscPacket packet;

                if (OscPacket.TryParse(line.Trim(), out packet) == false)
                {
                    context.Error(string.Format("Command {0} is an invalid OSC message or bundle. {1}", Name, line.Trim()));
                    continue;
                }

                commandPackets.Add(packet);
            }

            Script = commandPackets.ToArray();
        }

        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "Name", Name);

            StringBuilder sb = new StringBuilder();

            foreach (OscPacket packet in Script)
            {
                sb.AppendLine(packet.ToString());
            }

            element.InnerText = sb.ToString().TrimEnd();

            return element;
        }
    }
}