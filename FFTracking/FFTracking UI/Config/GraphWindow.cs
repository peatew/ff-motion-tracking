﻿using System.Collections.Generic;
using System.Drawing;
using System.Xml;
using Rug.Host;
using Rug.Host.Loading;
using Rug.LiteGL.Controls;
using Rug.Osc;

namespace FFTracking_UI.Config
{
    [XmlName("Graph")]
    internal class GraphWindow : ILoadable
    {
        public enum GraphType
        {
            Time,
            XY,
        }

        public readonly List<Trace> Traces = new List<Trace>();
        public AxesRange AxesRange;

        public string Name;

        public GraphType PlotType;
        public string XAxisLabel;
        public string YAxisLabel;

        public bool Rolling = false;

        public void Load(LoadContext context, XmlNode node)
        {
            Name = Helper.GetAttributeValue(node, "Name", string.Empty);

            XAxisLabel = Helper.GetAttributeValue(node, "XAxisLabel", string.Empty);
            YAxisLabel = Helper.GetAttributeValue(node, "YAxisLabel", string.Empty);

            PlotType = Helper.GetAttributeValue(node, "PlotType", GraphType.Time);
            Rolling = Helper.GetAttributeValue(node, "Rolling", Rolling);

            RectangleF axesRange = Helper.GetAttributeValue(node, "AxesRange", RectangleF.Empty);

            if (string.IsNullOrEmpty(Name) == true)
            {
                context.Error("Missing Name attribute.");
            }

            if (axesRange.Equals(RectangleF.Empty))
            {
                context.Error("Missing AxesRange attribute.");
            }

            AxesRange = new AxesRange(axesRange.X, axesRange.Y, axesRange.Width, axesRange.Height);

            Traces.Clear();
            Traces.AddRange(Loader.LoadObjects<Trace>(context, node));
        }

        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "Name", Name);
            Helper.AppendAttributeAndValue(element, "XAxisLabel", XAxisLabel);
            Helper.AppendAttributeAndValue(element, "YAxisLabel", YAxisLabel);
            Helper.AppendAttributeAndValue(element, "PlotType", PlotType);
            Helper.AppendAttributeAndValue(element, "Rolling", Rolling);
            
            Helper.AppendAttributeAndValue(element, "AxesRange", new RectangleF(AxesRange.MinX, AxesRange.MinY, AxesRange.MaxX, AxesRange.MaxY));

            Loader.SaveObjects(context, null, null, Traces.ToArray(), element);

            return element;
        }

        [XmlName("Trace")]
        public class Trace : ILoadable
        {
            public delegate void TraceEvent(Trace trace);

            public int Argument;
            public uint BlockSize = 256;
            public Color Color;
            public string Label;

            public uint MaxDataPoints = 1024 * 24;
            public string OscAddress;

            public int TraceIndex;

            public event OscMessageEvent MessageEvent;

            public void Load(LoadContext context, XmlNode node)
            {
                Label = Helper.GetAttributeValue(node, "Label", string.Empty);
                OscAddress = Helper.GetAttributeValue(node, "OscAddress", string.Empty);
                Argument = Helper.GetAttributeValue(node, "Argument", 0);
                Color = Helper.GetAttributeValue(node, "Color", Color.Transparent);
                MaxDataPoints = Helper.GetAttributeValue(node, "MaxDataPoints", (uint)(1024 * 24));
                BlockSize = Helper.GetAttributeValue(node, "BlockSize", (uint)(256));

                if (string.IsNullOrEmpty(Label) == true)
                {
                    context.Error("Missing Label attribute.");
                }

                if (string.IsNullOrEmpty(OscAddress) == true)
                {
                    context.Error("Missing OscAddress attribute.");
                }

                if (Color == Color.Transparent)
                {
                    context.Error("Missing Color attribute.");
                }
            }

            public void OnMessage(OscMessage message)
            {
                MessageEvent?.Invoke(message);
            }

            public XmlElement Save(LoadContext context, XmlElement element)
            {
                Helper.AppendAttributeAndValue(element, "Label", Label);
                Helper.AppendAttributeAndValue(element, "OscAddress", OscAddress);
                Helper.AppendAttributeAndValue(element, "Argument", Argument);
                Helper.AppendAttributeAndValue(element, "Color", Color);
                Helper.AppendAttributeAndValue(element, "MaxDataPoints", MaxDataPoints);

                return element;
            }
        }
    }
}