﻿using System;
using System.Windows.Forms;
using AForge.Imaging;
using PointGreySource;
using Rug.Cmd;
using Rug.CV;
using Rug.Host;
using Rug.Host.Loading;

namespace FFTracking_UI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            ConsoleColorState state = RC.ColorState;

            LoadManager.ScanAssembly(typeof(Program).Assembly);
            LoadManager.ScanAssembly(typeof(IObject).Assembly);
            LoadManager.ScanAssembly(typeof(IBlobSource).Assembly);
            LoadManager.ScanAssembly(typeof(PointGreyMaster).Assembly);
            LoadManager.ScanAssembly(typeof(Tracking.EmguCV.CheckboardDetector).Assembly);
            
            try
            {
                RC.Sys = new NullConsole();
                RC.App = RC.Sys;

                RC.Verbosity = ConsoleVerbosity.Normal;

                Options.Load();

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new MainForm());
            }
            finally
            {
                Options.Save();

                RC.ColorState = state;
            }
        }
    }
}
