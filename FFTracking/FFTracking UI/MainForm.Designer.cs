﻿namespace FFTracking_UI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.findServerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.connectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.disconnectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openVideoFileusingDirectShowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.commandsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.graphsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageDebuggersMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outputToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.terminalTab = new System.Windows.Forms.TabPage();
            this.terminal = new FFTracking_UI.Panels.Terminal();
            this.serverTab = new System.Windows.Forms.TabPage();
            this.serverOutput1 = new FFTracking_UI.Panels.ServerOutput();
            this.filterTab = new System.Windows.Forms.TabPage();
            this.filterPanel1 = new FFTracking_UI.Panels.FilterPanel();
            this.graphEditorPanel = new System.Windows.Forms.TabPage();
            this.graphEditorPanel1 = new FFTracking_UI.Panels.GraphEditorPanel();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.openServerFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveServerFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.terminalTab.SuspendLayout();
            this.serverTab.SuspendLayout();
            this.filterTab.SuspendLayout();
            this.graphEditorPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.commandsMenuItem,
            this.graphsToolStripMenuItem,
            this.imageDebuggersMenuItem,
            this.outputToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(9, 3, 0, 3);
            this.menuStrip1.Size = new System.Drawing.Size(1038, 25);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.findServerToolStripMenuItem,
            this.connectToolStripMenuItem,
            this.disconnectToolStripMenuItem,
            this.toolStripSeparator1,
            this.newToolStripMenuItem,
            this.openVideoFileusingDirectShowToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.closeToolStripMenuItem,
            this.toolStripMenuItem1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 19);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // findServerToolStripMenuItem
            // 
            this.findServerToolStripMenuItem.Name = "findServerToolStripMenuItem";
            this.findServerToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this.findServerToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.findServerToolStripMenuItem.Text = "Find Server";
            this.findServerToolStripMenuItem.Click += new System.EventHandler(this.findServerToolStripMenuItem_Click);
            // 
            // connectToolStripMenuItem
            // 
            this.connectToolStripMenuItem.Name = "connectToolStripMenuItem";
            this.connectToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F4;
            this.connectToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.connectToolStripMenuItem.Text = "Connect";
            this.connectToolStripMenuItem.Click += new System.EventHandler(this.connectToolStripMenuItem_Click);
            // 
            // disconnectToolStripMenuItem
            // 
            this.disconnectToolStripMenuItem.Name = "disconnectToolStripMenuItem";
            this.disconnectToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.disconnectToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.disconnectToolStripMenuItem.Text = "Disconnect";
            this.disconnectToolStripMenuItem.Click += new System.EventHandler(this.disconnectToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(183, 6);
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.newToolStripMenuItem.Text = "&New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // openVideoFileusingDirectShowToolStripMenuItem
            // 
            this.openVideoFileusingDirectShowToolStripMenuItem.Name = "openVideoFileusingDirectShowToolStripMenuItem";
            this.openVideoFileusingDirectShowToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openVideoFileusingDirectShowToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.openVideoFileusingDirectShowToolStripMenuItem.Text = "&Open";
            this.openVideoFileusingDirectShowToolStripMenuItem.Click += new System.EventHandler(this.openVideoFileusingDirectShowToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.saveToolStripMenuItem.Text = "&Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.saveAsToolStripMenuItem.Text = "&Save As";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(183, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            // 
            // commandsMenuItem
            // 
            this.commandsMenuItem.Name = "commandsMenuItem";
            this.commandsMenuItem.Size = new System.Drawing.Size(81, 19);
            this.commandsMenuItem.Text = "Commands";
            // 
            // graphsToolStripMenuItem
            // 
            this.graphsToolStripMenuItem.Name = "graphsToolStripMenuItem";
            this.graphsToolStripMenuItem.Size = new System.Drawing.Size(56, 19);
            this.graphsToolStripMenuItem.Text = "Graphs";
            // 
            // imageDebuggersMenuItem
            // 
            this.imageDebuggersMenuItem.Name = "imageDebuggersMenuItem";
            this.imageDebuggersMenuItem.Size = new System.Drawing.Size(112, 19);
            this.imageDebuggersMenuItem.Text = "Image Debuggers";
            // 
            // outputToolStripMenuItem
            // 
            this.outputToolStripMenuItem.Name = "outputToolStripMenuItem";
            this.outputToolStripMenuItem.Size = new System.Drawing.Size(57, 19);
            this.outputToolStripMenuItem.Text = "Output";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.serverTab);
            this.tabControl1.Controls.Add(this.terminalTab);
            this.tabControl1.Controls.Add(this.filterTab);
            this.tabControl1.Controls.Add(this.graphEditorPanel);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 25);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1038, 821);
            this.tabControl1.TabIndex = 1;
            // 
            // terminalTab
            // 
            this.terminalTab.Controls.Add(this.terminal);
            this.terminalTab.Location = new System.Drawing.Point(4, 29);
            this.terminalTab.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.terminalTab.Name = "terminalTab";
            this.terminalTab.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.terminalTab.Size = new System.Drawing.Size(1030, 788);
            this.terminalTab.TabIndex = 0;
            this.terminalTab.Text = "Terminal";
            this.terminalTab.UseVisualStyleBackColor = true;
            // 
            // terminal
            // 
            this.terminal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.terminal.HistoryLength = 128;
            this.terminal.Location = new System.Drawing.Point(4, 5);
            this.terminal.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.terminal.Name = "terminal";
            this.terminal.Size = new System.Drawing.Size(1022, 778);
            this.terminal.Statistics = null;
            this.terminal.TabIndex = 0;
            // 
            // serverTab
            // 
            this.serverTab.Controls.Add(this.serverOutput1);
            this.serverTab.Location = new System.Drawing.Point(4, 29);
            this.serverTab.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.serverTab.Name = "serverTab";
            this.serverTab.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.serverTab.Size = new System.Drawing.Size(1030, 788);
            this.serverTab.TabIndex = 3;
            this.serverTab.Text = "Server";
            this.serverTab.UseVisualStyleBackColor = true;
            // 
            // serverOutput1
            // 
            this.serverOutput1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.serverOutput1.HistoryLength = 128;
            this.serverOutput1.Location = new System.Drawing.Point(4, 5);
            this.serverOutput1.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.serverOutput1.Name = "serverOutput1";
            this.serverOutput1.Size = new System.Drawing.Size(1022, 778);
            this.serverOutput1.TabIndex = 0;
            // 
            // filterTab
            // 
            this.filterTab.Controls.Add(this.filterPanel1);
            this.filterTab.Location = new System.Drawing.Point(4, 29);
            this.filterTab.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.filterTab.Name = "filterTab";
            this.filterTab.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.filterTab.Size = new System.Drawing.Size(1030, 788);
            this.filterTab.TabIndex = 1;
            this.filterTab.Text = "Filter";
            this.filterTab.UseVisualStyleBackColor = true;
            // 
            // filterPanel1
            // 
            this.filterPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filterPanel1.Location = new System.Drawing.Point(4, 5);
            this.filterPanel1.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.filterPanel1.Name = "filterPanel1";
            this.filterPanel1.Size = new System.Drawing.Size(1022, 778);
            this.filterPanel1.TabIndex = 0;
            // 
            // graphEditorPanel
            // 
            this.graphEditorPanel.Controls.Add(this.graphEditorPanel1);
            this.graphEditorPanel.Location = new System.Drawing.Point(4, 29);
            this.graphEditorPanel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.graphEditorPanel.Name = "graphEditorPanel";
            this.graphEditorPanel.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.graphEditorPanel.Size = new System.Drawing.Size(1030, 788);
            this.graphEditorPanel.TabIndex = 4;
            this.graphEditorPanel.Text = "Graph Editor";
            this.graphEditorPanel.UseVisualStyleBackColor = true;
            // 
            // graphEditorPanel1
            // 
            this.graphEditorPanel1.BackColor = System.Drawing.Color.Black;
            this.graphEditorPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.graphEditorPanel1.Location = new System.Drawing.Point(4, 5);
            this.graphEditorPanel1.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.graphEditorPanel1.Name = "graphEditorPanel1";
            this.graphEditorPanel1.Size = new System.Drawing.Size(1022, 778);
            this.graphEditorPanel1.TabIndex = 0;
            this.graphEditorPanel1.Workspace = null;
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "AVI files (*.avi)|*.avi|All files (*.*)|*.*";
            this.openFileDialog.Title = "Opem movie";
            // 
            // openServerFileDialog
            // 
            this.openServerFileDialog.Filter = "XML files|*.xml|All files|*.*";
            this.openServerFileDialog.Title = "Open Server File";
            // 
            // saveServerFileDialog
            // 
            this.saveServerFileDialog.Filter = "XML files|*.xml|All files|*.*";
            this.saveServerFileDialog.Title = "Save Server File";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1038, 846);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "MainForm";
            this.Text = "Future Fest Tracker UI";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.terminalTab.ResumeLayout(false);
            this.serverTab.ResumeLayout(false);
            this.filterTab.ResumeLayout(false);
            this.graphEditorPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openVideoFileusingDirectShowToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage terminalTab;
        private System.Windows.Forms.TabPage filterTab;
        private Panels.Terminal terminal;
        private Panels.FilterPanel filterPanel1;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ToolStripMenuItem graphsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem commandsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outputToolStripMenuItem;
        private System.Windows.Forms.TabPage serverTab;
        private Panels.ServerOutput serverOutput1;
        private System.Windows.Forms.ToolStripMenuItem imageDebuggersMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openServerFileDialog;
        private System.Windows.Forms.SaveFileDialog saveServerFileDialog;
        private System.Windows.Forms.ToolStripMenuItem findServerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem connectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem disconnectToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.TabPage graphEditorPanel;
        private Panels.GraphEditorPanel graphEditorPanel1;
    }
}

