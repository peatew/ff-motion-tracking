﻿using System.Net;
using FFTracking_UI.Console;
using Rug.Host.Comms;

namespace FFTracking_UI
{
    partial class MainForm
    {
        private string serverFilePath = null; 

        private void AttachDebuggers()
        {
            config?.Attach(clientConnection);

            config?.AttachLocal(demoServer);
        }

        private void DetachDebuggers()
        {
            config?.Detach(clientConnection);

            config?.DetachLocal(demoServer);
        }

        private void Connection_Connect(OscConnectionInfo info)
        {
            Connection_Disconnect();

            clientConnection = new ClientConnection(FormReporter.ClientInstance, info);

            AttachDebuggers();

            clientConnection.Connect();
        }

        private void Connection_Disconnect()
        {
            DetachDebuggers();

            clientConnection?.Dispose();
        }

        private void Server_Close()
        {
            Connection_Disconnect();

            demoServer?.Dispose();
        }

        private void Server_New()
        {
            Server_Open(null);
        }

        private bool Server_Save()
        {
            if (string.IsNullOrEmpty(serverFilePath) == true)
            {
                return false; 
            } 

            Server_Save(serverFilePath);

            return true; 
        }

        private void Server_Save(string filePath)
        {
            demoServer?.Save(filePath); 
        }

        private void Server_Open(string filePath)
        {
            Server_Close();

            serverFilePath = filePath; 

            demoServer = new DemoServer(FormReporter.ServerInstance);

            demoServer.Connect();

            demoServer.Load(filePath);

            Connection_Connect(new OscConnectionInfo("Local connection", IPAddress.Any, IPAddress.Loopback, 8888, 7888));

            graphEditorPanel1.Workspace = demoServer.Workspace;
            graphEditorPanel1.Rebuild(); 
        }
    }
}