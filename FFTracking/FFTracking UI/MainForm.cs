﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using FFTracking_UI.Config;
using FFTracking_UI.Console;
using FFTracking_UI.DialogsAndWindows;
using Rug.Host;
using Rug.Host.Comms;
using Rug.Osc;

namespace FFTracking_UI
{
    public partial class MainForm : BaseForm
    {
        private readonly Dictionary<string, DialogsAndWindows.IGraphWindow> graphWindows = new Dictionary<string, DialogsAndWindows.IGraphWindow>();
        private readonly Dictionary<string, VideoPreviewWindow> imageDebuggerWindows = new Dictionary<string, VideoPreviewWindow>();

        private IOscConnection clientConnection;
        private ConfigLoader config;
        private DemoServer demoServer;

        public MainForm()
        {
            WindowID = "MainForm";

            InitializeComponent();
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Server_Close();
        }

        private void Command_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;

            Command command = item.Tag as Command;

            foreach (OscPacket packet in command.Script)
            {
                clientConnection?.Send(packet);
            }

            clientConnection?.Flush();
        }

        private void connectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (DirectConnectionDialog dialog = new DirectConnectionDialog())
            {
                if (dialog.ShowDialog(this) != DialogResult.OK)
                {
                    return;
                }

                Connection_Connect(dialog.Info);
            }
        }

        private void disconnectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Connection_Disconnect();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void findServerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (SearchForServerDialog dialog = new SearchForServerDialog())
            {
                if (dialog.ShowDialog(this) != DialogResult.OK)
                {
                    return;
                }

                Connection_Connect(dialog.Info);
            }
        }

        private void Graph_CheckedChanged(object sender, EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;

            Config.GraphWindow graph = item.Tag as Config.GraphWindow;

            if (graphWindows.ContainsKey(graph.Name) == true && item.Checked == false)
            {
                graphWindows[graph.Name].Close();
            }
            else if (graphWindows.ContainsKey(graph.Name) == false && item.Checked == true)
            {
                IGraphWindow window;
                if (graph.PlotType == Config.GraphWindow.GraphType.Time)
                {
                    window = new DialogsAndWindows.GraphWindow();
                }
                else
                {
                    window = new DialogsAndWindows.XYGraphWindow();
                }

                window.SetGraph(graph);
                window.FormClosed += GraphWindow_FormClosed;
                graphWindows.Add(graph.Name, window);

                window.Show();
            }
        }

        private void GraphWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            IGraphWindow window = sender as IGraphWindow;

            graphWindows.Remove(window.WindowID);

            (graphsToolStripMenuItem.DropDownItems.Find(window.WindowID, false)[0] as ToolStripMenuItem).Checked = false;
        }

        private void ImageDebugWindow_CheckedChanged(object sender, EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;

            ImageDebugWindow imageDebugWindow = item.Tag as ImageDebugWindow;

            if (imageDebuggerWindows.ContainsKey(imageDebugWindow.Name) == true && item.Checked == false)
            {
                imageDebuggerWindows[imageDebugWindow.Name].Close();
            }
            else if (imageDebuggerWindows.ContainsKey(imageDebugWindow.Name) == false && item.Checked == true)
            {
                VideoPreviewWindow window = new VideoPreviewWindow();

                window.SetDebugger(imageDebugWindow);

                window.FormClosed += ImageDebugWindow_FormClosed;

                imageDebuggerWindows.Add(imageDebugWindow.Name, window);

                window.Show();
            }
        }

        private void ImageDebugWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            VideoPreviewWindow window = sender as VideoPreviewWindow;

            imageDebuggerWindows.Remove(window.WindowID);

            (imageDebuggersMenuItem.DropDownItems.Find(window.WindowID, false)[0] as ToolStripMenuItem).Checked = false;
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Server_Close();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            terminal.HistoryLength = 1024;
            terminal.PacketRecived += Terminal_PacketRecived;

            foreach (object verbosityValue in Enum.GetValues(typeof(ReportVerbosity)))
            {
                ToolStripMenuItem item = new ToolStripMenuItem(verbosityValue.ToString());

                item.Tag = (ReportVerbosity)verbosityValue;
                item.CheckOnClick = true;
                item.CheckedChanged += ReportVerbosity_CheckedChanged;

                outputToolStripMenuItem.DropDownItems.Add(item);
            }

            config = new ConfigLoader();

            config.Load();

            foreach (Command command in config.Commands)
            {
                ToolStripMenuItem item = new ToolStripMenuItem(command.Name);

                item.Tag = command;
                item.Click += Command_Click;
                item.Enabled = false;

                commandsMenuItem.DropDownItems.Add(item);
            }

            foreach (Config.GraphWindow graph in config.Graphs)
            {
                ToolStripMenuItem item = new ToolStripMenuItem(graph.Name);

                item.Tag = graph;
                item.Name = graph.Name;
                item.CheckOnClick = true;
                item.CheckedChanged += Graph_CheckedChanged;
                item.Checked = Options.Windows[graph.Name].IsOpen;
                item.Enabled = true;

                graphsToolStripMenuItem.DropDownItems.Add(item);
            }

            foreach (ImageDebugWindow window in config.ImageDebugWindows)
            {
                ToolStripMenuItem item = new ToolStripMenuItem(window.Name);

                item.Tag = window;
                item.Name = window.Name;
                item.CheckOnClick = true;
                item.CheckedChanged += ImageDebugWindow_CheckedChanged;
                item.Checked = Options.Windows[window.Name].IsOpen;
                item.Enabled = true;

                imageDebuggersMenuItem.DropDownItems.Add(item);
            }

            SetupReportVerbosityCheckedState();

            Server_Open(Helper.ResolvePath("~/Tracking-Demo.xml"));
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Server_New();
        }

        private void openVideoFileusingDirectShowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openServerFileDialog.ShowDialog(this) != DialogResult.OK)
            {
                return;
            }

            Server_Open(openServerFileDialog.FileName);
        }

        private void ReportVerbosity_CheckedChanged(object sender, EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;

            if (item.Checked == true)
            {
                FormReporter.ClientInstance.ReportVerbosity = (ReportVerbosity)item.Tag;
            }

            SetupReportVerbosityCheckedState();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveServerFileDialog.ShowDialog(this) != DialogResult.OK)
            {
                return;
            }

            Server_Save(saveServerFileDialog.FileName);
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Server_Save() == false)
            {
                return;
            }

            saveAsToolStripMenuItem_Click(sender, e);
        }

        private void SetupReportVerbosityCheckedState()
        {
            foreach (ToolStripMenuItem item in outputToolStripMenuItem.DropDownItems)
            {
                item.Checked = (ReportVerbosity)item.Tag == FormReporter.ClientInstance.ReportVerbosity;
            }
        }

        private void Terminal_PacketRecived(Rug.Osc.OscPacket packet)
        {
            clientConnection?.Send(packet);
            clientConnection?.Flush();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            filterPanel1.CalculateRate();
        }
    }
}