﻿using System;
using System.Net;
using System.Xml;
using Rug.Host;
using Rug.Host.Comms;
using Rug.Host.Loading;

namespace FFTracking_UI
{
    internal class DemoServer
    {
        private IOscConnection connection;

        public string FilePath { get; private set; }

        public IReporter Reporter { get; private set; }

        public Workspace Workspace { get; private set; }

        public DemoServer(IReporter reporter)
        {
            Reporter = reporter;
        }

        public void Connect()
        {
            connection = new ServerConnection(Reporter, new OscConnectionInfo("Tracking Server", IPAddress.Any, IPAddress.Any, 7888, 8888));
            connection.Listener.UnknownAddress += Listener_UnknownAddress;
            Workspace = new Workspace(Reporter, connection);

            connection.Connect();
        }

        public void Dispose()
        {
            Workspace?.Dispose();
            connection?.Dispose();
        }

        public void Load(string filePath)
        {
            LoadContext context = new LoadContext(Reporter);

            if (string.IsNullOrEmpty(filePath) == false)
            {
                try
                {
                    Reporter.PrintNormal(filePath);

                    XmlDocument doc = new XmlDocument();

                    doc.Load(Helper.ResolvePath(filePath));

                    Workspace.Load(context, doc.DocumentElement);
                }
                catch (Exception ex)
                {
                    context.Error("Exception while loading server file. " + ex.Message);
                }
                finally
                {
                    Reporter.PrintNormal(context.Errors.Count + " errors");

                    context.ReportErrors();
                }
            }

            if (context.HasHadCriticalError == true)
            {
                return;
            }

            Workspace.Start();
        }

        public void Save(string filePath)
        {
            LoadContext context = new LoadContext(Reporter);

            try
            {
                XmlDocument doc = new XmlDocument();

                doc.AppendChild(Workspace.Save(context, Helper.CreateElement(doc, "Server")));

                doc.Save(filePath);
            }
            catch (Exception ex)
            {
                context.Error("Exception while saving server file. " + ex.Message);
            }
            finally
            {
                Reporter.PrintNormal(context.Errors.Count + " errors");

                context.ReportErrors();
            }
        }

        private void Listener_UnknownAddress(object sender, Rug.Osc.UnknownAddressEventArgs e)
        {
            connection?.Send("/error", "Unknown address " + e.Address);
        }
    }
}