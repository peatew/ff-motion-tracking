﻿using Rug.Cmd;

namespace FFTracking_UI.Console
{
    static class Colors
    {
        public const ConsoleColorExt Emphasized = ConsoleColorExt.Cyan;
        public const ConsoleColorExt UserInput = ConsoleColorExt.DarkCyan;
        public const ConsoleColorExt Ident = ConsoleColorExt.DarkGray;
        public const ConsoleColorExt Normal = ConsoleColorExt.White;
        public const ConsoleColorExt Success = ConsoleColorExt.Green;
        public const ConsoleColorExt Error = ConsoleColorExt.Red;
                                 
        public const ConsoleColorExt ErrorDetail = ConsoleColorExt.DarkRed;
        public const ConsoleColorExt Transmit = ConsoleColorExt.DarkGreen;
        public const ConsoleColorExt Receive = ConsoleColorExt.DarkRed;
        public const ConsoleColorExt Message = ConsoleColorExt.Gray;

        public const ConsoleColorExt Action = ConsoleColorExt.DarkMagenta;
        public const ConsoleColorExt Debug = ConsoleColorExt.Blue;
        public const ConsoleColorExt Warning = ConsoleColorExt.Yellow;
    }
}
