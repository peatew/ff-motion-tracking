﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace FFTracking_UI.Controls
{
    class TextBoxWithHelpText : TextBox
	{
		private bool helpTextVisible = false;

		private Color helpTextColor = Color.Gray;
		private string helpText = "";

		public Color HelpTextColor
		{
			get { return helpTextColor; }
			set
			{
				helpTextColor = value;
				Invalidate();
			}
		}

		public string HelpText
		{
			get { return helpText; }
			set
			{
				helpText = value;
				Invalidate();
			}
		}

		public TextBoxWithHelpText()
		{
			this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);

			this.TextChanged += new System.EventHandler(this.CheckHelpText);
			this.Leave += new System.EventHandler(this.CheckHelpText);
			this.Enter += new System.EventHandler(this.CheckHelpText);
		}

		protected override void OnCreateControl()
		{
			base.OnCreateControl();

			CheckHelpText(null, null);
		}

		protected override void OnPaint(PaintEventArgs args)
		{
			base.OnPaint(args);

			if (Enabled == false) 
			{
				args.Graphics.Clear(SystemColors.Control); 
			}

			if (helpTextVisible == true)
			{
				using (SolidBrush drawBrush = new SolidBrush(HelpTextColor)) 
				{
					args.Graphics.DrawString(HelpText, Font, drawBrush, new PointF(0.0F, 0.0F));
				}
			}
		}

		private void CheckHelpText(object sender, EventArgs args)
		{
			if (String.IsNullOrEmpty(Text) == true)
			{
				EnableWaterMark();
			}
			else
			{
				DisbaleWaterMark();
			}
		}

		private void EnableWaterMark()
		{
			this.SetStyle(ControlStyles.UserPaint, true);

			this.helpTextVisible = true;

			Refresh();
		}

		private void DisbaleWaterMark()
		{
			this.helpTextVisible = false;

			this.SetStyle(ControlStyles.UserPaint, false);

			Refresh();
		}
	}
}
