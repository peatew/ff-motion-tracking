﻿using System;
using System.Net;
using Rug.Cmd;
using Rug.Host;
using Rug.Osc;

namespace FFTracking_UI.Console
{
    internal class FormReporter : IReporter
    {
        public static FormReporter ClientInstance = new FormReporter();

        public static FormReporter ServerInstance = new FormReporter();

        private object syncLock = new object();

        public IOscMessageFilter OscMessageFilter { get; set; }

        public ReportVerbosity ReportVerbosity { get; set; }

        public readonly FormConsole Console = new FormConsole(); 

        private FormReporter()
        {
        }

        public void OnException(string message, Exception ex)
        {
            lock (syncLock)
            {
                Console.WriteException(message, ex);
            }
        }

        public void OnInfo(ReportType type, string message)
        {
            lock (syncLock)
            {
                switch (type)
                {
                    case ReportType.Title:
                        Console.WriteLine(Colors.Emphasized, message);
                        break;

                    case ReportType.Normal:
                        Console.WriteLine(Colors.Message, message);
                        break;

                    case ReportType.Detail:
                        Console.WriteLine(Colors.Normal, message);
                        break;

                    case ReportType.Action:
                        Console.WriteLine(Colors.Action, message);
                        break;

                    case ReportType.Error:
                        Console.WriteLine(Colors.ErrorDetail, message);
                        break;

                    case ReportType.Success:
                        Console.WriteLine(Colors.Success, message);
                        break;

                    default:
                        break;
                }
            }
        }

        public void OnInfo(ReportType type, string ident, string message)
        {
            lock (syncLock)
            {
                switch (type)
                {
                    case ReportType.Title:
                        Console.WriteMessage(Direction.Action, Colors.Ident, ident, Colors.Emphasized, message);
                        break;

                    case ReportType.Normal:
                        Console.WriteMessage(Direction.Action, Colors.Ident, ident, Colors.Message, message);
                        break;

                    case ReportType.Detail:
                        Console.WriteMessage(Direction.Action, Colors.Ident, ident, Colors.Normal, message);
                        break;

                    case ReportType.Action:
                        Console.WriteMessage(Direction.Action, Colors.Ident, ident, Colors.Action, message);
                        break;

                    case ReportType.Error:
                        Console.WriteMessage(Direction.Action, Colors.Ident, ident, Colors.ErrorDetail, message);
                        break;

                    case ReportType.Success:
                        Console.WriteMessage(Direction.Action, Colors.Ident, ident, Colors.Success, message);
                        break;

                    default:
                        break;
                }
            }
        }

        public void OnInfo(Direction direction, ReportType type, string ident, string message)
        {
            lock (syncLock)
            {
                switch (type)
                {
                    case ReportType.Title:
                        Console.WriteMessage(direction, Colors.Ident, ident, Colors.Emphasized, message);
                        break;

                    case ReportType.Normal:
                        Console.WriteMessage(direction, Colors.Ident, ident, Colors.Message, message);
                        break;

                    case ReportType.Detail:
                        Console.WriteMessage(direction, Colors.Ident, ident, Colors.Normal, message);
                        break;

                    case ReportType.Action:
                        Console.WriteMessage(direction, Colors.Ident, ident, Colors.Action, message);
                        break;

                    case ReportType.Error:
                        Console.WriteMessage(direction, Colors.Ident, ident, Colors.ErrorDetail, message);
                        break;

                    case ReportType.Success:
                        Console.WriteMessage(direction, Colors.Ident, ident, Colors.Success, message);
                        break;

                    default:
                        break;
                }
            }
        }

        public void PrintBlankLine(ReportVerbosity verbosity)
        {
            if (ShouldPrint(verbosity) == false)
            {
                return;
            }

            lock (syncLock)
            {
                Console.WriteLine(Colors.Emphasized, "");
            }
        }

        public void PrintDebug(string format, params object[] args)
        {
            Print(format, args, ReportVerbosity.Debug, Colors.Debug);
        }

        public void PrintDebug(Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {
            Print(direction, endPoint?.ToString(), format, args, ReportVerbosity.Debug, Colors.Debug);
        }

        public void PrintDebug(Direction direction, string ident, string format, params object[] args)
        {
            Print(direction, ident, format, args, ReportVerbosity.Debug, Colors.Debug);
        }

        public void PrintDetail(string format, params object[] args)
        {
            Print(format, args, ReportVerbosity.Detail, Colors.Normal);
        }

        public void PrintDetail(Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {
            Print(direction, endPoint?.ToString(), format, args, ReportVerbosity.Detail, Colors.Normal);
        }

        public void PrintDetail(Direction direction, string ident, string format, params object[] args)
        {
            Print(direction, ident, format, args, ReportVerbosity.Detail, Colors.Normal);
        }


        public void PrintEmphasized(string format, params object[] args)
        {
            Print(format, args, ReportVerbosity.Emphasized, Colors.Emphasized);
        }

        public void PrintEmphasized(Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {
            Print(direction, endPoint?.ToString(), format, args, ReportVerbosity.Emphasized, Colors.Emphasized);
        }

        public void PrintEmphasized(Direction direction, string ident, string format, params object[] args)
        {
            Print(direction, ident, format, args, ReportVerbosity.Emphasized, Colors.Emphasized);
        }

        public void PrintError(string format, params object[] args)
        {
            lock (syncLock)
            {
                if (args.Length == 0)
                {
                    Console.WriteLine(Colors.ErrorDetail, format);
                }
                else
                {
                    Console.WriteLine(Colors.ErrorDetail, string.Format(format, args));
                }
            }
        }

        public void PrintError(Direction direction, IPEndPoint origin, string format, params object[] args)
        {
            lock (syncLock)
            {
                if (args.Length == 0)
                {
                    Console.WriteMessage(direction, Colors.Ident, origin.ToString(), Colors.ErrorDetail, format);
                }
                else
                {
                    Console.WriteMessage(direction, Colors.Ident, origin.ToString(), Colors.ErrorDetail, string.Format(format, args));
                }
            }
        }

        public void PrintError(Direction direction, string ident, string format, params object[] args)
        {
            lock (syncLock)
            {
                if (args.Length == 0)
                {
                    Console.WriteMessage(direction, Colors.Ident, ident, Colors.ErrorDetail, format);
                }
                else
                {
                    Console.WriteMessage(direction, Colors.Ident, ident, Colors.ErrorDetail, string.Format(format, args));
                }
            }
        }

        public void PrintException(Exception ex, string format, params object[] args)
        {
            lock (syncLock)
            {
                if (args.Length == 0)
                {
                    Console.WriteException(format, ex);
                }
                else
                {
                    Console.WriteException(string.Format(format, args), ex);
                }
            }
        }


        public void PrintNormal(string format, params object[] args)
        {
            Print(format, args, ReportVerbosity.Normal, Colors.Normal);
        }

        public void PrintNormal(Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {
            Print(direction, endPoint?.ToString(), format, args, ReportVerbosity.Normal, Colors.Normal);
        }

        public void PrintNormal(Direction direction, string ident, string format, params object[] args)
        {
            Print(direction, ident, format, args, ReportVerbosity.Normal, Colors.Normal);
        }
        
        public void PrintOscPackets(Direction direction, params OscPacket[] packets)
        {
            lock (syncLock)
            {
                foreach (OscPacket packet in packets)
                {
                    if (packet is OscMessage)
                    {
                        PrintOscMessage(direction, packet as OscMessage);
                    }
                    else if (packet is OscBundle)
                    {
                        OscBundle bundle = packet as OscBundle;

                        foreach (OscPacket sub in bundle)
                        {
                            PrintOscPackets(direction, sub);
                        }
                    }
                }
            }
        }

        public void PrintOscPackets(Direction direction, IPEndPoint endPoint, params OscPacket[] packets)
        {
            lock (syncLock)
            {
                foreach (OscPacket packet in packets)
                {
                    if (packet is OscMessage)
                    {
                        PrintOscMessage(direction, endPoint, packet as OscMessage);
                    }
                    else if (packet is OscBundle)
                    {
                        OscBundle bundle = packet as OscBundle;

                        foreach (OscPacket sub in bundle)
                        {
                            PrintOscPackets(direction, endPoint, sub);
                        }
                    }
                }
            }
        }

        public void PrintWarning(ReportVerbosity verbosity, string format, params object[] args)
        {
            if (ShouldPrint(verbosity) == false)
            {
                return;
            }

            lock (syncLock)
            {
                if (args.Length == 0)
                {
                    Console.WriteLine(Colors.Warning, format);
                }
                else
                {
                    Console.WriteLine(Colors.Warning, string.Format(format, args));
                }
            }
        }

        public void PrintWarning(ReportVerbosity verbosity, Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {
            if (ShouldPrint(verbosity) == false)
            {
                return;
            }

            lock (syncLock)
            {
                if (args.Length == 0)
                {
                    Console.WriteMessage(direction, Colors.Ident, endPoint.ToString(), Colors.Warning, format);
                }
                else
                {
                    Console.WriteMessage(direction, Colors.Ident, endPoint.ToString(), Colors.Warning, string.Format(format, args));
                }
            }
        }

        public void PrintWarning(ReportVerbosity verbosity, Direction direction, string ident, string format, params object[] args)
        {
            if (ShouldPrint(verbosity) == false)
            {
                return;
            }

            lock (syncLock)
            {
                if (args.Length == 0)
                {
                    Console.WriteMessage(direction, Colors.Ident, ident, Colors.Warning, format);
                }
                else
                {
                    Console.WriteMessage(direction, Colors.Ident, ident, Colors.Warning, string.Format(format, args));
                }
            }
        }    

        private void Print(string format, object[] args, ReportVerbosity reportVerbosity, ConsoleColorExt color)
        {
            if (ShouldPrint(reportVerbosity) == false)
            {
                return;
            }

            /* 
            lock (syncLock)
            {
                if (args.Length == 0)
                {
                    Cmd.WriteLine(color, format);
                }
                else
                {
                    Cmd.WriteLine(color, string.Format(format, args));
                }
            }
            */ 

            lock (syncLock)
            {
                if (args.Length == 0)
                {
                    Console.WriteMessage(Direction.Action, Colors.Ident, "", color, format);
                }
                else
                {
                    Console.WriteMessage(Direction.Action, Colors.Ident, "", color, string.Format(format, args));
                }
            }
        }

        private void Print(Direction direction, string ident, string format, object[] args, ReportVerbosity reportVerbosity, ConsoleColorExt color)
        {
            if (ShouldPrint(reportVerbosity) == false)
            {
                return;
            }

            lock (syncLock)
            {
                if (args.Length == 0)
                {
                    Console.WriteMessage(direction, Colors.Ident, ident, color, format);
                }
                else
                {
                    Console.WriteMessage(direction, Colors.Ident, ident, color, string.Format(format, args));
                }
            }
        }

        private void PrintOscMessage(Direction direction, OscMessage oscMessage)
        {
            if (OscMessageFilter?.ShouldPrintMessage(oscMessage) == false)
            {
                return;
            }

            Console.WriteMessage(direction, Colors.Ident, oscMessage.Origin.ToString(), Colors.Message, oscMessage.ToString());
        }

        private void PrintOscMessage(Direction direction, IPEndPoint endPoint, OscMessage oscMessage)
        {
            if (OscMessageFilter?.ShouldPrintMessage(oscMessage) == false)
            {
                return;
            }

            Console.WriteMessage(direction, Colors.Ident, endPoint.ToString(), Colors.Message, oscMessage.ToString());
        }

        private bool ShouldPrint(ReportVerbosity verbosity)
        {
            return (int)ReportVerbosity <= (int)verbosity;
        }
    }
}