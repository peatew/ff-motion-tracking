﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Rug.LiteGL.Controls;
using Rug.Osc;

namespace FFTracking_UI.DialogsAndWindows
{
    partial class GraphWindow : BaseForm, IGraphWindow
    {
        private Config.GraphWindow plot;
        private object syncLock = new object();
        private Dictionary<Config.GraphWindow.Trace, OscMessageEvent> variableEvents = new Dictionary<Config.GraphWindow.Trace, OscMessageEvent>();

        private List<Graph.Trace> traces = new List<GraphBase.Trace>();
        private List<ToolStripMenuItem> trackItems = new List<ToolStripMenuItem>();

        public GraphWindow()
        {
            InitializeComponent();
        }

        public void SetGraph(Config.GraphWindow plot)
        {
            WindowID = plot.Name;
            Text = plot.Name;
            graph.YAxisLabel = plot.YAxisLabel;

            graph.ShowYAxisTitle = !string.IsNullOrEmpty(graph.YAxisLabel);

            graph.AxesRange = plot.AxesRange;
            graph.Rolling = plot.Rolling;
            horizontalRollToolStripMenuItem.Checked = graph.Rolling; 

            int index = 0;
            foreach (Config.GraphWindow.Trace trace in plot.Traces)
            {
                trace.TraceIndex = index++;
                traces.Add(new Graph.Trace() { Color = trace.Color, Name = trace.Label, MaxDataPoints = trace.MaxDataPoints, BlockSize = trace.BlockSize });
            }

            graph.AddTraces(traces.ToArray());

            lock (syncLock)
            {
                foreach (Config.GraphWindow.Trace trace in plot.Traces)
                {
                    OscMessageEvent changedEvent = null;

                    changedEvent = delegate (OscMessage message) { AddScopeData(trace, message); };

                    variableEvents.Add(trace, changedEvent);

                    trace.MessageEvent += changedEvent;
                }
            }

            trackItems.Add(trackingToolStripMenuItem.DropDownItems[0] as ToolStripMenuItem);

            index = 0;
            foreach (Graph.Trace trace in traces)
            {
                ToolStripMenuItem centerItem = new ToolStripMenuItem(trace.Name, null, CenterItem_Click, (Keys)((int)Shortcut.Ctrl1 + index))
                {
                    Tag = index,
                };

                centerTraceToolStripMenuItem.DropDownItems.Add(centerItem);

                ToolStripMenuItem trackItem = new ToolStripMenuItem(trace.Name, null, TrackItem_Click, (Keys)((int)Shortcut.Alt1 + index))
                {
                    Tag = index,
                };

                trackItems.Add(trackItem);
                trackingToolStripMenuItem.DropDownItems.Add(trackItem);

                index++;
            }
        }

        public void Clear()
        {
            graph.Clear();
        }

        private void AddScopeData(Config.GraphWindow.Trace trace, OscMessage message)
        {
            if (graph == null)
            {
                return;
            }

            if (trace == null)
            {
                return;
            }

            try
            {
                if (message.Count <= trace.Argument)
                {
                    return;
                }

                object valueObj = message[trace.Argument];

                if ((valueObj is float) == false)
                {
                    return;
                }

                graph.AddScopeData(trace.TraceIndex, (float)valueObj);
            }
            catch { }
        }

        private void GraphWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            lock (syncLock)
            {
                foreach (Config.GraphWindow.Trace trace in variableEvents.Keys)
                {
                    trace.MessageEvent -= variableEvents[trace];
                }
            }
        }


        private void CenterAll_Click(object sender, EventArgs e)
        {
            CenterOnTrace(-1);
        }

        private void CenterItem_Click(object sender, EventArgs e)
        {
            CenterOnTrace((int)(sender as ToolStripMenuItem).Tag);
        }

        private void TrackItem_Click(object sender, EventArgs e)
        {
            SetActiveTrackIndex((int)(sender as ToolStripMenuItem).Tag);
        }


        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if ((keyData & Keys.Control) == Keys.Control)
            {
                for (int i = 0; i < traces.Count + 1; i++)
                {
                    if ((keyData & (Keys)((int)Keys.D0 + i)) == (Keys)((int)Keys.D0 + i))
                    {
                        CenterOnTrace(i - 1);
                    }
                }
            }

            if ((keyData & Keys.Alt) == Keys.Alt)
            {
                if ((keyData & Keys.Back) == Keys.Back)
                {
                    SetActiveTrackIndex(-2);
                    graph.ResetView();
                }

                for (int i = 0; i < traces.Count + 1; i++)
                {
                    if ((keyData & (Keys)((int)Keys.D0 + i)) == (Keys)((int)Keys.D0 + i))
                    {
                        SetActiveTrackIndex(i - 1);
                    }
                }
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void SetActiveTrackIndex(int index)
        {
            if (graph.TrackTraceIndex == index && graph.TrackTrace == true)
            {
                index = -2;
            }

            for (int i = 0; i < trackItems.Count; i++)
            {
                trackItems[i].Checked = i == index + 1;
            }

            graph.TrackTraceIndex = index;
            graph.TrackTrace = index > -2;
        }

        private void CenterOnTrace(int i)
        {
            SetActiveTrackIndex(-2);
            graph.CenterOnTrace(i);
        }


        private void horizonatalZoomInMenuItem_Click(object sender, EventArgs e)
        {
            graph.ZoomGraph(-0.1f, 0);
        }

        private void horizonatalZoomOutMenuItem_Click(object sender, EventArgs e)
        {
            graph.ZoomGraph(0.1f, 0);
        }

        private void resetViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetActiveTrackIndex(-2);
            graph.ResetView();
        }

        private void scrollDownToolStripMenuItem_Click(object sender, EventArgs e)
        {
            graph.ScrollGraph(0, -0.1f);
        }

        private void scrollLeftToolStripMenuItem_Click(object sender, EventArgs e)
        {
            graph.ScrollGraph(-0.1f, 0f);
        }

        private void scrollUpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            graph.ScrollGraph(0, 0.1f);
        }

        private void verticalZoomInMenuItem_Click(object sender, EventArgs e)
        {
            graph.ZoomGraph(0, -0.1f);
        }

        private void verticalZoomOutMenuItem_Click(object sender, EventArgs e)
        {
            graph.ZoomGraph(0, 0.1f);
        }

        private void xScrollRightToolStripMenuItem_Click(object sender, EventArgs e)
        {
            graph.ScrollGraph(0.1f, 0f);
        }

        private void allToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetActiveTrackIndex(-1);
        }

        private void horizontalRollToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            graph.Rolling = horizontalRollToolStripMenuItem.Checked;

            if (graph.Rolling == false)
            {
                graph.Clear();
                graph.ResetView();
            }
        }

        private void graph_UserChangedViewport(Rug.LiteGL.Controls.GraphBase graph, bool xChanged, bool yChanged)
        {
            SetActiveTrackIndex(-2);
        }
    }
}