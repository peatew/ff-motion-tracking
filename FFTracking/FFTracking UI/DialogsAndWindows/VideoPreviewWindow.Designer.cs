﻿namespace FFTracking_UI
{
    partial class VideoPreviewWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.imagePreviewPanel1 = new FFTracking_UI.Panels.ImagePreviewPanel();
            this.SuspendLayout();
            // 
            // imagePreviewPanel1
            // 
            this.imagePreviewPanel1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.imagePreviewPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imagePreviewPanel1.Location = new System.Drawing.Point(0, 0);
            this.imagePreviewPanel1.Name = "imagePreviewPanel1";
            this.imagePreviewPanel1.Size = new System.Drawing.Size(483, 421);
            this.imagePreviewPanel1.TabIndex = 0;
            // 
            // VideoPreviewWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(483, 421);
            this.Controls.Add(this.imagePreviewPanel1);
            this.Name = "VideoPreviewWindow";
            this.Text = "VideoPreviewWindow";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VideoPreviewWindow_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private Panels.ImagePreviewPanel imagePreviewPanel1;
    }
}