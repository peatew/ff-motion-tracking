﻿using System;
using System.Windows.Forms;
using Rug.Host.Comms;

namespace FFTracking_UI.DialogsAndWindows
{
    public partial class SearchForServerDialog : Form
    {
        private static readonly string[] ellipseAnimationFrames = new string[]
        {
            "    ",
            ".   ",
            "..  ",
            "... ",
            " ...",
            "  ..",
            "   .",
            "    "
        };

        private SearchForConnections autoConnector;
        private int ellipseAnimationFrameIndex = 0;
        private int numberOfUdpConnections;

        public OscConnectionInfo Info { get; private set; }

        public SearchForServerDialog()
        {
            InitializeComponent();
        }

        private void AutoConnector_DeviceDiscovered(OscConnectionInfo obj)
        {
            if (InvokeRequired == true)
            {
                BeginInvoke(new Action<OscConnectionInfo>(OnDeviceDiscovered), obj);
            }
            else
            {
                OnDeviceDiscovered(obj);
            }
        }

        private void AutoConnector_DeviceExpired(OscConnectionInfo obj)
        {
            if (InvokeRequired == true)
            {
                BeginInvoke(new Action<OscConnectionInfo>(OnDeviceExpired), obj);
            }
            else
            {
                OnDeviceExpired(obj);
            }
        }

        private void SearchingForConnectionsDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            // make sure you disconnect the event to avoid erroneous rows getting added to the disposed grid view
            autoConnector.DeviceDiscovered -= AutoConnector_DeviceDiscovered;
            autoConnector.DeviceExpired -= AutoConnector_DeviceExpired;

            ellipseAnimationTimer_Tick(null, EventArgs.Empty);

            autoConnector.Dispose();
        }

        private void SearchingForConnectionsDialog_Load(object sender, EventArgs e)
        {
            autoConnector = new SearchForConnections();

            autoConnector.DeviceDiscovered += AutoConnector_DeviceDiscovered;
            autoConnector.DeviceExpired += AutoConnector_DeviceExpired;

            ellipseAnimationTimer.Enabled = true;

            autoConnector.BeginSearch();
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;

            Close();
        }

        private void Connect_Click(object sender, EventArgs e)
        {
            if (hostsGridView.SelectedRows.Count == 0)
            {
                System.Media.SystemSounds.Exclamation.Play();

                return;
            }

            DataGridViewRow row = hostsGridView.SelectedRows[0];

            Info = row.Tag as OscConnectionInfo;

            DialogResult = System.Windows.Forms.DialogResult.OK;

            Close();
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter ||
                e.KeyCode == Keys.Return)
            {
                Connect_Click(sender, EventArgs.Empty);
            }
        }

        private void ellipseAnimationTimer_Tick(object sender, EventArgs e)
        {
            ellipseAnimationFrameIndex = ++ellipseAnimationFrameIndex % ellipseAnimationFrames.Length;

            Text = "Searching For Connections " + ellipseAnimationFrames[ellipseAnimationFrameIndex];
        }

        private void Connections_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }

            Connect_Click(sender, EventArgs.Empty);
        }

        private void OnDeviceDiscovered(OscConnectionInfo info)
        {
            DataGridViewRow row = new DataGridViewRow() { Tag = info };

            row.CreateCells(hostsGridView, info.Descriptor, GetNetworkAdapterOrFileName(info), info.Address, info.ReceivePort);

            hostsGridView.Rows.Add(row);

            if (hostsGridView.Rows.Count == 1)
            {
                hostsGridView.ClearSelection();
                hostsGridView.Rows[0].Selected = true;
            }

            numberOfUdpConnections++;

            numberOfConnectionsLabel.Text = string.Format(numberOfConnectionsLabel.Tag.ToString(), numberOfUdpConnections);

            if (Options.OpenConnectionToFirstDeviceFound == true)
            {
                Connect_Click(null, EventArgs.Empty);
            }
        }

        private string GetNetworkAdapterOrFileName(OscConnectionInfo info)
        {
            if (info is OscConnectionInfo)
            {
                return (info as OscConnectionInfo).NetworkAdapterIPAddress.ToString();
            }
            else
            {
                return ""; 
            }
        }

        private void OnDeviceExpired(OscConnectionInfo info)
        {
            DataGridViewRow foundRow = null;

            foreach (DataGridViewRow row in hostsGridView.Rows)
            {
                if (info.Equals(row.Tag as OscConnectionInfo) == false)
                {
                    continue;
                }

                foundRow = row;
            }

            if (foundRow == null)
            {
                return;
            }

            hostsGridView.Rows.Remove(foundRow);

            if (foundRow.Selected == true)
            {
                if (hostsGridView.Rows.Count > 0)
                {
                    hostsGridView.ClearSelection();
                    hostsGridView.Rows[0].Selected = true;
                }
            }

            numberOfUdpConnections--;

            numberOfConnectionsLabel.Text = string.Format(numberOfConnectionsLabel.Tag.ToString(), numberOfUdpConnections);
        }
    }
}
