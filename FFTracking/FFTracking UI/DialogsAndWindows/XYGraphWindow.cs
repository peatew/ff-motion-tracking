﻿using System.Collections.Generic;
using System.Windows.Forms;
using OpenTK;
using Rug.LiteGL.Controls;
using Rug.Osc;

namespace FFTracking_UI.DialogsAndWindows
{
    partial class XYGraphWindow : BaseForm, IGraphWindow
    {
        private Config.GraphWindow plot;
        private object syncLock = new object();
        private Dictionary<Config.GraphWindow.Trace, OscMessageEvent> variableEvents = new Dictionary<Config.GraphWindow.Trace, OscMessageEvent>();

        public XYGraphWindow()
        {
            InitializeComponent();
        }

        public void SetGraph(Config.GraphWindow plot)
        {
            WindowID = plot.Name;
            Text = plot.Name;

            graph1.XAxisLabel = plot.XAxisLabel;
            graph1.YAxisLabel = plot.YAxisLabel;

            graph1.ShowXAxisTitle = !string.IsNullOrEmpty(graph1.XAxisLabel);
            graph1.ShowYAxisTitle = !string.IsNullOrEmpty(graph1.YAxisLabel);

            graph1.AxesRange = plot.AxesRange;

            List<Graph.Trace> traces = new List<Graph.Trace>();

            int index = 0;
            foreach (Config.GraphWindow.Trace trace in plot.Traces)
            {
                trace.TraceIndex = index++;
                traces.Add(new Graph.Trace() { Color = trace.Color, Name = trace.Label, MaxDataPoints = trace.MaxDataPoints, BlockSize = trace.BlockSize });
            }

            graph1.AddTraces(traces.ToArray());

            lock (syncLock)
            {
                foreach (Config.GraphWindow.Trace trace in plot.Traces)
                {
                    OscMessageEvent changedEvent = null;

                    changedEvent = delegate (OscMessage message) { AddScopeData(trace, message); };

                    variableEvents.Add(trace, changedEvent);

                    trace.MessageEvent += changedEvent;
                }
            }
        }

        public void Clear()
        {
            graph1.Clear();
        }

        private void AddScopeData(Config.GraphWindow.Trace trace, OscMessage message)
        {
            if (graph1 == null)
            {
                return;
            }

            if (trace == null)
            {
                return;
            }

            try
            {
                if (message.Count <= trace.Argument + 1)
                {
                    return;
                }

                object valueObjX = message[trace.Argument];

                if ((valueObjX is float) == false)
                {
                    return;
                }

                object valueObjY = message[trace.Argument + 1];

                if ((valueObjY is float) == false)
                {
                    return;
                }

                graph1.AddScopeData(trace.TraceIndex, new Vector2((float)valueObjX, (float)valueObjY));
            }
            catch { }
        }

        private void GraphWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            lock (syncLock)
            {
                foreach (Config.GraphWindow.Trace trace in variableEvents.Keys)
                {
                    trace.MessageEvent -= variableEvents[trace];
                }
            }
        }
    }
}