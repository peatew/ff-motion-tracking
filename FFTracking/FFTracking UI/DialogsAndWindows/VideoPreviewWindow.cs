﻿using System.Drawing;
using System.Windows.Forms;
using FFTracking_UI.Config;
using FFTracking_UI.DialogsAndWindows;
using Rug.Host;

namespace FFTracking_UI
{
    partial class VideoPreviewWindow : BaseForm
    {
        ImageDebugWindow imageDebugWindow; 

        public VideoPreviewWindow()
        {
            InitializeComponent();
        }

        public void SetDebugger(ImageDebugWindow imageDebugWindow)
        {
            this.imageDebugWindow = imageDebugWindow; 

            WindowID = imageDebugWindow.Name;
            Text = "Image Preview: " + WindowID; 

            imageDebugWindow.NewDebugFrame += ImageDebugWindow_NewDebugFrame;
        }

        private void ImageDebugWindow_NewDebugFrame(IDebugImageSource source, Bitmap bitmap)
        {
            imagePreviewPanel1.CopyToImage(bitmap);
        }

        private void VideoPreviewWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            imageDebugWindow.NewDebugFrame -= ImageDebugWindow_NewDebugFrame;
        }
    }
}
