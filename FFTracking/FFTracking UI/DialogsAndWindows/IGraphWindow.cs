﻿namespace FFTracking_UI.DialogsAndWindows
{
    interface IGraphWindow
    {
        string WindowID { get; set; }

        event System.Windows.Forms.FormClosedEventHandler FormClosed;

        void Close();
        void SetGraph(Config.GraphWindow graph);
        void Show();
    }
}
