﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace FFTracking_UI.DialogsAndWindows
{
    public partial class BaseForm : Form
    {
        public string WindowID { get; set; } = string.Empty;

        public BaseForm()
        {
            InitializeComponent();
        }

        private void BaseForm_Load(object sender, EventArgs e)
        {
            if (Options.Windows[WindowID].Bounds != Rectangle.Empty)
            {
                this.DesktopBounds = Options.Windows[WindowID].Bounds;
            }

            WindowState = Options.Windows[WindowID].WindowState;

            Options.Windows[WindowID].IsOpen = true;
        }

        private void BaseForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing || e.CloseReason == CloseReason.None)
            {
                Options.Windows[WindowID].IsOpen = false;
            }
        }

        private void BaseForm_Resize(object sender, EventArgs e)
        {
            if (WindowState != FormWindowState.Minimized)
            {
                Options.Windows[WindowID].WindowState = WindowState;
            }
        }

        private void BaseForm_ResizeBegin(object sender, EventArgs e)
        {

        }

        private void BaseForm_ResizeEnd(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                Options.Windows[WindowID].Bounds = this.DesktopBounds;
            }
        }
    }
}
