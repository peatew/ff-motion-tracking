﻿using System;
using System.Drawing;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Windows.Forms;
using Rug.Host.Comms;

namespace FFTracking_UI.DialogsAndWindows
{
    public partial class DirectConnectionDialog : Form
    {
        private IPAddress ipAddress;
        private bool ipAddressOK;

        private ushort receivePort;
        private bool receivePortOK;

        private ushort sendPort;
        private bool sendPortOK;

        public OscConnectionInfo Info { get; private set; }

        public DirectConnectionDialog()
        {
            InitializeComponent();
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;

            Close();
        }

        private void Connect_Click(object sender, EventArgs e)
        {
            if (ipAddressOK == false || sendPortOK == false)
            {
                return;
            }

            IPAddress intefaceIP = ((IntefaceInfo)networkInterface.Items[networkInterface.SelectedIndex]).IpAddress;

            Info = new OscConnectionInfo("Direct Connection", intefaceIP, ipAddress, sendPort, receivePort);

            DialogResult = DialogResult.OK;
        }

        private void DirectConnectionDialog_Load(object sender, EventArgs e)
        {
            networkInterface.Items.Clear();

            networkInterface.Items.Add(new IntefaceInfo() { NetworkInterface = null, String = "All Adapters", InterfaceName = "All Adapters", IpAddress = IPAddress.Any });

            networkInterface.Items.Add(new IntefaceInfo() { NetworkInterface = null, String = "All Adapters IPv6", InterfaceName = "All Adapters IPv6", IpAddress = IPAddress.IPv6Any });

            NetworkInterface[] interfaces = NetworkInterface.GetAllNetworkInterfaces();

            foreach (NetworkInterface adapter in interfaces)
            {
                var ipProps = adapter.GetIPProperties();

                foreach (var ip in ipProps.UnicastAddresses)
                {
                    if (ip.Address.AddressFamily == AddressFamily.InterNetwork)
                    {
                        networkInterface.Items.Add(new IntefaceInfo() { NetworkInterface = adapter, String = adapter.Description.ToString() + " (" + ip.Address.ToString() + ")", InterfaceName = adapter.Description.ToString(), IpAddress = ip.Address });
                    }
                }
            }

            networkInterface.SelectedIndex = 0;
        }

        private void IPTextBox_TextChanged(object sender, EventArgs e)
        {
            IPTextBox.ForeColor = Control.DefaultForeColor;

            IPAddress address;

            if (IPAddress.TryParse(IPTextBox.Text, out address) == false)
            {
                IPTextBox.ForeColor = Color.Red;
                ipAddressOK = false;
            }
            else
            {
                ipAddress = address;
                ipAddressOK = true;
            }
        }

        private void ReceivePortTextBox_TextChanged(object sender, EventArgs e)
        {
            ReceivePortTextBox.ForeColor = Control.DefaultForeColor;

            ushort port;

            if (ushort.TryParse(ReceivePortTextBox.Text, out port) == false ||
                port == 0)
            {
                ReceivePortTextBox.ForeColor = Color.Red;
                receivePortOK = false;
            }
            else
            {
                this.receivePort = port;
                receivePortOK = true;
            }
        }

        private void SendPortTextBox_TextChanged(object sender, EventArgs e)
        {
            SendPortTextBox.ForeColor = Control.DefaultForeColor;

            ushort port;

            if (ushort.TryParse(SendPortTextBox.Text, out port) == false ||
                port == 0)
            {
                SendPortTextBox.ForeColor = Color.Red;
                sendPortOK = false;
            }
            else
            {
                this.sendPort = port;
                sendPortOK = true;
            }
        }

        private struct IntefaceInfo
        {
            public string InterfaceName;
            public IPAddress IpAddress;
            public NetworkInterface NetworkInterface;
            public string String;

            public override string ToString()
            {
                return String;
            }
        }
    }
}