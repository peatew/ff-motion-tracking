﻿namespace FFTracking_UI.DialogsAndWindows
{
    partial class DirectConnectionDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cancelButton = new System.Windows.Forms.Button();
            this.connectButton = new System.Windows.Forms.Button();
            this.IPTextBox = new FFTracking_UI.Controls.TextBoxWithHelpText();
            this.SendPortTextBox = new FFTracking_UI.Controls.TextBoxWithHelpText();
            this.networkInterface = new System.Windows.Forms.ComboBox();
            this.ReceivePortTextBox = new FFTracking_UI.Controls.TextBoxWithHelpText();
            this.SuspendLayout();
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(226, 103);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 6;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // connectButton
            // 
            this.connectButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.connectButton.Location = new System.Drawing.Point(145, 103);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(75, 23);
            this.connectButton.TabIndex = 5;
            this.connectButton.Text = "Connect";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.Connect_Click);
            // 
            // IPTextBox
            // 
            this.IPTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IPTextBox.HelpText = "IP Address";
            this.IPTextBox.HelpTextColor = System.Drawing.Color.Gray;
            this.IPTextBox.Location = new System.Drawing.Point(12, 49);
            this.IPTextBox.Name = "IPTextBox";
            this.IPTextBox.Size = new System.Drawing.Size(208, 20);
            this.IPTextBox.TabIndex = 2;
            this.IPTextBox.TextChanged += new System.EventHandler(this.IPTextBox_TextChanged);
            // 
            // SendPortTextBox
            // 
            this.SendPortTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SendPortTextBox.HelpText = "Send Port";
            this.SendPortTextBox.HelpTextColor = System.Drawing.Color.Gray;
            this.SendPortTextBox.Location = new System.Drawing.Point(226, 49);
            this.SendPortTextBox.Name = "SendPortTextBox";
            this.SendPortTextBox.Size = new System.Drawing.Size(73, 20);
            this.SendPortTextBox.TabIndex = 3;
            this.SendPortTextBox.TextChanged += new System.EventHandler(this.SendPortTextBox_TextChanged);
            // 
            // networkInterface
            // 
            this.networkInterface.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.networkInterface.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.networkInterface.FormattingEnabled = true;
            this.networkInterface.Location = new System.Drawing.Point(12, 12);
            this.networkInterface.Name = "networkInterface";
            this.networkInterface.Size = new System.Drawing.Size(287, 21);
            this.networkInterface.TabIndex = 1;
            // 
            // ReceivePort
            // 
            this.ReceivePortTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ReceivePortTextBox.HelpText = "Receive Port";
            this.ReceivePortTextBox.HelpTextColor = System.Drawing.Color.Gray;
            this.ReceivePortTextBox.Location = new System.Drawing.Point(226, 75);
            this.ReceivePortTextBox.Name = "ReceivePort";
            this.ReceivePortTextBox.Size = new System.Drawing.Size(73, 20);
            this.ReceivePortTextBox.TabIndex = 4;
            this.ReceivePortTextBox.TextChanged += new System.EventHandler(this.ReceivePortTextBox_TextChanged);
            // 
            // DirectConnectionDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(312, 138);
            this.Controls.Add(this.ReceivePortTextBox);
            this.Controls.Add(this.networkInterface);
            this.Controls.Add(this.SendPortTextBox);
            this.Controls.Add(this.IPTextBox);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.connectButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DirectConnectionDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Connect To Server";
            this.Load += new System.EventHandler(this.DirectConnectionDialog_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button connectButton;
        private Controls.TextBoxWithHelpText IPTextBox;
        private Controls.TextBoxWithHelpText SendPortTextBox;
        private System.Windows.Forms.ComboBox networkInterface;
        private Controls.TextBoxWithHelpText ReceivePortTextBox;
    }
}