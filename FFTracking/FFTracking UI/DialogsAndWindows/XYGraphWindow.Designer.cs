﻿namespace FFTracking_UI.DialogsAndWindows
{
    partial class XYGraphWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.graph1 = new Rug.LiteGL.Controls.XYGraph();
            this.SuspendLayout();
            // 
            // graph1
            // 
            this.graph1.AxisLabelFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.graph1.AxisValueColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(158)))), ((int)(((byte)(158)))));
            this.graph1.AxisValueMinorColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.graph1.BackColor = System.Drawing.Color.Black;
            this.graph1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.graph1.GraphInset = new System.Windows.Forms.Padding(64, 16, 16, 64);
            this.graph1.Location = new System.Drawing.Point(0, 0);
            this.graph1.LockXMouse = false;
            this.graph1.LockYMouse = false;
            this.graph1.Name = "graph1";
            this.graph1.ShowLegend = true;
            this.graph1.ShowXAxisTitle = true;
            this.graph1.ShowXAxisUnits = true;
            this.graph1.ShowYAxisTitle = true;
            this.graph1.ShowYAxisUnits = true;
            this.graph1.Size = new System.Drawing.Size(284, 261);
            this.graph1.TabIndex = 0;
            this.graph1.XAxisLabel = "";
            this.graph1.YAxisLabel = "";
            this.graph1.ZoomFromXMin = false;
            this.graph1.ZoomXAndYTogether = true;
            // 
            // XYGraphWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.graph1);
            this.Name = "XYGraphWindow";
            this.Text = "Graph";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.GraphWindow_FormClosed);
            this.ResumeLayout(false);

        }

        #endregion

        private Rug.LiteGL.Controls.XYGraph graph1;
    }
}