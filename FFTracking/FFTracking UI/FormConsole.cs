﻿using System;
using Rug.Cmd;
using Rug.Host;
using Rug.Osc;

namespace FFTracking_UI.Console
{
    internal class FormConsole
    {
        private object syncLock = new object();

        public bool ShowFullErrors { get; set; }

        public IConsole Console { get; set; } 

        public FormConsole()
        {
            Console = Rug.Cmd.RC.App; 

            ShowFullErrors = true;
        }

        public static string GetMemStringFromBytes(long bytes, bool space)
        {
            decimal num = bytes / 1024M;
            string str = "KB";
            while (num >= 1000M)
            {
                if (str == "KB")
                {
                    num /= 1024M;
                    str = "MB";
                }
                else
                {
                    if (str == "MB")
                    {
                        num /= 1024M;
                        str = "GB";
                        continue;
                    }
                    if (str == "GB")
                    {
                        num /= 1024M;
                        str = "TB";
                        continue;
                    }
                    if (str == "TB")
                    {
                        num /= 1024M;
                        str = "PB";
                        continue;
                    }
                    if (str == "PB")
                    {
                        num /= 1024M;
                        str = "XB";
                        continue;
                    }
                    if (str == "XB")
                    {
                        num /= 1024M;
                        str = "ZB";
                        continue;
                    }
                    if (str == "ZB")
                    {
                        num /= 1024M;
                        str = "YB";
                        continue;
                    }
                    if (str == "YB")
                    {
                        num /= 1024M;
                        str = "??";
                        continue;
                    }
                    num /= 1024M;
                }
            }
            return (num.ToString("N2") + (space ? (" " + str) : str));
        }

        public static string GetMemStringFromBytes(long bytes, int maxLength)
        {
            return GetMemStringFromBytes(bytes, false).PadLeft(maxLength, ' ');
        }

        public static string GetMemStringFromBytes(long bytes, int maxLength, bool space)
        {
            return GetMemStringFromBytes(bytes, space).PadLeft(maxLength, ' ');
        }

        public static string MaxLength(string str, int max)
        {
            if (str.Length > max)
            {
                if (max - 3 > str.Length)
                {
                    return "...";
                }
                else
                {
                    return str.Substring(0, max - 3) + "...";
                }
            }
            else
            {
                return str;
            }
        }

        public static string MaxLengthLeftPadded(string str, int totalWidth, char paddingChar, string appendIfCut)
        {
            if (str.Length > totalWidth)
            {
                return (str.Substring(0, totalWidth - appendIfCut.Length) + appendIfCut);
            }
            return str.PadLeft(totalWidth, paddingChar);
        }

        public static string MaxLengthPadded(string str, int totalWidth, char paddingChar, string appendIfCut)
        {
            if (str.Length > totalWidth)
            {
                return (str.Substring(0, totalWidth - appendIfCut.Length) + appendIfCut);
            }
            return str.PadRight(totalWidth, paddingChar);
        }

        public void Write(ConsoleColorExt color, string str)
        {
            lock (syncLock)
            {
                Console.Write(color, str);
            }
        }

        public void WriteError(string message)
        {
            lock (syncLock)
            {
                Console.WriteLine(Colors.Error, message);
            }
        }

        public void WriteException(Exception ex)
        {
            if (ShowFullErrors == true)
            {
                lock (syncLock)
                {
                    WriteException_Inner(ex);
                }
            }
            else
            {
                WriteError(ex.Message);
            }
        }

        public void WriteException(string message, Exception ex)
        {
            lock (syncLock)
            {
                Console.WriteLine(Colors.Error, message);

                if (ShowFullErrors == true)
                {
                    WriteException_Inner(ex);
                }
                else
                {
                    Console.WriteLine(Colors.ErrorDetail, ex.Message);
                }
            }
        }

        public void WriteInfo(string message)
        {
            lock (syncLock)
            {
                Console.WriteLine(Colors.Success, message);
            }
        }

        public void WriteInfo(string message, string detail)
        {
            lock (syncLock)
            {
                if (ShowFullErrors == true &&
                    String.IsNullOrEmpty(detail) == false)
                {
                    Console.Write(Colors.Success, message);
                    Console.WriteLine(Colors.Normal, " (" + detail + ")");
                }
                else
                {
                    Console.WriteLine(Colors.Success, message);
                }
            }
        }

        /// <summary>
        /// Write a 2 part item line to the console with colors.
        /// </summary>
        /// <param name="color1">Color to write part 1 with.</param>
        /// <param name="str1">String to write.</param>
        /// <param name="color2">Color to write part 2 with.</param>
        /// <param name="str2">String to write.</param>
        public void WriteItem(ConsoleColorExt color1, string str1, ConsoleColorExt color2, string str2)
        {
            lock (syncLock)
            {
                WriteItem_Inner(color1, str1, color2, str2);
            }
        }

        public void WriteItem(ConsoleColorExt color1, string str1, int max, ConsoleColorExt dotColor, ConsoleColorExt color2, string str2)
        {
            lock (syncLock)
            {
                WriteItem_Inner(color1, str1, max, dotColor, color2, str2);
            }
        }

        public void WriteLine()
        {
            lock (syncLock)
            {
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Write a line to the console with colors.
        /// </summary>
        /// <param name="color">Color to write with.</param>
        /// <param name="str">String to write.</param>
        public void WriteLine(ConsoleColorExt color, string str)
        {
            lock (syncLock)
            {
                Console.WriteLine(color, str);
            }
        }

        /// <summary>
        /// Write a line to the console with colors.
        /// </summary>
        /// <param name="color">Color to write with.</param>
        /// <param name="str">String to write.</param>
        /// <param name="maxLength">The maximum length.</param>
        public void WriteLine(ConsoleColorExt color, string str, int maxLength)
        {
            lock (syncLock)
            {
                Console.WriteLine(color, MaxLengthPadded(str, maxLength, ' ', " .."));
            }
        }

        public void WriteMessage(Direction direction, OscTimeTag? timeTag, string message)
        {
            WriteMessage(direction, timeTag, Colors.Message, message, -1);
        }

        public void WriteMessage(Direction direction, OscTimeTag? timeTag, ConsoleColorExt messageColor, string message)
        {
            WriteMessage(direction, timeTag, messageColor, message, -1);
        }

        public void WriteMessage(Direction direction, OscTimeTag? timeTag, string message, int maxLength)
        {
            WriteMessage(direction, timeTag, Colors.Message, message, maxLength);
        }

        public void WriteMessage(Direction direction, OscTimeTag? timeTag, ConsoleColorExt messageColor, string message, int maxLength)
        {
            int length = 0;
            lock (syncLock)
            {
                switch (direction)
                {
                    case Direction.Transmit:
                        Console.Write(Colors.Transmit, "TX ");
                        length += 3;
                        break;

                    case Direction.Receive:
                        Console.Write(Colors.Receive, "RX ");
                        length += 3;
                        break;

                    case Direction.Action:
                        Console.Write(Colors.Action, "// ");
                        length += 3;
                        break;

                    default:
                        break;
                }

                if (timeTag != null)
                {
                    string timeTagString = timeTag.ToString() + " ";
                    Console.Write(Colors.Normal, timeTagString);

                    length += timeTagString.Length;
                }

                if (maxLength > 0)
                {
                    Console.WriteLine(messageColor, MaxLength(message, maxLength - length));
                }
                else
                {
                    Console.WriteLine(messageColor, message);
                }
            }
        }

        public void WriteMessage(Direction direction, ConsoleColorExt prefixColor, string prefix, ConsoleColorExt messageColor, string message)
        {
            WriteMessage(direction, prefixColor, prefix, messageColor, message, -1);
        }

        public void WriteMessage(Direction direction, ConsoleColorExt prefixColor, string prefix, ConsoleColorExt messageColor, string message, int maxLength)
        {
            int length = 0;
            lock (syncLock)
            {
                switch (direction)
                {
                    case Direction.Transmit:
                        Console.Write(Colors.Transmit, "TX ");
                        length += 3;
                        break;

                    case Direction.Receive:
                        Console.Write(Colors.Receive, "RX ");
                        length += 3;
                        break;

                    case Direction.Action:
                        Console.Write(Colors.Action, "// ");
                        length += 3;
                        break;

                    default:
                        break;
                }

                if (string.IsNullOrEmpty(prefix) == false)
                {
                    Console.Write(prefixColor, prefix + " ");

                    length += prefix.Length + 1;
                }

                if (maxLength > 0)
                {
                    Console.WriteLine(messageColor, MaxLengthPadded(message, maxLength - length, ' ', " .."));
                }
                else
                {
                    Console.WriteLine(messageColor, message);
                }
            }
        }

        internal void WriteError(string message, string detail)
        {
            lock (syncLock)
            {
                if (ShowFullErrors == true &&
                    String.IsNullOrEmpty(detail) == false)
                {
                    Console.Write(Colors.Error, message);
                    Console.WriteLine(Colors.ErrorDetail, " (" + detail + ")");
                }
                else
                {
                    Console.WriteLine(Colors.Error, message);
                }
            }
        }

        /// <summary>
        /// Write a exception to the console.
        /// </summary>
        /// <param name="ex">exception object</param>
        private void WriteException_Inner(Exception ex)
        {
            Console.WriteLine(Colors.Error, ex.Message);

            WriteStackTrace_Inner(ex.StackTrace);
        }

        /// <summary>
        /// Write a 2 part item line to the console with colors.
        /// </summary>
        /// <param name="color1">Color to write part 1 with.</param>
        /// <param name="str1">String to write.</param>
        /// <param name="color2">Color to write part 2 with.</param>
        /// <param name="str2">String to write.</param>
        private void WriteItem_Inner(ConsoleColorExt color1, string str1, ConsoleColorExt color2, string str2)
        {
            Console.Write(color1, str1);
            Console.WriteLine(color2, str2);
        }

        private void WriteItem_Inner(ConsoleColorExt color1, string str1, int max, ConsoleColorExt dotColor, ConsoleColorExt color2, string str2)
        {
            Console.Write(color1, str1);

            if (str1.Length < max)
            {
                Console.Write(dotColor, "".PadLeft(max - str1.Length, '.'));
            }

            Console.Write(color1, ": ");

            Console.WriteLine(color2, str2);
        }

        /// <summary>
        /// Write a stack trace string to the console
        /// </summary>
        /// <param name="trace">trace string</param>
        private void WriteStackTrace_Inner(string trace)
        {
            if (String.IsNullOrEmpty(trace) == false)
            {
                Console.WriteLine(Colors.ErrorDetail, new string('=', 80));
                Console.WriteLine(Colors.Emphasized, "  " + trace.Replace("\n", "\n  "));
                Console.WriteLine();

                Console.WriteLine(Colors.ErrorDetail, new string('=', 80));
                Console.WriteLine();
            }
        }
    }
}