﻿using Rug.Osc;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles messages from attention zones.
/// </summary>
public class AttentionTrackerBase : MonoBehaviour
{
    /// <summary>
    /// The number of zones to track. 
    /// </summary>
    public int NumberOfZonesToTrack = 4;

    /// <summary>
    /// The base OSC address of the attention zone object. 
    /// </summary>
    public string OscAddress = "/attention";

    private readonly List<OscMessageEvent> zoneEventHandlers = new List<OscMessageEvent>();
    private OscMaster oscMaster;

    protected virtual void OnStatsMax(float value)
    {
        // applications should override implementions of this method
    }

    protected virtual void OnStatsMean(float value)
    {
        // applications should override implementions of this method
    }

    protected virtual void OnStatsMin(float value)
    {
        // applications should override implementions of this method
    }

    protected virtual void OnStatsSum(float value)
    {
        // applications should override implementions of this method
    }

    protected virtual void OnZonePosition(int zoneId, float x, float y)
    {
        // applications should override implementions of this method
    }

    #region Start / Stop

    private void Start()
    {
        // find the osc master
        oscMaster = FindObjectOfType<OscMaster>();

        if (oscMaster == null)
        {
            Debug.LogError("No OscMaster object found.");

            return;
        }

        // attach to all the stats messages
        oscMaster.Listener.Attach(OscAddress + "/stats/sum", OnMessageStatsSum);
        oscMaster.Listener.Attach(OscAddress + "/stats/min", OnMessageStatsMin);
        oscMaster.Listener.Attach(OscAddress + "/stats/max", OnMessageStatsMax);
        oscMaster.Listener.Attach(OscAddress + "/stats/mean", OnMessageStatsMean);

        // attach to the messages for each zone 
        zoneEventHandlers.Clear();

        for (int i = 0; i < NumberOfZonesToTrack; i++)
        {
            int zoneIndex = i;

            OscMessageEvent @event = delegate (OscMessage message)
            {
                OnMessageZonePosition(zoneIndex, message);
            };

            zoneEventHandlers.Add(@event);

            oscMaster.Listener.Attach(OscAddress + "/zone/" + zoneIndex + "/position", @event);
        }
    }

    private void OnDisable()
    {
        if (oscMaster == null)
        {
            return;
        }

        // detach from the stats messages
        oscMaster.Listener.Detach(OscAddress + "/stats/sum", OnMessageStatsSum);
        oscMaster.Listener.Detach(OscAddress + "/stats/min", OnMessageStatsMin);
        oscMaster.Listener.Detach(OscAddress + "/stats/max", OnMessageStatsMax);
        oscMaster.Listener.Detach(OscAddress + "/stats/mean", OnMessageStatsMean);

        // detach from zone messages 
        for (int i = 0; i < zoneEventHandlers.Count; i++)
        {
            oscMaster.Listener.Detach(OscAddress + "/zone/" + i + "/position", zoneEventHandlers[i]);
        }

        zoneEventHandlers.Clear();
    }

    #endregion

    private void OnMessageStatsMax(OscMessage message)
    {
        // /attention/stats/max, (float)value
        try
        {
            int argumentIndex = 0;

            float value = (float)message[argumentIndex++];

            OnStatsMax(value);
        }
        catch { }
    }

    private void OnMessageStatsMean(OscMessage message)
    {
        // /attention/stats/mean, (float)value
        try
        {
            int argumentIndex = 0;

            float value = (float)message[argumentIndex++];

            OnStatsMean(value);
        }
        catch { }
    }

    private void OnMessageStatsMin(OscMessage message)
    {
        // /attention/stats/min, (float)value
        try
        {
            int argumentIndex = 0;

            float value = (float)message[argumentIndex++];

            OnStatsMin(value);
        }
        catch { }
    }

    private void OnMessageStatsSum(OscMessage message)
    {
        // /attention/stats/sum, (float)value
        try
        {
            int argumentIndex = 0;

            float value = (float)message[argumentIndex++];

            OnStatsSum(value);
        }
        catch { }
    }

    private void OnMessageZonePosition(int id, OscMessage message)
    {
        // /attention/zone/0/position, (float)x, (float)y
        try
        {
            int argumentIndex = 0;

            float x = (float)message[argumentIndex++];
            float y = (float)message[argumentIndex++];

            OnZonePosition(id, x, y);
        }
        catch { }
    }
}