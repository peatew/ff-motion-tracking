﻿using Rug.Osc;
using UnityEngine;

public class BlobTrackerBase : MonoBehaviour
{
    private OscMaster oscMaster;

    protected virtual void OnBlob(int id, float centerX, float centerY, float deltaX, float deltaY, int mergeCount)
    {
        // applications should override implementions of this method
    }

    protected virtual void OnBlobDie(int id, float x, float y, int mergeCount)
    {
        // applications should override implementions of this method
    }

    protected virtual void OnBlobMerge(int id1, int id2, float x, float y, int mergeCount1, int mergeCount2)
    {
        // applications should override implementions of this method
    }

    protected virtual void OnBlobSpawn(int id, float x, float y, int mergeCount)
    {
        // applications should override implementions of this method
    }

    protected virtual void OnBlobSplit(int id, float x, float y, int mergeCount)
    {
        // applications should override implementions of this method
    }

    #region Start / Stop

    private void Start()
    {
        // find the osc master
        oscMaster = FindObjectOfType<OscMaster>();

        if (oscMaster == null)
        {
            Debug.LogError("No OscMaster object found.");

            return;
        }

        // attach to the blob messages
        oscMaster.Listener.Attach("/blobs/blob", OnMessageBlob);
        oscMaster.Listener.Attach("/blobs/die", OnMessageBlobDie);
        oscMaster.Listener.Attach("/blobs/merge", OnMessageBlobMerge);
        oscMaster.Listener.Attach("/blobs/spawn", OnMessageBlobSpawn);
        oscMaster.Listener.Attach("/blobs/split", OnMessageBlobSplit);
    }

    private void OnDisable()
    {
        if (oscMaster == null)
        {
            return;
        }

        // detach to the blob messages
        oscMaster.Listener.Detach("/blobs/blob", OnMessageBlob);
        oscMaster.Listener.Detach("/blobs/die", OnMessageBlobDie);
        oscMaster.Listener.Detach("/blobs/merge", OnMessageBlobMerge);
        oscMaster.Listener.Detach("/blobs/spawn", OnMessageBlobSpawn);
        oscMaster.Listener.Detach("/blobs/split", OnMessageBlobSplit);
    }

    #endregion 

    private void OnMessageBlob(OscMessage message)
    {
        // /blobs/blob, (int)id, (float)centerX, (float)centerY, (float)deltaX, (float)deltaY, (int)mergeCount
        try
        {
            int argumentIndex = 0;

            int id = (int)message[argumentIndex++];
            float centerX = (float)message[argumentIndex++];
            float centerY = (float)message[argumentIndex++];
            float deltaX = (float)message[argumentIndex++];
            float deltaY = (float)message[argumentIndex++];
            int mergeCount = (int)message[argumentIndex++];

            OnBlob(id, centerX, centerY, deltaX, deltaY, mergeCount); 
        }
        catch { } 
    }

    private void OnMessageBlobDie(OscMessage message)
    {
        // /blobs/die, (int)id, (float)x, (float)y, (int)mergeCount
        try
        {
            int argumentIndex = 0;

            int id = (int)message[argumentIndex++];
            float x = (float)message[argumentIndex++];
            float y = (float)message[argumentIndex++];
            int mergeCount = (int)message[argumentIndex++];

            OnBlobDie(id, x, y, mergeCount);
        }
        catch { }
    }

    private void OnMessageBlobMerge(OscMessage message)
    {
        // /blobs/merge, (int)id1, (int)id2, (float)x, (float)y, (int)mergeCount1, (int)mergeCount2
        try
        {
            int argumentIndex = 0;

            int id1 = (int)message[argumentIndex++];
            int id2 = (int)message[argumentIndex++];
            float x = (float)message[argumentIndex++];
            float y = (float)message[argumentIndex++];
            int mergeCount1 = (int)message[argumentIndex++];
            int mergeCount2 = (int)message[argumentIndex++];

            OnBlobMerge(id1, id2, x, y, mergeCount1, mergeCount2);
        }
        catch { }
    }

    private void OnMessageBlobSpawn(OscMessage message)
    {
        // /blobs/spawn, (int)id, (float)x, (float)y, (int)mergeCount
        try
        {
            int argumentIndex = 0;

            int id = (int)message[argumentIndex++];
            float x = (float)message[argumentIndex++];
            float y = (float)message[argumentIndex++];
            int mergeCount = (int)message[argumentIndex++];

            OnBlobSpawn(id, x, y, mergeCount);
        }
        catch { }
    }

    private void OnMessageBlobSplit(OscMessage message)
    {
        // /blobs/split, (int)id, (float)x, (float)y, (int)mergeCount
        try
        {
            int argumentIndex = 0;

            int id = (int)message[argumentIndex++];
            float x = (float)message[argumentIndex++];
            float y = (float)message[argumentIndex++];
            int mergeCount = (int)message[argumentIndex++];

            OnBlobSplit(id, x, y, mergeCount);
        }
        catch { }
    }
}