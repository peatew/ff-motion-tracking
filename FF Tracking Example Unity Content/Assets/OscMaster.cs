﻿using UnityEngine;
using Rug.Osc;

public class OscMaster : MonoBehaviour
{
    public OscSyncListener Listener { get; private set; }

    void Awake()
    {
        // Connect to port 7888 
        Listener = new OscSyncListener(7888);

        Listener.Connect();
    }

    void Update()
    {
        // process all messages in the update thread 
        while (Listener.TryReceive() == true) { }
    }

    void OnDestroy()
    {
        if (Listener == null)
        {
            return;
        }

        Listener.Dispose();
        Listener = null;
    }
}