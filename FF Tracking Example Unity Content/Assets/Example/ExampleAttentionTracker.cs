﻿using UnityEngine;

/// <summary>
/// Example of a attention tracker implementation. 
/// </summary>
public class ExampleAttentionTracker : AttentionTrackerBase
{
    /// <summary>
    /// The minimum amount of motion in the time frame. 
    /// </summary>
    public float Minimum;

    /// <summary>
    /// The maximum amount of motion in the time frame. 
    /// </summary>
    public float Maximum;

    /// <summary>
    /// The mean amount of motion in the time frame. 
    /// </summary>
    public float Mean;

    /// <summary>
    /// The sum total amount of motion in the time frame. 
    /// </summary>
    public float Sum;

    /// <summary>
    /// The transform mode. 
    /// </summary>
    public CameraSpaceTransformMode TransformMode = CameraSpaceTransformMode.HorizontalAndVertical;

    /// <summary>
    /// Zone markers. 
    /// </summary>
    public Transform[] ZoneMarkers;

    protected override void OnStatsMax(float value)
    {
        Maximum = value;
    }

    protected override void OnStatsMean(float value)
    {
        Mean = value;
    }

    protected override void OnStatsMin(float value)
    {
        Minimum = value;
    }

    protected override void OnStatsSum(float value)
    {
        Sum = value;
    }

    protected override void OnZonePosition(int zoneId, float x, float y)
    {
        if (zoneId < 0 || zoneId >= ZoneMarkers.Length)
        {
            return;
        }

        Vector3 position = TransformHelper.TransformIntoCameraSpace(Camera.main, transform.position, new Vector3(x, y, 0), TransformMode);

        ZoneMarkers[zoneId].position = position;
    }
}