﻿using UnityEngine;

/// <summary>
/// Example blob tracker implementation.
/// </summary>
public class ExampleBlobTracker : BlobTrackerBase
{
    /// <summary>
    /// Particle system to use for emition. 
    /// </summary>
    public ParticleSystem ParticleSystem;

    /// <summary>
    /// Transfrom mode.
    /// </summary>
    public CameraSpaceTransformMode TransformMode = CameraSpaceTransformMode.HorizontalAndVertical;

    protected override void OnBlob(int id, float centerX, float centerY, float deltaX, float deltaY, int mergeCount)
    {
        EmitOneParticle(centerX, centerY, Color.green);
    }

    protected override void OnBlobDie(int id, float x, float y, int mergeCount)
    {
        EmitOneParticle(x, y, Color.red);
    }

    protected override void OnBlobMerge(int id1, int id2, float x, float y, int mergeCount1, int mergeCount2)
    {
        EmitOneParticle(x, y, Color.cyan);
    }

    protected override void OnBlobSpawn(int id, float x, float y, int mergeCount)
    {
        EmitOneParticle(x, y, Color.yellow);
    }

    protected override void OnBlobSplit(int id, float x, float y, int mergeCount)
    {
        EmitOneParticle(x, y, Color.magenta);
    }

    private void EmitOneParticle(float x, float y, Color color)
    {
        Vector3 position = TransformHelper.TransformIntoCameraSpace(Camera.main, transform.position, new Vector3(x, y, 0), TransformMode);

        ParticleSystem.EmitParams @params = new ParticleSystem.EmitParams()
        {
            position = position,
            startColor = color,
        };

        ParticleSystem.Emit(@params, 1);
    }
}