﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Flags]
public enum CameraSpaceTransformMode : int
{
    /// <summary>
    /// No transfrom is applied and the origin coord is used.
    /// </summary>
    None = 0,

    /// <summary>
    /// Transfrom the horizontal portion of the input coordinate only.
    /// </summary>
    Horizontal = 1,

    /// <summary>
    /// Transfrom the vertical portion of the input coordinate only.
    /// </summary>
    Vertical = 2,

    /// <summary>
    /// Transfrom the horizontal and vertical portion of the input coordinate.
    /// </summary>
    HorizontalAndVertical = Horizontal | Vertical,
}

public static class TransformHelper
{
    /// <summary>
    /// Transform normalised space coords into camera space. 
    /// </summary>
    /// <param name="camera">Camera to project into.</param>
    /// <param name="origin">Origin / offset coorinate.</param>
    /// <param name="normalisedSpacePosition">Normalised space position. (-1 -> +1 in each axis)</param>
    /// <param name="mode">Transform mode.</param>
    /// <returns></returns>
    public static Vector3 TransformIntoCameraSpace(Camera camera, Vector3 origin, Vector3 normalisedSpacePosition, CameraSpaceTransformMode mode)
    {
        if (mode == CameraSpaceTransformMode.None)
        {
            return origin; 
        }

        if (camera.orthographic == false)
        {
            throw new Exception("A transform into camera space can only be performed using a orthographic camera."); 
        }

        float cameraVerticalSize = camera.orthographicSize;
        float cameraHorizontalSize = camera.orthographicSize * camera.aspect;

        switch (mode)
        {
            case CameraSpaceTransformMode.Horizontal:
                return origin + new Vector3(normalisedSpacePosition.x * cameraHorizontalSize, 0, 0);
            case CameraSpaceTransformMode.Vertical:
                return origin + new Vector3(0, normalisedSpacePosition.y * cameraVerticalSize, 0);
            case (CameraSpaceTransformMode.HorizontalAndVertical):
                return origin + new Vector3(normalisedSpacePosition.x * cameraHorizontalSize, normalisedSpacePosition.y * cameraVerticalSize, 0);
            case CameraSpaceTransformMode.None:
                return origin;
            default:
                throw new ArgumentOutOfRangeException("mode", mode, null);
        }
    }
}
