﻿
FF Trackking system front end example.

This application is an example of interpreting the blob and attention information produced by the FF Tracking System. 

To extend functionallity you should create derivative classes from the AttentionTrackerBase and BlobTrackerBase base classes. 

Using the Examples provided it will: 

 * Emit particles for each blob event that is received. 
 * Show a sphere for each element of attention.

