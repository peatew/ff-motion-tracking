﻿using System;
using System.Collections.Generic;
using Rug.Host.Loading;

namespace Rug.Host
{
    public static class StaticResourceMaster
    {
        private static readonly Dictionary<string, IStaticResource> staticResources = new Dictionary<string, IStaticResource>();
        private static readonly object syncObject = new object();

        public static bool ResourcesAreLoaded { get; private set; }

        public static void LoadResources(LoadContext context)
        {
            lock (syncObject)
            {
                ResourcesAreLoaded = true;
                foreach (IStaticResource controller in staticResources.Values)
                {
                    controller.LoadResources(context);
                }
            }
        }

        public static void RegisterStaticResource(string key, IStaticResource controller)
        {
            lock (syncObject)
            {
                IStaticResource otherController;

                if (staticResources.TryGetValue(key, out otherController) == true)
                {
                    if (otherController == controller)
                    {
                        return;
                    }

                    throw new Exception(string.Format("The key {0} already represents a resource controller.", key));
                }

                staticResources.Add(key, controller);
            }
        }

        public static bool TryGetStaticResource(string key, out IStaticResource controller)
        {
            lock (syncObject)
            {
                return staticResources.TryGetValue(key, out controller);
            }
        }

        public static bool TryGetStaticResource<T>(string key, out T controller) where T : IStaticResource
        {
            IStaticResource staticResource;
            bool found = staticResources.TryGetValue(key, out staticResource);

            if (found == false ||
                (staticResource is T) == false)
            {
                controller = default(T);
                return false;
            }

            controller = (T)staticResource;
            return true;
        }

        public static void UnloadResources()
        {
            lock (syncObject)
            {
                ResourcesAreLoaded = false;
                foreach (IStaticResource controller in staticResources.Values)
                {
                    controller.UnloadResources();
                }
            }
        }

        public static void UnregisterStaticResource(string key)
        {
            lock (syncObject)
            {
                IStaticResource controller;

                if (staticResources.TryGetValue(key, out controller) == false)
                {
                    return;
                }

                staticResources.Remove(key);

                if (ResourcesAreLoaded == true)
                {
                    controller.UnloadResources();
                }
            }
        }
    }
}