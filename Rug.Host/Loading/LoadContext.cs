﻿using System.Collections.Generic;

namespace Rug.Host.Loading
{
    public sealed class LoadContext
    {
        /// <summary>
        /// The errors encountered while loading.
        /// </summary>
        public readonly List<string> Errors = new List<string>();

        /// <summary>
        /// The reporter.
        /// </summary>
        public readonly IReporter Reporter;

        /// <summary>
        /// Whether the context has encountered a critical error.
        /// </summary>
        public bool HasHadCriticalError = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoadContext"/> class.
        /// </summary>
        /// <param name="reporter">The reporter.</param>
        public LoadContext(IReporter reporter)
        {
            Reporter = reporter;
        }

        /// <summary>
        /// Adds the specified error message to the set of errors encountered during loading.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="isCriticalError">if set to <c>true</c> [is critical error].</param>
        public void Error(string message, bool isCriticalError = true)
        {
            Errors.Add(message);

            HasHadCriticalError &= isCriticalError;
        }

        /// <summary>
        /// Prints errors to the reporter
        /// </summary>
        /// <returns>System.String.</returns>
        public void ReportErrors()
        {
            foreach (string error in Errors)
            {
                Reporter.PrintError(error);
            }
        }
    }
}