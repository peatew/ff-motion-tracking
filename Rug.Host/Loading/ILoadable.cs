using System;
using System.Collections.Generic;
using System.Xml;

namespace Rug.Host.Loading
{
    /// <summary>
    /// Loadable Interface.
    /// </summary>
    /// <autogeneratedoc />
    public interface ILoadable
    {
        /// <summary>
        /// Saves this instance to the specified element.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns>XmlElement.</returns>
        /// <autogeneratedoc />
        XmlElement Save(LoadContext context, XmlElement element);

        /// <summary>
        /// Loads this instance from the specified node.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <autogeneratedoc />
        void Load(LoadContext context, XmlNode node);
    }

    /// <summary>
    /// Class Loader.
    /// </summary>
    /// <autogeneratedoc />
    public static class Loader
    {
        /// <summary>
        /// Gets the name of the XML tag.
        /// </summary>
        /// <param name="original">The original.</param>
        /// <returns>System.String.</returns>
        /// <autogeneratedoc />
        public static string XmlTagName(string original)
        {
            string name = original;

            name = name.Replace(" ", "").Replace("\n", "").Replace("\r", "").Replace("\t", "");

            return name;
        }

        /// <summary>
        /// Saves the object.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <param name="obj">The object.</param>
        /// <param name="root">The root.</param>
        /// <returns>XmlElement.</returns>
        /// <autogeneratedoc />
        /// TODO Edit XML Comment Template for SaveObject
        public static XmlElement SaveObject(LoadContext context, object obj, XmlNode root)
        {
            return SaveObject(context, null, obj, root);
        }

        /// <summary>
        /// Saves the object.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <param name="obj">The object.</param>
        /// <param name="root">The root.</param>
        /// <returns>XmlElement.</returns>
        /// <exception cref="System.Exception">Object is not loadable</exception>
        /// <autogeneratedoc />
        /// TODO Edit XML Comment Template for SaveObject
        public static XmlElement SaveObject(LoadContext context, string tag, object obj, XmlNode root)
        {
            if (obj is ILoadable)
            {
                ILoadable loadable = obj as ILoadable;

                string name = LoadManager.GetXmlName(obj.GetType());

                XmlElement element;
                XmlDocument doc = root.OwnerDocument;

                if (root is XmlDocument)
                {
                    doc = (XmlDocument)root;
                }

                if (Helper.IsNotNullOrEmpty(tag))
                {
                    element = doc.CreateElement(tag);

                    element.Attributes.Append(doc.CreateAttribute("Type"));
                    element.Attributes["Type"].Value = name;
                }
                else
                {
                    element = doc.CreateElement(XmlTagName(name));
                }

                return loadable.Save(context, element);
            }
            else
            {
                throw new Exception("Object is not loadable");
            }
        }

        /// <summary>
        /// Saves the objects.
        /// </summary>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="tag">The tag.</param>
        /// <param name="objs">The objects.</param>
        /// <param name="root">The root.</param>
        /// <returns>XmlElement.</returns>
        /// <autogeneratedoc />
        /// TODO Edit XML Comment Template for SaveObjects
        public static XmlElement SaveObjects(LoadContext context, string collectionName, string tag, object[] objs, XmlNode root)
        {
            XmlElement element;

            if (Helper.IsNotNullOrEmpty(collectionName))
            {
                XmlDocument doc = root.OwnerDocument;

                if (root is XmlDocument)
                {
                    doc = (XmlDocument)root;
                }

                element = doc.CreateElement(collectionName);
            }
            else
            {
                element = root as XmlElement;
            }

            foreach (object obj in objs)
            {
                element.AppendChild(SaveObject(context, tag, obj, element));
            }

            return element;
        }

        /// <summary>
        /// Loads the object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sourceNode">The source node.</param>
        /// <returns>T.</returns>
        /// <autogeneratedoc />
        /// TODO Edit XML Comment Template for LoadObject`1
        public static T LoadObject<T>(LoadContext context, XmlNode sourceNode) where T : ILoadable
        {
            T[] loaded = LoadObjects<T>(context, null, sourceNode);

            if (loaded.Length == 0)
            {
                return default(T);
            }
            else
            {
                return loaded[0];
            }
        }

        /// <summary>
        /// Loads the object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tag">The tag.</param>
        /// <param name="sourceNode">The source node.</param>
        /// <returns>T.</returns>
        /// <autogeneratedoc />
        /// TODO Edit XML Comment Template for LoadObject`1
        public static T LoadObject<T>(LoadContext context, string tag, XmlNode sourceNode) where T : ILoadable
        {
            T[] loaded = LoadObjects<T>(context, tag, sourceNode);

            if (loaded.Length == 0)
            {
                return default(T);
            }
            else
            {
                return loaded[0];
            }
        }

        /// <summary>
        /// Loads the object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sourceNode">The source node.</param>
        /// <param name="argTypes">The argument types.</param>
        /// <param name="arguments">The arguments.</param>
        /// <returns>T.</returns>
        /// <autogeneratedoc />
        /// TODO Edit XML Comment Template for LoadObject`1
        public static T LoadObject<T>(LoadContext context, XmlNode sourceNode, Type[] argTypes, object[] arguments) where T : ILoadable
        {
            T[] loaded = LoadObjects<T>(context, null, sourceNode, argTypes, arguments);

            if (loaded.Length == 0)
            {
                return default(T);
            }
            else
            {
                return loaded[0];
            }
        }

        /// <summary>
        /// Loads the object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tag">The tag.</param>
        /// <param name="sourceNode">The source node.</param>
        /// <param name="argTypes">The argument types.</param>
        /// <param name="arguments">The arguments.</param>
        /// <returns>T.</returns>
        /// <autogeneratedoc />
        /// TODO Edit XML Comment Template for LoadObject`1
        public static T LoadObject<T>(LoadContext context, string tag, XmlNode sourceNode, Type[] argTypes, object[] arguments) where T : ILoadable
        {
            T[] loaded = LoadObjects<T>(context, tag, sourceNode, argTypes, arguments);

            if (loaded.Length == 0)
            {
                return default(T);
            }
            else
            {
                return loaded[0];
            }
        }

        /// <summary>
        /// Loads the objects.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sourceNode">The source node.</param>
        /// <returns>T[].</returns>
        /// <autogeneratedoc />
        /// TODO Edit XML Comment Template for LoadObjects`1
        public static T[] LoadObjects<T>(LoadContext context, XmlNode sourceNode) where T : ILoadable
        {
            return LoadObjects<T>(context, null, sourceNode, new Type[0], new object[0]);
        }

        /// <summary>
        /// Loads the objects.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tag">The tag.</param>
        /// <param name="sourceNode">The source node.</param>
        /// <returns>T[].</returns>
        /// <autogeneratedoc />
        /// TODO Edit XML Comment Template for LoadObjects`1
        public static T[] LoadObjects<T>(LoadContext context, string tag, XmlNode sourceNode) where T : ILoadable
        {
            return LoadObjects<T>(context, tag, sourceNode, new Type[0], new object[0]);
        }

        /// <summary>
        /// Loads the objects.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sourceNode">The source node.</param>
        /// <param name="argTypes">The argument types.</param>
        /// <param name="arguments">The arguments.</param>
        /// <returns>T[].</returns>
        /// <autogeneratedoc />
        /// TODO Edit XML Comment Template for LoadObjects`1
        public static T[] LoadObjects<T>(LoadContext context, XmlNode sourceNode, Type[] argTypes, object[] arguments) where T : ILoadable
        {
            return LoadObjects<T>(context, null, sourceNode, argTypes, arguments);
        }

        /// <summary>
        /// Loads the objects.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tag">The tag.</param>
        /// <param name="sourceNode">The source node.</param>
        /// <param name="argTypes">The argument types.</param>
        /// <param name="arguments">The arguments.</param>
        /// <returns>T[].</returns>
        /// <autogeneratedoc />
        /// TODO Edit XML Comment Template for LoadObjects`1
        public static T[] LoadObjects<T>(LoadContext context, string tag, XmlNode sourceNode, Type[] argTypes, object[] arguments) where T : ILoadable
        {
            Type baseType = typeof(T);

            if (sourceNode == null)
            {
                return new T[0];
            }

            Dictionary<string, Type> types = LoadManager.GetTypeOfType(baseType);
            List<T> objects = new List<T>();

            if ((tag == null) || (tag.Length < 1))
            {
                foreach (XmlNode node in sourceNode.ChildNodes)
                {
                    string typeName = node.Name;

                    if (types.ContainsKey(typeName))
                    {
                        Type type = types[typeName];

                        T obj = (T)type.GetConstructor(argTypes).Invoke(arguments);

                        obj.Load(context, node);
                        objects.Add(obj);
                    }
                }
            }
            else
            {
                XmlNodeList nodes = sourceNode.SelectNodes(tag);

                foreach (XmlNode node in nodes)
                {
                    //Allow case insensitivity for attribute "Type".
                    XmlAttribute typeAttribute = node.Attributes["Type"];
                    if (typeAttribute == null) typeAttribute = node.Attributes["type"];
                    if (typeAttribute != null)
                    {
                        string typeName = typeAttribute.Value;

                        if (types.ContainsKey(typeName))
                        {
                            Type type = types[typeName];

                            T obj = (T)type.GetConstructor(argTypes).Invoke(arguments);

                            obj.Load(context, node);
                            objects.Add(obj);
                        }
                    }
                }
            }

            return objects.ToArray();
        }
    }
}
