﻿using System;
using System.Threading;
using Rug.Host.BaseClasses;
using Rug.Host.Commands;
using Rug.Host.Loading;

namespace Rug.Host.Misc
{
    [XmlName("Misc.Clock")]
    public class Clock : ObjectBase, ITimingSource
    {
        private bool shouldExit = false;
        private Thread thread;
        private double time;

        [OscMember]
        public OscInt Interval { get; private set; }

        [OscMember]
        public OscFloat Speed { get; private set; }

        public DateTime StartTime { get; set; }

        public DateTime Time { get; set; }

        public float DeltaTime { get; private set; } = 0; 

        public event EventHandler Pulse;

        public Clock()
        {
            Interval = new OscInt(this, nameof(Interval), "/" + nameof(Interval).ToLower(), 10);
            Speed = new OscFloat(this, nameof(Speed), "/" + nameof(Speed).ToLower(), 1f);

            StartTime = DateTime.Now;
            Time = DateTime.Now;
        }

        public override void Bind(Workspace workspace)
        {
            Reset();

            shouldExit = false;
            thread = new Thread(TimeLoop);
            thread.Name = Name; 
            thread.Start();
        }

        public override void Unbind()
        {
            shouldExit = true;
            thread?.Join(1000);
        }

        public void Reset()
        {
            Time = StartTime;
            time = 0;
            DeltaTime = 0; 
        }

        private void TimeLoop()
        {
            DateTime lastTime = DateTime.Now;

            while (shouldExit == false)
            {
                if (Enabled.Value == true)
                {
                    Pulse?.Invoke(this, EventArgs.Empty);

                    Connection?.Flush();

                    thread.Join(Interval.Value);

                    DateTime now = DateTime.Now;

                    time += Math.Abs((double)(now.Ticks - lastTime.Ticks) * (double)Speed.Value);

                    Time = new DateTime(StartTime.Ticks + (long)time);

                    DeltaTime = (float)(now - lastTime).TotalSeconds; 

                    lastTime = now;
                }
                else
                {
                    thread.Join(Interval.Value);
                }
            }
        }
    }
}