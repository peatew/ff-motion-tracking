﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;
using Rug.Host.BaseClasses;
using Rug.Host.Commands;
using Rug.Host.Loading;

namespace Rug.Host.Misc
{
    [XmlName("Misc.Transform")]
    public class Transform : ObjectBase, ITransfromSource
    {
        [OscMember]
        public OscVector3 EulerAngles { get; private set; }

        //[OscMember]
        //public OscQuaternion Rotation { get; private set; }

        [OscMember]
        public OscVector3 Translation { get; private set; }

        [OscMember]
        public OscVector3 Scale { get; private set; }

        public Matrix4 Matrix
        {
            get
            {
                Vector3 eulerAngles = (EulerAngles.Value / 360) * MathHelper.TwoPi;

                Quaternion rotation = QuaternionFromEulerAngles(eulerAngles.X, eulerAngles.Y, eulerAngles.Z); 

                return Matrix4.CreateFromQuaternion(rotation) * Matrix4.CreateScale(Scale.Value) * Matrix4.CreateTranslation(Translation.Value); 
            }
        }

        public Transform()
        {
            EulerAngles = new OscVector3(this, nameof(EulerAngles), "/" + nameof(EulerAngles).ToLower(), new Vector3(0, 0, 0));
            //Rotation = new OscQuaternion(this, nameof(Rotation), "/" + nameof(Rotation).ToLower(), 1f);
            Translation = new OscVector3(this, nameof(Translation), "/" + nameof(Translation).ToLower(), new Vector3(0, 0, 0));
            Scale = new OscVector3(this, nameof(Scale), "/" + nameof(Scale).ToLower(), new Vector3(1, 1, 1));
        }

        public static Quaternion QuaternionFromEulerAngles(float yaw, float pitch, float roll)
        {
            // Heading = Yaw
            // Attitude = Pitch
            // Bank = Roll

            yaw *= 0.5f;
            pitch *= 0.5f;
            roll *= 0.5f;

            // Assuming the angles are in radians.
            float c1 = (float)Math.Cos(yaw);
            float s1 = (float)Math.Sin(yaw);
            float c2 = (float)Math.Cos(pitch);
            float s2 = (float)Math.Sin(pitch);
            float c3 = (float)Math.Cos(roll);
            float s3 = (float)Math.Sin(roll);
            float c1c2 = c1 * c2;
            float s1s2 = s1 * s2;

            Quaternion quaternion = new Quaternion();
            quaternion.W = c1c2 * c3 - s1s2 * s3;
            quaternion.X = c1c2 * s3 + s1s2 * c3;
            quaternion.Y = s1 * c2 * c3 + c1 * s2 * s3;
            quaternion.Z = c1 * s2 * c3 - s1 * c2 * s3;

            return quaternion;
        }

        //private Quaternion FromEulerAngles()
        //{                        
        //    // Assuming the angles are in radians.
        //    double c1 = Math.Cos(heading / 2);
        //    double s1 = Math.Sin(heading / 2);
        //    double c2 = Math.Cos(attitude / 2);
        //    double s2 = Math.Sin(attitude / 2);
        //    double c3 = Math.Cos(bank / 2);
        //    double s3 = Math.Sin(bank / 2);

        //    double c1c2 = c1 * c2;
        //    double s1s2 = s1 * s2;

        //    float w = c1c2 * c3 - s1s2 * s3;
        //    float x = c1c2 * s3 + s1s2 * c3;
        //    float y = s1 * c2 * c3 + c1 * s2 * s3;
        //    float z = c1 * s2 * c3 - s1 * c2 * s3;

        //    Quaternion quaternion = new Quaternion(x, y, z, w); 
        //}

        public override void Bind(Workspace workspace)
        {

        }

        public override void Unbind()
        {

        }
    }
}
