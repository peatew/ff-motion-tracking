﻿using System;
using System.Threading;
using Rug.Host.BaseClasses;
using Rug.Host.Commands;
using Rug.Host.Loading;

namespace Rug.Host.Misc
{
    [XmlName("Misc.Pulse")]
    public class SimplePulse : ObjectBase, IPulse
    {
        private bool shouldExit = false;

        private Thread thread;

        [OscMember]
        public OscInt Interval { get; private set; }

        public float DeltaTime { get; private set; } 

        public event EventHandler Pulse;

        public SimplePulse()
        {
            Interval = new OscInt(this, "Interval", "/interval", 100);
        }

        public override void Bind(Workspace workspace)
        {
            shouldExit = false;
            thread = new Thread(TimeLoop);
            thread.Name = Name;
            ThreadName = Name;
            thread.Start();
        }

        public override void Unbind()
        {
            shouldExit = true;

            thread?.Join(1000);
        }

        private void TimeLoop()
        {
            DateTime lastTime = DateTime.Now; 

            while (shouldExit == false)
            {
                DateTime timeNow = DateTime.Now;

                DeltaTime = (float)(timeNow - lastTime).TotalSeconds;

                lastTime = timeNow; 

                if (Enabled.Value == true)
                {
                    Pulse?.Invoke(this, EventArgs.Empty);

                    Connection?.Flush();
                }

                thread.Join(Interval.Value);
            }
        }
    }
}
