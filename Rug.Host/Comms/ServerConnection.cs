﻿using System.Net;
using Rug.Osc;
using Rug.Osc.Packaging;

namespace Rug.Host.Comms
{
    public class ServerConnection : IOscConnection
    {
        private object syncObject = new object();

        private IPAddress adapterAddress;
        private OscListener listener;
        private OscPackageBuilder packageBuilder;
        private int receivePort;
        private IReporter reporter;
        private OscSender sender;
        private int sendPort;

        public OscConnectionInfo ConnectionInfo { get; private set; } 

        public OscListener Listener { get { return listener; } }

        public bool PackageMessages
        {
            get { return packageBuilder.ImmediateMode; }
            set { packageBuilder.ImmediateMode = value; }
        }

        public IReporter Reporter { get { return reporter; } }

        public ServerConnection(IReporter reporter, OscConnectionInfo info)
        {
            ConnectionInfo = info; 

            this.reporter = reporter;
            this.adapterAddress = info.NetworkAdapterIPAddress;
            this.receivePort = info.ReceivePort;
            this.sendPort = info.SendPort;

            listener = new OscListener(adapterAddress, receivePort);
            listener.PacketReceived += Listener_PacketReceived;
            listener.PacketProcessed += Listener_PacketProcessed;
            sender = new OscSender(IPAddress.Broadcast, 0, sendPort);
            sender.PacketSent += Sender_PacketSent;
            packageBuilder = new OscPackageBuilder(0, false);

            packageBuilder.BundleComplete += PackageBuilder_BundleComplete;
        }

        public void Connect()
        {
            listener.Connect();
            sender.Connect();
        }

        public void Dispose()
        {
            listener?.Dispose();
            sender?.Dispose();
        }

        public void Flush()
        {
            lock (syncObject)
            {
                packageBuilder.Flush();
            }
        }

        public void Send(string oscAddress, params object[] arguments)
        {
            Send(new OscMessage(oscAddress, arguments));
        }

        public void Send(params OscPacket[] packets)
        {
            lock (syncObject)
            {
                packageBuilder.Add(packets);
            }
        }

        private void Listener_PacketProcessed(OscPacket packet)
        {
            Flush();
        }

        private void Listener_PacketReceived(OscPacket packet)
        {
            reporter.PrintOscPackets(Direction.Receive, packet);
        }

        private void PackageBuilder_BundleComplete(ulong arg1, OscBundle arg2)
        {
            sender.Send(arg2);
        }

        private void Sender_PacketSent(OscPacket packet)
        {
            reporter.PrintOscPackets(Direction.Transmit, packet);
        }
    }
}