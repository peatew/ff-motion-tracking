﻿using System;
using System.Net;

namespace Rug.Host.Comms
{
    public sealed class OscConnectionInfo : IComparable<OscConnectionInfo>, IEquatable<OscConnectionInfo>
    {
        public IPAddress Address { get; private set; } 

        public string Descriptor { get; private set; }

        public IPAddress NetworkAdapterIPAddress { get; private set; }

        public int SendPort { get; private set; } 

        public int ReceivePort { get; private set; }

        public OscConnectionInfo(string descriptor, IPAddress networkAdapterIPAddress, IPAddress address, int sendPort, int receivePort)
        {
            Descriptor = descriptor;
            NetworkAdapterIPAddress = networkAdapterIPAddress;
            Address = address;
            SendPort = sendPort;
            ReceivePort = receivePort; 
        }

        public int CompareTo(OscConnectionInfo other)
        {
            return Descriptor.CompareTo(other.Descriptor);
        }

        public bool Equals(OscConnectionInfo other)
        {
            OscConnectionInfo otherNetworkConnectionInfo = other as OscConnectionInfo;

            return
                Descriptor.Equals(otherNetworkConnectionInfo.Descriptor) &&
                NetworkAdapterIPAddress.Equals(otherNetworkConnectionInfo.NetworkAdapterIPAddress) &&
                Address.Equals(otherNetworkConnectionInfo.Address) &&
                SendPort.Equals(otherNetworkConnectionInfo.SendPort) && 
                ReceivePort.Equals(otherNetworkConnectionInfo.ReceivePort);
        }
    }
}