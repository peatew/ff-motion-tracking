﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading;
using Rug.Osc.Ahoy;

namespace Rug.Host.Comms
{
    public class SearchForConnections : IDisposable, IEnumerable<OscConnectionInfo>
    {
        public readonly int SendInterval;

        public readonly int Timeout;

        private readonly List<OscConnectionInfo> autoConnectionInfoList = new List<OscConnectionInfo>();

        private readonly object autoConnectionInfoListSyncLock = new object();

        private readonly List<IAhoyQuery> queriesList = new List<IAhoyQuery>();

        private readonly object queriesListSyncLock = new object();

        private DateTime startTime;

        public int Count { get { return autoConnectionInfoList.Count; } }

        public OscConnectionInfo this[int index] { get { return autoConnectionInfoList[index]; } }

        public event Action<OscConnectionInfo> DeviceDiscovered;

        public event Action<OscConnectionInfo> DeviceExpired;

        public SearchForConnections(int timeout = 0, int sendInterval = 300)
        {
            Timeout = timeout;
            SendInterval = sendInterval;
        }

        public static OscConnectionInfo[] EnumerateConnections(int timeout = 1000, int interval = 300)
        {
            using (SearchForConnections autoConnector2 = new SearchForConnections(timeout, interval))
            {
                autoConnector2.Search();

                autoConnector2.Sort();

                return autoConnector2.ToArray();
            }
        }

        public void BeginSearch()
        {
            EndSearch();

            startTime = DateTime.Now;

            lock (autoConnectionInfoListSyncLock)
            {
                autoConnectionInfoList.Clear();
            }

            lock (queriesListSyncLock)
            {
                BeginSearchUdp();
            }
        }

        public void Dispose()
        {
            EndSearch();
        }

        public void EndSearch()
        {
            lock (queriesListSyncLock)
            {
                EndSearchUdp();
            }

            startTime = DateTime.MinValue;
        }

        public IEnumerator<OscConnectionInfo> GetEnumerator()
        {
            return autoConnectionInfoList.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (autoConnectionInfoList as IEnumerable).GetEnumerator();
        }

        public int IndexOf(OscConnectionInfo info)
        {
            lock (autoConnectionInfoListSyncLock)
            {
                return autoConnectionInfoList.IndexOf(info);
            }
        }

        public void Search()
        {
            if (Timeout <= 0)
            {
                throw new System.Exception("Cannot search with an infinite timeout.");
            }

            BeginSearch();

            WaitForCompletion();
        }

        public void Sort()
        {
            lock (autoConnectionInfoListSyncLock)
            {
                autoConnectionInfoList.Sort();
            }
        }

        public OscConnectionInfo[] ToArray()
        {
            lock (autoConnectionInfoListSyncLock)
            {
                return autoConnectionInfoList.ToArray();
            }
        }

        public void WaitForCompletion()
        {
            if (Timeout <= 0)
            {
                throw new System.Exception("Cannot wait with an infinite timeout.");
            }

            try
            {
                DateTime endTime = startTime.AddMilliseconds(Timeout);

                double timeDelta = (endTime - startTime).TotalSeconds;

                int waitInterval = (int)(timeDelta * 1000d);

                if (waitInterval > 0)
                {
                    Thread.CurrentThread.Join(waitInterval);
                }
            }
            finally
            {
                EndSearch();
            }
        }

        private void BeginSearchUdp()
        {
            queriesList.Add(AhoyQuery.CreateQuery());

            foreach (IAhoyQuery query in queriesList)
            {
                query.ServiceDiscovered += delegate (AhoyServiceInfo serviceInfo)
                {
                    NetworkInterface networkInterface = GetInterfaceFor(serviceInfo.NetworkAdapterIPAddress);

                    string adapterName = "Unknown adapter";

                    if (networkInterface != null)
                    {
                        adapterName = networkInterface.Description;
                    }

                    OscConnectionInfo info = new OscConnectionInfo(
                        serviceInfo.Descriptor,
                        serviceInfo.NetworkAdapterIPAddress,
                        serviceInfo.Address,
                        serviceInfo.SendPort, 
                        serviceInfo.ListenPort);

                    lock (autoConnectionInfoListSyncLock)
                    {
                        autoConnectionInfoList.Add(info);
                    }

                    DeviceDiscovered?.Invoke(info);
                };

                query.ServiceExpired += delegate (AhoyServiceInfo serviceInfo)
                {
                    NetworkInterface networkInterface = GetInterfaceFor(serviceInfo.NetworkAdapterIPAddress);

                    string adapterName = "Unknown adapter";

                    if (networkInterface != null)
                    {
                        adapterName = networkInterface.Description;
                    }

                    OscConnectionInfo info = new OscConnectionInfo(
                        serviceInfo.Descriptor,
                        serviceInfo.NetworkAdapterIPAddress,
                        serviceInfo.Address,
                        serviceInfo.SendPort,
                        serviceInfo.ListenPort);

                    lock (autoConnectionInfoListSyncLock)
                    {
                        autoConnectionInfoList.Remove(info);
                    }

                    DeviceExpired?.Invoke(info);
                };

                query.BeginSearch(SendInterval);
            }
        }

        private void EndSearchUdp()
        {
            foreach (IAhoyQuery query in queriesList)
            {
                query.EndSearch();
            }

            queriesList.Clear();
        }

        private NetworkInterface GetInterfaceFor(IPAddress networkAdapterIPAddress)
        {
            foreach (NetworkInterface @interface in NetworkInterface.GetAllNetworkInterfaces())
            {
                var ipProps = @interface.GetIPProperties();

                foreach (var ip in ipProps.UnicastAddresses)
                {
                    if (ip.Address.Equals(networkAdapterIPAddress) == true)
                    {
                        return @interface;
                    }
                }
            }

            return null;
        }
    }
}
