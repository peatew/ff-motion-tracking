﻿using System;
using Rug.Osc;

namespace Rug.Host.Comms
{
    public interface IOscConnection : IDisposable
    {
        OscConnectionInfo ConnectionInfo { get; }

        OscListener Listener { get; }

        IReporter Reporter { get; } 

        bool PackageMessages { get; set; } 

        void Connect();

        void Send(string oscAddress, params object[] arguments);

        void Send(params OscPacket[] packet);

        void Flush(); 
    }
}
