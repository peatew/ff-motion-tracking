﻿using System.Net;
using Rug.Osc;
using Rug.Osc.Packaging;

namespace Rug.Host.Comms
{
    public class ClientConnection : IOscConnection
    {
        private IPAddress adapterAddress;
        private OscListener listener;
        private OscPackageBuilder packageBuilder;
        private int receivePort;
        private IReporter reporter;
        private OscSender sender;
        private int sendPort;
        private IPAddress serverAddress;

        public OscListener Listener { get { return listener; } }
        
        public OscConnectionInfo ConnectionInfo { get; private set; }

        public bool PackageMessages
        {
            get { return packageBuilder.ImmediateMode; }
            set { packageBuilder.ImmediateMode = value; }
        }

        public IReporter Reporter { get { return reporter; } }

        public ClientConnection(IReporter reporter, OscConnectionInfo info)
        {
            ConnectionInfo = info; 

            this.reporter = reporter;

            adapterAddress = info.NetworkAdapterIPAddress;
            serverAddress = info.Address;
            receivePort = info.ReceivePort;
            sendPort = info.SendPort;

            listener = new OscListener(adapterAddress, receivePort);
            listener.PacketReceived += Listener_PacketReceived;
            listener.Attach("/error", OnError); 

            sender = new OscSender(serverAddress, 0, sendPort);
            sender.PacketSent += Sender_PacketSent;
            packageBuilder = new OscPackageBuilder(0, false);

            packageBuilder.BundleComplete += PackageBuilder_BundleComplete;
        }

        private void Listener_PacketReceived(OscPacket packet)
        {
            reporter.PrintOscPackets(Direction.Receive, packet);
        }

        private void OnError(OscMessage message)
        {
            if (message.Count == 0)
            {
                return; 
            }

            reporter.PrintError(message[0].ToString());
        }

        private void Sender_PacketSent(OscPacket packet)
        {
            reporter.PrintOscPackets(Direction.Transmit, packet);
        }

        public void Connect()
        {
            listener.Connect();
            sender.Connect();
        }

        public void Dispose()
        {
            listener?.Dispose();
            sender?.Dispose();
        }

        public void Flush()
        {
            packageBuilder.Flush();
        }

        public void Send(string oscAddress, params object[] arguments)
        {
            Send(new OscMessage(oscAddress, arguments));
        }

        public void Send(params OscPacket[] packets)
        {
            packageBuilder.Add(packets);
        }

        private void PackageBuilder_BundleComplete(ulong arg1, OscBundle arg2)
        {
            sender.Send(arg2);
        }

    }
}