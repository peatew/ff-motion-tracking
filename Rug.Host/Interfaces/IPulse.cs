﻿using System;

namespace Rug.Host
{
    public interface IPulse : IObject
    {
        float DeltaTime { get; } 

        event EventHandler Pulse; 
    }
}
