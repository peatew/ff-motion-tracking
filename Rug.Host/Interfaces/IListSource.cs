﻿using System;
using System.Collections.Generic;

namespace Rug.Host
{
    public interface IListSource<T> : IObject
    {
        List<T> List { get; }

        event EventHandler ListUpdated;
    }
}