﻿using Rug.Host.Loading;

namespace Rug.Host
{
    public interface IStaticResource
    {
        void LoadResources(LoadContext context);

        void UnloadResources();
    }
}