﻿using OpenTK;
using Rug.Host.BaseClasses;
using Rug.Host.Commands;
using Rug.Host.Comms;
using Rug.Host.Loading;

namespace Rug.Host
{
    public interface IObject : ILoadable
    {
        OscMemberCollection OscMembers { get; } 

        IOscConnection Connection { get; }

        OscBool Enabled { get; }

        string ThreadName { get; } 

        string Name { get; set; }

        OscRectangleF Bounds { get; }

        void Attach(IOscConnection connection);
        void Detach();

        void Bind(Workspace workspace);
        void Unbind();

        void Send(); 
    }
}