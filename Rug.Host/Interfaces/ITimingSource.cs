﻿using System;
using Rug.Host.Commands;

namespace Rug.Host
{
    public interface ITimingSource : IObject, IPulse
    {
        OscFloat Speed { get; }

        DateTime StartTime { get; set; }

        DateTime Time { get; set; }

        void Reset();
    }
}