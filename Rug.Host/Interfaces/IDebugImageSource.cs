﻿using System.Drawing;

namespace Rug.Host
{
    public delegate void DebugFrameEvent(IDebugImageSource source, Bitmap bitmap);

    public interface IDebugImageSource : IObject
    {
        bool DebuggerAttached { get; set; }

        event DebugFrameEvent NewDebugFrame;
    }
}