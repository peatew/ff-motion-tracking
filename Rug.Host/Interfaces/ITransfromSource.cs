﻿using OpenTK;

namespace Rug.Host
{
    public interface ITransfromSource : IObject
    {
        Matrix4 Matrix { get; } 
    }
}
