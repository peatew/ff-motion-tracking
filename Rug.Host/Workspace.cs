﻿using System;
using System.Collections.Generic;
using System.Xml;
using Rug.Host.Commands;
using Rug.Host.Comms;
using Rug.Host.Loading;
using Rug.Osc;
using Rug.Osc.Ahoy;

namespace Rug.Host
{
    public class Workspace : IDisposable, ILoadable
    {
        private readonly Dictionary<string, IObject> objects = new Dictionary<string, IObject>();

        private readonly object syncLock = new object();
        private IOscConnection connection;
        //private Thread highFrequencPulseThread;
        //private Thread lowFrequencPulseThread;
        private IReporter reporter;
        //private bool shouldExitPulseThread = false;
        private AhoyServiceProvider ahoyService;

        public Workspace(IReporter reporter, IOscConnection connection)
        {
            this.reporter = reporter;
            this.connection = connection;

            foreach (OscCommandnfo command in OscCommandManager.GetCommandsForType(typeof(Workspace), reporter))
            {
                this.connection.Listener.Attach(command.OscAddress, command.GetForObject(this));
            }
        }

        public T FindObject<T>(string name) where T : IObject
        {
            IObject found = null;

            if (objects.TryGetValue(name, out found) == false)
            {
                return default(T); 
            }

            if ((found is T) == false)
            {
                return default(T);
            }

            return (T)found; 
        }

        public IEnumerable<T> FindObjects<T>() where T : IObject
        {
            List<T> found = new List<T>();

            foreach (IObject @object in objects.Values)
            {
                if ((found is T) == false)
                {
                    continue;
                }

                found.Add((T)@object); 
            }

            return found; 
        }

        public IEnumerable<IObject> Objects { get { return objects.Values; } } 

        public void Clear()
        {
            //shouldExitPulseThread = true;

            //highFrequencPulseThread?.Join();
            //lowFrequencPulseThread?.Join();

            UnbindAll(); 

            lock (syncLock)
            {                
                foreach (IObject @object in objects.Values)
                {
                    @object.Detach();
                }

                objects.Clear();
            }
        }

        private void UnbindAll()
        {
            foreach (IObject @object in objects.Values)
            {
                @object.Unbind(); 
            }
        }

        private void BindAll(LoadContext context)
        {
            foreach (IObject @object in objects.Values)
            {
                try
                {
                    @object.Bind(this);
                }
                catch (Exception ex)
                {
                     context.Error("Failed to link object " + @object.Name + ". " + ex.Message);
                }
            }
        }

        public void Dispose()
        {            
            ahoyService?.Dispose(); 

            Clear();

            StaticResourceMaster.UnloadResources();
        }

        public void Load(LoadContext context, XmlNode node)
        {
            lock (syncLock)
            {
                Clear();

                foreach (IObject @object in Loader.LoadObjects<IObject>(context, node.SelectSingleNode("Objects")))
                {
                    objects.Add(@object.Name, @object);
                    //AddObjectToLookups(@object);
                }

                if (context.HasHadCriticalError == true)
                {
                    return;
                }

                foreach (IObject @object in objects.Values)
                {
                    @object.Attach(connection);
                }

                StaticResourceMaster.LoadResources(context);

                BindAll(context);      
            }
        }

        public void Start()
        {
            //lock (syncLock)
            //{
            //    shouldExitPulseThread = false;

            //    highFrequencPulseThread = new Thread(HighFrequencyPulseLoop);
            //    highFrequencPulseThread.Start();

            //    lowFrequencPulseThread = new Thread(LowFrequencPulseLoop);
            //    lowFrequencPulseThread.Start();
            //}

            ahoyService = new AhoyServiceProvider(connection.ConnectionInfo.NetworkAdapterIPAddress);
            {
                ahoyService.Add(new AhoyService(connection.ConnectionInfo.Descriptor, connection.ConnectionInfo.SendPort, connection.ConnectionInfo.ReceivePort)); 

                //ahoyService.MessageReceived += AhoyService_MessageReceived;
                //ahoyService.MessageSent += AhoyService_MessageSent;

                ahoyService.Start();
            }
        }

        public XmlElement Save(LoadContext context, XmlElement element)
        {
            lock (syncLock)
            {
                List<IObject> objects = new List<IObject>(this.objects.Values);

                element.AppendChild(Loader.SaveObjects(context, "Objects", null, objects.ToArray(), element));

                return element; 
            }
        }

        [OscCommand("/hello")]
        private void Command_Hello(OscMessage message)
        {
            lock (syncLock)
            {
                connection.Send("/hello", "Image Feature Service", typeof(Workspace).Assembly.GetName().Version.ToString());

                foreach (IObject @object in objects.Values)
                {
                    @object.Send(); 
                }
            }
        }

        //private void HighFrequencyPulseLoop()
        //{
        //    return; 
        //    DateTime periodStart = DateTime.Now;

        //    while (shouldExitPulseThread == false)
        //    {
        //        DateTime now = DateTime.Now;

        //        double ticks = now.Ticks - periodStart.Ticks;

        //        ticks /= 10000000.0;

        //        periodStart = now;

        //        lock (syncLock)
        //        {
        //            foreach (IOscPulsable source in oscPulseSources)
        //            {
        //                source.HighFrequencyPulseOsc(connection, ticks);
        //            }
        //        }

        //        connection.Flush();

        //        highFrequencPulseThread.Join(100);
        //    }

        //    highFrequencPulseThread = null;
        //}

        //private void LowFrequencPulseLoop()
        //{
        //    return;
        //    DateTime periodStart = DateTime.Now;

        //    while (shouldExitPulseThread == false)
        //    {
        //        DateTime now = DateTime.Now;

        //        double ticks = now.Ticks - periodStart.Ticks;

        //        ticks /= 10000000.0;

        //        periodStart = now;

        //        lock (syncLock)
        //        {
        //            foreach (IOscPulsable source in oscPulseSources)
        //            {
        //                source.LowFrequencyPulseOsc(connection, ticks);
        //            }
        //        }

        //        connection.Flush();

        //        lowFrequencPulseThread.Join(1000);
        //    }

        //    lowFrequencPulseThread = null;
        //}
    }
}