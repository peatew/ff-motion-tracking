﻿using System.Collections.Generic;
using System.Drawing;
using System.Xml;
using Rug.Host.Commands;
using Rug.Host.Comms;
using Rug.Host.Loading;
using Rug.Osc;

namespace Rug.Host.BaseClasses
{
    public abstract class ObjectBase : IObject
    {
        private readonly OscMemberCollection oscMembers;

        private readonly List<IOscMember> members = new List<IOscMember>(); 

        public IOscConnection Connection { get; private set; }

        [OscMember]
        public OscBool Enabled { get; private set; }

        public string Name { get; set; }

        public string ThreadName { get; protected set; } 

        [OscMember]
        public OscRectangleF Bounds { get; private set; } 

        public OscMemberCollection OscMembers { get { return oscMembers; } }

        public ObjectBase()
        {
            Enabled = new OscBool(this, nameof(Enabled), "/" + nameof(Enabled).ToLower(), true);
            Bounds = new OscRectangleF(this, nameof(Bounds), "/" + nameof(Bounds).ToLower(), new RectangleF(0, 0, 160, 40));

            oscMembers = new OscMemberCollection(members);
        }

        public virtual void Attach(IOscConnection connection)
        {
            Connection = connection;

            Connection?.Listener.Attach(Name, OnSendMessage); 

            OscCommandManager.AttachCommands(connection, this.GetType(), this, Name);

            members.Clear();
            members.AddRange(OscCommandManager.GetMemebers(this.GetType(), this));

            foreach (IOscMember member in OscMembers)
            {
                member.Attach(); 
            }
        }

        private void OnSendMessage(OscMessage message)
        {
            Send(); 
        }
        
        public virtual void Detach()
        {
            Connection?.Listener.Detach(Name, OnSendMessage);

            foreach (IOscMember member in OscMembers)
            {
                member.Detach();
            }

            OscCommandManager.DetachCommands(Connection, this.GetType(), this, Name);

            Connection = null;
        }

        public abstract void Bind(Workspace workspace);

        public abstract void Unbind();

        public virtual void Load(LoadContext context, XmlNode node)
        {
            Name = Helper.GetAttributeValue(node, "Name", string.Empty);

            if (string.IsNullOrEmpty(Name) == true)
            {
                context.Error("Missing Name attribute.");
            }

            if (OscAddress.IsValidAddressLiteral(Name) == false)
            {
                context.Error(string.Format("Invalid Name attribute \"{0}\".", Name));
            }

            members.Clear();
            members.AddRange(OscCommandManager.GetMemebers(this.GetType(), this));

            foreach (IOscMember member in OscMembers)
            {
                member.Load(context, node); 
            }
        }

        public virtual XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "Name", Name);

            foreach (IOscMember member in OscMembers)
            {
                member.Save(context, element);
            }

            return element;
        }

        public virtual void Send()
        {
            Connection?.Send(Name); 

            foreach (IOscMember member in OscMembers)
            {
                member.Send(); 
            }

            OscCommandManager.SendCommandUsageForTypeAndBaseTypes(Connection, this.GetType(), this, Name);
        }

        //[OscCommand("/enable", "Usage: [$basePath]/enable, f")]
        //private void Command_Enable(OscMessage message)
        //{
        //    if (message.Count > 0)
        //    {
        //        if ((message[0] is float) == false &&
        //            (message[0] is int) == false &&
        //            (message[0] is bool) == false)
        //        {
        //            Connection?.Send("/usage", OscCommandManager.GetCommandHelpString(typeof(ObjectBase), Name, "/enable"));

        //            return;
        //        }

        //        if ((message[0] is float) == true)
        //        {
        //            Enabled = (float)message[0] > 0.5f;
        //        }
        //        else if ((message[0] is int) == true)
        //        {
        //            Enabled = (int)message[0] >= 1;
        //        }
        //        else if ((message[0] is bool) == true)
        //        {
        //            Enabled = (bool)message[0];
        //        }
        //    }

        //    Connection?.Send(message.Address, Enabled ? 1f : 0f);
        //}
    }
}
