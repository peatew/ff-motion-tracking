﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using Rug.Host.Loading;
using Rug.Osc;

namespace Rug.Host.Commands
{
    public class OscObjectList<T> : IOscProperty, IOscObjectList, IList<T> where T : class, IObject
    {
        public readonly IObject Owner;

        private readonly List<T> innerList = new List<T>();

        private readonly string collectionName;
        private readonly string tag;

        public string AttributeName { get; private set; }

        public int Count { get { return innerList.Count; } }

        public bool IsFixedSize { get { throw new NotImplementedException(); } }

        public bool IsReadOnly { get { return ((IList<T>)innerList).IsReadOnly; } }

        public bool IsSynchronized { get { throw new NotImplementedException(); } }

        public string MemberName { get; private set; }

        public string OscAddress { get { return Owner.Name + MemberName; } }

        public object SyncRoot { get { throw new NotImplementedException(); } }

        public bool WasLoaded { get; private set; }

        public Type PropertyType { get { return typeof(T); } }

        object IList.this[int index]
        {
            get
            {
                return innerList[index];
            }

            set
            {
                innerList[index] = value as T;
            }
        }

        public T this[int index]
        {
            get
            {
                return innerList[index];
            }

            set
            {
                innerList[index] = value;
            }
        }

        public OscObjectList(IObject owner, string attributeName, string memberName, string collectionName = null, string tag = null)
        {
            Owner = owner;

            AttributeName = attributeName;

            MemberName = memberName;

            this.collectionName = collectionName;
            this.tag = tag;
        }

        #region OSC Object Functionality

        public void Attach()
        {
            Owner.Connection?.Listener.Attach(OscAddress, OnMessage);

            foreach (T item in this)
            {
                item.Attach(Owner.Connection); 
            }
        }

        public void Detach()
        {
            Owner.Connection?.Listener.Detach(Owner.Name + MemberName, OnMessage);

            foreach (T item in this)
            {
                item.Detach();
            }
        }

        public OscMessage GetMessage()
        {
            return new OscMessage(OscAddress, Count);
        }

        public void Load(LoadContext context, XmlNode node)
        {
            innerList.Clear();

            if (collectionName == null)
            {
                innerList.AddRange(Loader.LoadObjects<T>(context, tag, node));
            }
            else
            {
                innerList.AddRange(Loader.LoadObjects<T>(context, tag, node.SelectSingleNode(collectionName)));
            }
        }

        public void Save(LoadContext context, XmlElement element)
        {
            Loader.SaveObjects(context, collectionName, tag, innerList.ToArray(), element);
        }

        public void Send()
        {
            Owner.Connection?.Send(GetMessage());
        }

        public override string ToString()
        {
            return GetMessage().ToString();
        }

        protected void OnMessage(OscMessage message)
        {
            Send();
        }

        public void Bind(Workspace workspace)
        {
            foreach (T item in this)
            {
                item.Bind(workspace); 
            }
        }

        public void Unbind()
        {
            foreach (T item in this)
            {
                item.Unbind();
            }
        }

        #endregion OSC Object Functionality

        public void Add(T item)
        {
            innerList.Add(item);
        }

        public int Add(object value)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            innerList.Clear();
        }

        public bool Contains(T item)
        {
            return innerList.Contains(item);
        }

        public bool Contains(object value)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            innerList.CopyTo(array, arrayIndex);
        }

        public void CopyTo(Array array, int index)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<T> GetEnumerator()
        {
            return ((IList<T>)innerList).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IList<T>)innerList).GetEnumerator();
        }

        public int IndexOf(T item)
        {
            return innerList.IndexOf(item);
        }

        public int IndexOf(object value)
        {
            throw new NotImplementedException();
        }

        public void Insert(int index, T item)
        {
            innerList.Insert(index, item);
        }

        public void Insert(int index, object value)
        {
            throw new NotImplementedException();
        }

        public bool Remove(T item)
        {
            return innerList.Remove(item);
        }

        public void Remove(object value)
        {
            throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            innerList.RemoveAt(index);
        }
    }
}