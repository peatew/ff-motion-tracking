﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rug.Host.Commands
{
    public interface IOscObjectList : IList, IOscMember
    {
        void Bind(Workspace workspace);

        void Unbind();
    }
}
