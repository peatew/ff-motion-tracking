﻿using System;

namespace Rug.Host.Commands
{
    public delegate void BoundObjectEvent(IOscObjectBinding @obj);

    public interface IOscObjectBinding : IOscMember
    {
        string AttributeName { get; }

        string BindToName { get; set; }

        bool IsBound { get; }

        bool IsRequired { get; }

        Type BindingType { get; }

        event BoundObjectEvent BindingChanged;

        event BoundObjectEvent Bound;

        event BoundObjectEvent Unbound;

        void Bind(Workspace workspace);

        void Unbind();
    }

    public interface IOscObjectBinding<T> : IOscObjectBinding
        where T : IObject
    {
        T BoundTo { get; set; }
    }
}