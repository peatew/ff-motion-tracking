﻿using System;

namespace Rug.Host.Commands
{
    public class OscCommandAttribute : Attribute
    {
        public string OscAddress { get; }

        public string HelpString { get; }

        public OscCommandAttribute(string oscAddress, string helpString = "")
        {
            OscAddress = oscAddress;
            HelpString = helpString;
        }
     }
}
