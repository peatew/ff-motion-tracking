﻿using System;
using System.Collections.Generic;
using System.Xml;
using Rug.Host.Loading;
using Rug.Osc;

namespace Rug.Host.Commands
{
    public class ObjectBinding<T> : IOscObjectBinding<T>
        where T : IObject
    {
        private readonly object syncLock = new object();

        private string bindToName;

        private T boundTo;

        public string AttributeName { get; private set; }

        public Type BindingType { get { return typeof(T); } } 

        public string BindToName
        {
            get
            {
                if (IsBound == true)
                {
                    return BoundTo.Name;
                }
                else
                {
                    return bindToName;
                }
            }

            set
            {
                if (bindToName == value)
                {
                    return;
                }

                bindToName = value;

                if (IsBound == true)
                {
                    BoundTo = default(T);
                }
            }
        }

        public T BoundTo
        {
            get
            {
                return boundTo;
            }

            set
            {
                T initialValue;
                T newValue;

                //lock (syncLock)
                {
                    initialValue = boundTo;
                    newValue = value;

                    if (EqualityComparer<T>.Default.Equals(newValue, initialValue) == true)
                    {
                        return;
                    }

                    if (EqualityComparer<T>.Default.Equals(newValue, default(T)) == false)
                    {
                        Unbound?.Invoke(this);

                        lock (syncLock)
                        {
                            bindToName = newValue.Name;

                            boundTo = newValue;
                        }

                        Bound?.Invoke(this);
                    }
                    else
                    {
                        Unbound?.Invoke(this);

                        lock (syncLock)
                        {
                            boundTo = newValue;
                        }
                    }

                    BindingChanged?.Invoke(this);
                }
            }
        }

        public bool IsBound { get { return EqualityComparer<T>.Default.Equals(boundTo, default(T)) == false; } }

        public bool IsRequired { get; private set; }

        public string MemberName { get; private set; }

        public string OscAddress { get { return Owner.Name + MemberName; } }

        public IObject Owner { get; private set; }

        public event BoundObjectEvent BindingChanged;

        public event BoundObjectEvent Bound;

        public event BoundObjectEvent Unbound;

        public ObjectBinding(IObject owner, string attributeName, string memberName, bool required = true)
        {
            Owner = owner;
            AttributeName = attributeName;
            MemberName = memberName;
            IsRequired = required;
        }

        public bool TryGet(out T boundTo)
        {
            lock (syncLock)
            {
                boundTo = this.boundTo;

                return IsBound;
            }
        }

        public virtual void Attach()
        {
            Owner.Connection?.Listener.Attach(OscAddress, Command_OscBinding);
        }

        public void Bind(Workspace workspace)
        {
            BoundTo = workspace.FindObject<T>(BindToName);

            if (IsBound == true || IsRequired == false)
            {
                return;
            }

            Owner.Connection?.Send("/error/binding", "Object could not be found or is not of a valid type.", OscAddress, BindToName);
        }

        public virtual void Detach()
        {
            Owner.Connection?.Listener.Detach(Owner.Name + MemberName, Command_OscBinding);
        }

        public void Load(LoadContext context, XmlNode node)
        {
            BindToName = Helper.GetAttributeValue(node, AttributeName, string.Empty);

            if (string.IsNullOrEmpty(BindToName) == true && IsRequired == true)
            {
                context.Error(string.Format("Missing {0} attribute.", AttributeName));
            }
        }

        public void Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, AttributeName, BindToName);
        }

        public void Unbind()
        {
            BoundTo = default(T);
        }

        private void Command_OscBinding(OscMessage message)
        {
            if (message.Count == 1)
            {
                if ((message[0] is string) == false)
                {
                    Owner.Connection?.Send("/usage", string.Format("Usage: {0}, string", OscAddress));
                }
                else
                {
                    BindToName = message[0].ToString();
                }
            }

            Send();
        }

        public void Send()
        {
            Owner.Connection?.Send(GetMessage());
        }

        public OscMessage GetMessage()
        {
            return new OscMessage(OscAddress, IsBound ? boundTo.Name : BindToName, true);
        }

        public override string ToString()
        {
            return GetMessage().ToString();
        }        
    }
}