﻿using System;
using System.Drawing;
using System.Xml;
using OpenTK;
using Rug.Host.Loading;
using Rug.Osc;

namespace Rug.Host.Commands
{
    public abstract class OscProperty<T> : IOscProperty
    {
        public readonly T DefaultValue;

        public readonly IObject Owner;

        public T Value;

        public string AttributeName { get; private set; }

        public string MemberName { get; private set; }

        public string OscAddress { get { return Owner.Name + MemberName; } }

        public bool WasLoaded { get; private set; }

        public Type PropertyType { get { return typeof(T); } }

        public OscProperty(IObject owner, string attributeName, string memberName, T defaultValue)
        {
            Owner = owner;

            DefaultValue = defaultValue;

            AttributeName = attributeName;

            MemberName = memberName;
        }

        public virtual void Attach()
        {
            Owner.Connection?.Listener.Attach(OscAddress, OnMessage);
        }

        public virtual void Detach()
        {
            Owner.Connection?.Listener.Detach(Owner.Name + MemberName, OnMessage);
        }

        public override string ToString()
        {
            return GetMessage().ToString();
        }

        public abstract void Load(LoadContext context, XmlNode node);
        public abstract void Save(LoadContext context, XmlElement element);

        protected abstract void OnMessage(OscMessage message);
        public abstract OscMessage GetMessage();
        
        public void Send()
        {
            Owner.Connection?.Send(GetMessage());
        }
    }

    public class OscString : OscProperty<string>
    {
        public OscString(IObject owner, string attributeName, string memberName, string defaultValue) : base(owner, attributeName, memberName, defaultValue)
        {
        }

        public override void Load(LoadContext context, XmlNode node)
        {
            Value = Helper.GetAttributeValue(node, AttributeName, DefaultValue);

            if (DefaultValue.Equals(Value) == true)
            {
                context.Error(string.Format("Missing {0} attribute.", AttributeName));
            }
        }

        public override void Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, AttributeName, Value);
        }

        protected override void OnMessage(OscMessage message)
        {
            if (message.Count == 1)
            {
                if ((message[0] is string) == false)
                {
                    Owner.Connection?.Send("/usage", string.Format("Usage: {0}, string", OscAddress));
                }
                else
                {
                    Value = message[0].ToString();
                }
            }

            Send();
        }

        public override OscMessage GetMessage()
        {
            return new OscMessage(OscAddress, Value); 
        }
    }

    public class OscBool : OscProperty<bool>
    {
        public OscBool(IObject owner, string attributeName, string memberName, bool defaultValue) : base(owner, attributeName, memberName, defaultValue)
        {
        }

        public override void Load(LoadContext context, XmlNode node)
        {
            Value = Helper.GetAttributeValue(node, AttributeName, DefaultValue);
        }

        public override void Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, AttributeName, Value);
        }

        protected override void OnMessage(OscMessage message)
        {
            if (message.Count == 1)
            {
                if ((message[0] is bool) == false)
                {
                    Owner.Connection?.Send("/usage", string.Format("Usage: {0}, bool", OscAddress));
                }
                else
                {
                    Value = (bool)message[0];
                }
            }

            Send();
        }

        public override OscMessage GetMessage()
        {
            return new OscMessage(OscAddress, Value);
        }
    }

    public class OscFloat : OscProperty<float>
    {
        public OscFloat(IObject owner, string attributeName, string memberName, float defaultValue) : base(owner, attributeName, memberName, defaultValue)
        {
        }

        public override void Load(LoadContext context, XmlNode node)
        {
            Value = Helper.GetAttributeValue(node, AttributeName, DefaultValue);
        }

        public override void Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, AttributeName, Value);
        }

        protected override void OnMessage(OscMessage message)
        {
            if (message.Count == 1)
            {
                if ((message[0] is float) == false)
                {
                    Owner.Connection?.Send("/usage", string.Format("Usage: {0}, float", OscAddress));
                }
                else
                {
                    Value = (float)message[0];
                }
            }

            Send();
        }

        public override OscMessage GetMessage()
        {
            return new OscMessage(OscAddress, Value);
        }
    }

    public class OscInt : OscProperty<int>
    {
        public OscInt(IObject owner, string attributeName, string memberName, int defaultValue) : base(owner, attributeName, memberName, defaultValue)
        {
        }

        public override void Load(LoadContext context, XmlNode node)
        {
            Value = Helper.GetAttributeValue(node, AttributeName, DefaultValue);
        }

        public override void Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, AttributeName, Value);
        }

        protected override void OnMessage(OscMessage message)
        {
            if (message.Count == 1)
            {
                if ((message[0] is int) == false)
                {
                    Owner.Connection?.Send("/usage", string.Format("Usage: {0}, int", OscAddress));
                }
                else
                {
                    Value = (int)message[0];
                }
            }

            Send();
        }

        public override OscMessage GetMessage()
        {
            return new OscMessage(OscAddress, Value);
        }
    }

    public class OscPointF : OscProperty<PointF>
    {
        public OscPointF(IObject owner, string attributeName, string memberName, PointF defaultValue) : base(owner, attributeName, memberName, defaultValue)
        {
        }

        public override void Load(LoadContext context, XmlNode node)
        {
            Value = Helper.GetAttributeValue(node, AttributeName, DefaultValue);
        }

        public override void Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, AttributeName, Value);
        }

        protected override void OnMessage(OscMessage message)
        {
            if (message.Count == 2)
            {
                if ((message[0] is float) == false ||
                    (message[1] is float) == false)
                {
                    Owner.Connection?.Send("/usage", string.Format("Usage: {0}, float, float", OscAddress));
                }
                else
                {
                    Value = new PointF((float)message[0], (float)message[1]);
                }
            }

            Send();
        }

        public override OscMessage GetMessage()
        {
            return new OscMessage(OscAddress, Value.X, Value.Y);
        }
    }

    public class OscVector2 : OscProperty<Vector2>
    {
        public OscVector2(IObject owner, string attributeName, string memberName, Vector2 defaultValue) : base(owner, attributeName, memberName, defaultValue)
        {
        }

        public override void Load(LoadContext context, XmlNode node)
        {
            Value = Helper.GetAttributeValue(node, AttributeName, DefaultValue);
        }

        public override void Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, AttributeName, Value);
        }

        protected override void OnMessage(OscMessage message)
        {
            if (message.Count == 2)
            {
                if ((message[0] is float) == false ||
                    (message[1] is float) == false)
                {
                    Owner.Connection?.Send("/usage", string.Format("Usage: {0}, float, float", OscAddress));
                }
                else
                {
                    Value = new Vector2((float)message[0], (float)message[1]);
                }
            }

            Send();
        }

        public override OscMessage GetMessage()
        {
            return new OscMessage(OscAddress, Value.X, Value.Y);
        }
    }

    public class OscVector3 : OscProperty<Vector3>
    {
        public OscVector3(IObject owner, string attributeName, string memberName, Vector3 defaultValue) : base(owner, attributeName, memberName, defaultValue)
        {
        }

        public override void Load(LoadContext context, XmlNode node)
        {
            Value = Helper.GetAttributeValue(node, AttributeName, DefaultValue);
        }

        public override void Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, AttributeName, Value);
        }

        protected override void OnMessage(OscMessage message)
        {
            if (message.Count == 3)
            {
                if ((message[0] is float) == false ||
                    (message[1] is float) == false ||
                    (message[2] is float) == false)
                {
                    Owner.Connection?.Send("/usage", string.Format("Usage: {0}, float, float, float", OscAddress));
                }
                else
                {
                    Value = new Vector3((float)message[0], (float)message[1], (float)message[2]);
                }
            }

            Send();
        }

        public override OscMessage GetMessage()
        {
            return new OscMessage(OscAddress, Value.X, Value.Y, Value.Z);
        }
    }

    public class OscVector4 : OscProperty<Vector4>
    {
        public OscVector4(IObject owner, string attributeName, string memberName, Vector4 defaultValue) : base(owner, attributeName, memberName, defaultValue)
        {
        }

        public override void Load(LoadContext context, XmlNode node)
        {
            Value = Helper.GetAttributeValue(node, AttributeName, DefaultValue);
        }

        public override void Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, AttributeName, Value);
        }

        protected override void OnMessage(OscMessage message)
        {
            if (message.Count == 4)
            {
                if ((message[0] is float) == false ||
                    (message[1] is float) == false ||
                    (message[2] is float) == false ||
                    (message[4] is float) == false)
                {
                    Owner.Connection?.Send("/usage", string.Format("Usage: {0}, float, float, float", OscAddress));
                }
                else
                {
                    Value = new Vector4((float)message[0], (float)message[1], (float)message[2], (float)message[3]);
                }
            }

            Send();
        }

        public override OscMessage GetMessage()
        {
            return new OscMessage(OscAddress, Value.X, Value.Y, Value.Z, Value.W);
        }
    }

    public class OscQuaternion : OscProperty<Quaternion>
    {
        public OscQuaternion(IObject owner, string attributeName, string memberName, Quaternion defaultValue) : base(owner, attributeName, memberName, defaultValue)
        {
        }

        public override void Load(LoadContext context, XmlNode node)
        {
            Value = Helper.GetAttributeValue(node, AttributeName, DefaultValue);
        }

        public override void Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, AttributeName, Value);
        }

        protected override void OnMessage(OscMessage message)
        {
            if (message.Count == 4)
            {
                if ((message[0] is float) == false ||
                    (message[1] is float) == false ||
                    (message[2] is float) == false ||
                    (message[4] is float) == false)
                {
                    Owner.Connection?.Send("/usage", string.Format("Usage: {0}, float, float, float, float", OscAddress));
                }
                else
                {
                    Value = new Quaternion((float)message[0], (float)message[1], (float)message[2], (float)message[3]);
                }
            }

            Send();
        }

        public override OscMessage GetMessage()
        {
            return new OscMessage(OscAddress, Value.X, Value.Y, Value.Z, Value.W);
        }
    }


    public class OscRectangle : OscProperty<Rectangle>
    {
        public OscRectangle(IObject owner, string attributeName, string memberName, Rectangle defaultValue) : base(owner, attributeName, memberName, defaultValue)
        {
        }

        public override void Load(LoadContext context, XmlNode node)
        {
            Value = Helper.GetAttributeValue(node, AttributeName, DefaultValue);
        }

        public override void Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, AttributeName, Value);
        }

        protected override void OnMessage(OscMessage message)
        {
            if (message.Count == 4)
            {
                if ((message[0] is int) == false ||
                    (message[1] is int) == false ||
                    (message[2] is int) == false ||
                    (message[4] is int) == false)
                {
                    Owner.Connection?.Send("/usage", string.Format("Usage: {0}, float, float, float", OscAddress));
                }
                else
                {
                    Value = new Rectangle((int)message[0], (int)message[1], (int)message[2], (int)message[3]);
                }
            }

            Send();
        }

        public override OscMessage GetMessage()
        {
            return new OscMessage(OscAddress, Value.X, Value.Y, Value.Width, Value.Height);
        }
    }

    public class OscRectangleF : OscProperty<RectangleF>
    {
        public OscRectangleF(IObject owner, string attributeName, string memberName, RectangleF defaultValue) : base(owner, attributeName, memberName, defaultValue)
        {
        }

        public override void Load(LoadContext context, XmlNode node)
        {
            Value = Helper.GetAttributeValue(node, AttributeName, DefaultValue);
        }

        public override void Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, AttributeName, Value);
        }

        protected override void OnMessage(OscMessage message)
        {
            if (message.Count == 4)
            {
                if ((message[0] is float) == false ||
                    (message[1] is float) == false ||
                    (message[2] is float) == false ||
                    (message[4] is float) == false)
                {
                    Owner.Connection?.Send("/usage", string.Format("Usage: {0}, float, float, float", OscAddress));
                }
                else
                {
                    Value = new RectangleF((float)message[0], (float)message[1], (float)message[2], (float)message[3]);
                }
            }

            Send();
        }

        public override OscMessage GetMessage()
        {
            return new OscMessage(OscAddress, Value.X, Value.Y, Value.Width, Value.Height);
        }
    }    
}