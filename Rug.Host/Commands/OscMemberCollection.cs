﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Rug.Host.Commands
{
    public class OscMemberCollection : ReadOnlyCollection<IOscMember>
    {
        public OscMemberCollection(IList<IOscMember> list) : base(list) { }
    }
}
