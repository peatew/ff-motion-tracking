﻿using System.Xml;
using Rug.Host.Loading;

namespace Rug.Host.Commands
{
    public interface IOscMember
    {
        string MemberName { get; }
        string OscAddress { get; }

        void Attach();
        void Detach();

        void Load(LoadContext context, XmlNode node);
        void Save(LoadContext context, XmlElement element);

        void Send(); 
    }
}
