﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Rug.Host.Comms;
using Rug.Osc;

namespace Rug.Host.Commands
{
    public static class OscCommandManager
    {
        private static readonly object syncObject = new object();
        private static readonly Dictionary<Type, OscCommandnfo[]> typeCommandsLookup = new Dictionary<Type, OscCommandnfo[]>();
        private static readonly Dictionary<Type, Dictionary<string, string>> typeHelpStringsLookup = new Dictionary<Type, Dictionary<string, string>>();

        public static void AttachCommands(IOscConnection connection, Type type, object instance, string basePath)
        {
            Type typeToScan = type;

            do
            {
                AttachCommandsForType(connection, typeToScan, instance, basePath);

                typeToScan = typeToScan.BaseType;
            }
            while (typeToScan != typeof(object));
        }

        public static void DetachCommands(IOscConnection connection, Type type, object instance, string basePath)
        {
            Type typeToScan = type;

            do
            {
                DetachCommandsForType(connection, typeToScan, instance, basePath);

                typeToScan = typeToScan.BaseType;
            }
            while (typeToScan != typeof(object));
        }

        public static string GetCommandHelpString(Type type, string basePath, string oscAddress)
        {
            return GetCommandHelpString(type, oscAddress).Replace("[$basePath]", basePath);
        }

        public static string GetCommandHelpString(Type type, string oscAddress)
        {
            Dictionary<string, string> helpStrings;
            if (typeHelpStringsLookup.TryGetValue(type, out helpStrings) == false)
            {
                return "";
            }

            string helpString;
            if (helpStrings.TryGetValue(oscAddress, out helpString) == false)
            {
                return "";
            }

            return helpString;
        }

        public static OscCommandnfo[] GetCommandsForType(Type type, IReporter reporter)
        {
            OscCommandnfo[] result;

            lock (syncObject)
            {
                if (typeCommandsLookup.TryGetValue(type, out result) == true)
                {
                    return result;
                }

                result = ScanTypeForCommands(type, reporter);

                // stash for the future
                typeCommandsLookup.Add(type, result);

                BuildHelpStringLookup(type, result);

                return result;
            }
        }

        public static IOscMember[] GetMemebers(Type type, object instance)
        {
            List<IOscMember> members = new List<IOscMember>();

            //Type typeToScan = type;

            //do
            //{
                members.AddRange(GetMembersForType(type, instance));

                //typeToScan = typeToScan.BaseType;
            //}
            //while (typeToScan != typeof(object));

            return members.ToArray(); 
        }

        public static void SendCommandUsageForTypeAndBaseTypes(IOscConnection connection, Type type, object instance, string basePath)
        {
            Type typeToScan = type;

            do
            {
                foreach (OscCommandnfo info in GetCommandsForType(typeToScan, connection.Reporter))
                {
                    connection?.Send("/usage", info.HelpString.Replace("[$basePath]", basePath));
                }

                typeToScan = typeToScan.BaseType;
            }
            while (typeToScan != typeof(object));
        }

        private static void AttachCommandsForType(IOscConnection connection, Type type, object instance, string basePath)
        {
            foreach (OscCommandnfo command in OscCommandManager.GetCommandsForType(type, connection.Reporter))
            {
                connection.Listener.Attach(basePath + command.OscAddress, command.GetForObject(instance));
            }
        }

        private static void BuildHelpStringLookup(Type type, OscCommandnfo[] commands)
        {
            Dictionary<string, string> lookup = new Dictionary<string, string>();

            foreach (OscCommandnfo info in commands)
            {
                lookup.Add(info.OscAddress, info.HelpString);
            }

            typeHelpStringsLookup.Add(type, lookup);
        }

        private static void DetachCommandsForType(IOscConnection connection, Type type, object instance, string basePath)
        {
            foreach (OscCommandnfo command in OscCommandManager.GetCommandsForType(type, connection.Reporter))
            {
                connection.Listener.Detach(basePath + command.OscAddress, command.GetForObject(instance));
            }
        }

        private static IOscMember[] GetMembersForType(Type type, object instance)
        {
            IOscMember[] result;

            lock (syncObject)
            {
                //if (typeCommandsLookup.TryGetValue(type, out result) == true)
                //{
                //    return result;
                //}

                result = ScanTypeForOscMembers(type, instance);

                //// stash for the future
                //typeCommandsLookup.Add(type, result);

                //BuildHelpStringLookup(type, result);

                return result;
            }
        }

        private static OscCommandnfo[] ScanTypeForCommands(Type type, IReporter reporter)
        {
            List<OscCommandnfo> commands = new List<OscCommandnfo>();

            foreach (MethodInfo method in type.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
            {
                object[] attributes = method.GetCustomAttributes(typeof(OscCommandAttribute), true);

                if (attributes.Length == 0)
                {
                    continue;
                }

                OscCommandAttribute attribute = attributes[0] as OscCommandAttribute;

                ParameterInfo[] parameters = method.GetParameters();

                if (method.ReturnType != typeof(void))
                {
                    reporter.PrintError("Osc Command {0} on type {1} does not have the correct signature. Invalid return type.", method.Name, type.FullName);
                    continue;
                }

                if (parameters.Length != 1)
                {
                    reporter.PrintError("Osc Command {0} on type {1} does not have the correct signature. Invalid arguments.", method.Name, type.FullName);
                    continue;
                }

                if (parameters[0].ParameterType != typeof(OscMessage))
                {
                    reporter.PrintDebug("Osc Command {0} on type {1} does not have the correct signature. Invalid argument type {2}.", method.Name, type.FullName, parameters[0].ParameterType.FullName);
                    continue;
                }

                commands.Add(new OscCommandnfo(attribute, method));
            }

            return commands.ToArray();
        }

        private static IOscMember[] ScanTypeForOscMembers(Type type, object instance)
        {
            List<IOscMember> members = new List<IOscMember>();

            foreach (PropertyInfo property in type.GetProperties(BindingFlags.GetProperty | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
            {
                object[] attributes = property.GetCustomAttributes(typeof(OscMemberAttribute), true);

                if (attributes.Length == 0)
                {
                    continue;
                }

                OscCommandAttribute attribute = attributes[0] as OscCommandAttribute;

                members.Add(property.GetGetMethod().Invoke(instance, new object[0]) as IOscMember);
            }

            return members.ToArray();
        }
    }

    public class OscCommandnfo
    {
        public readonly MethodInfo Method;
        private readonly OscCommandAttribute CommandDetails;

        public string HelpString { get { return CommandDetails.HelpString; } }

        public string OscAddress { get { return CommandDetails.OscAddress; } }

        public OscCommandnfo(OscCommandAttribute details, MethodInfo method)
        {
            CommandDetails = details;
            Method = method;
        }

        public OscMessageEvent GetForObject(object instance)
        {
            return (OscMessageEvent)Delegate.CreateDelegate(typeof(OscMessageEvent), instance, Method);
        }
    }
}