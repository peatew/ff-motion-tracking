﻿using System;

namespace Rug.Host.Commands
{
    public interface IOscProperty : IOscMember
    {
        bool WasLoaded { get; }

        Type PropertyType { get; } 
    }
}