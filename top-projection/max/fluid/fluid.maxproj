{
	"name" : "fluid",
	"version" : 1,
	"creationdate" : 3549269006,
	"modificationdate" : 3550128158,
	"viewrect" : [ 687.0, 235.0, 486.0, 570.0 ],
	"autoorganize" : 1,
	"hideprojectwindow" : 0,
	"showdependencies" : 1,
	"autolocalize" : 0,
	"contents" : 	{
		"patchers" : 		{
			"fluid-exciter-world.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"add_border.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"add_border_obstacles.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"fluid-exciter.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"osc-ff.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"old-fluid_gpu.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"fluid_gpu.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"fluid_gpu_hack.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"jit-world-example.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"osc-mockup.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}

		}
,
		"code" : 		{
			"_main-dev.js" : 			{
				"kind" : "javascript",
				"local" : 1
			}
,
			"ab.hsflow.jxs" : 			{
				"kind" : "shader",
				"local" : 1
			}
,
			"tut.lumagate.jxs" : 			{
				"kind" : "shader",
				"local" : 1
			}
,
			"luma.fp.glsl" : 			{
				"kind" : "shader",
				"local" : 1
			}

		}

	}
,
	"layout" : 	{

	}
,
	"searchpath" : 	{

	}
,
	"detailsvisible" : 1,
	"amxdtype" : 1633771873,
	"readonly" : 0,
	"devpathtype" : 0,
	"devpath" : "."
}
