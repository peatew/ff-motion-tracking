//setup for single texture
varying vec2 texcoord0;
uniform sampler2DRect tex0;

//luma threshold
uniform vec2 lum;
const vec4 lumcoeff = vec4(0.299, 0.587, 0.114, 0.);


void main() {
  vec4 a = texture2DRect(tex0, texcoord0);

  // calculate our luminance
  float luminance = dot(a, lumcoeff);

  // compare to the thresholds
  float clo = smoothstep(lum.x, clamp(lum.x + 0.2, 0.0, 1.0), luminance);
  float chi = smoothstep(clamp(luminance - 0.2, 0.0, 1.0), luminance, lum.y);

  //combine the comparisons
  float amask = clo * chi;

  // output texture with alpha-mask
  gl_FragColor = vec4(a.rgb, amask);
}
