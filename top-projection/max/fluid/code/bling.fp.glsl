//setup for single texture
varying vec2 texcoord0;
uniform sampler2DRect tex0;
uniform float twist;
uniform float spread;
uniform float wet_mix;


vec3 rgb2hsv(vec3 c)
{
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

void main() {
  vec4 a = texture2DRect(tex0, texcoord0);

  float level = rgb2hsv(vec3(a)).z;
  float level_r = 0.5 + 0.5 * sin(a.r * (twist - spread));
  float level_g = 0.5 + 0.5 * sin(a.g * twist);
  float level_b = 0.5 + 0.5 * sin(a.b * (twist + spread));

  vec3 calculatedColour = vec3(level_r, level_g, level_b);

  vec3 c = mix(a.rgb, smoothstep(level, 0.0, vec3(level_r, level_g, level_b)), wet_mix);
  gl_FragColor = vec4(c, 1.0);
  //gl_FragColor = vec4(vec3(level), 1.0);
  //gl_FragColor = a;
}
