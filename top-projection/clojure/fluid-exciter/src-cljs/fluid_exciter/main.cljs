(ns fluid-exciter.main
  (:require [thi.ng.geom.core :as g]
            [thi.ng.geom.vector :as v]
            [thi.ng.color.gradients :as grad]
            [thi.ng.math.core :as m]
            [eu.cassiel.twizzle :as tw]
            [fluid-exciter.streamers :as sx]))

;; Outlets
(def TO-SKETCH 0)
(def TO-BACKGROUND 1)

(def Z -0.4)                            ; Hack to scale in 3D without orthographics

(def NUM-COLOURS 100)

(def my-grad
  (->> :green-cyan
       (grad/cosine-schemes)
       (apply grad/cosine-gradient NUM-COLOURS)))

(def shuffled-gradient (shuffle my-grad))

(def size {:w 800
           :h 600})

(def state (atom {:curr-data nil        ; map of id -> [px py vx vy]
                  :prev-data nil
                  :frame 0
                  ;; Automation values are [px py size]
                  :automation sx/initial-auto
                  :blank false}))

(defn clear [me]
  (doto me
    (.outlet TO-SKETCH "reset")
    (.outlet TO-SKETCH "glclearcolor" 0 0 0 1)
    (.outlet TO-SKETCH "glclear")))

(defn centre-pattern [me]
  (doto me
    (.outlet TO-SKETCH "glcolor" 1 1 1 1)
    (.outlet TO-SKETCH "moveto" 0 0)
    (.outlet TO-SKETCH "circle" (rand))))

(defn blobs-blob [me idx px py vx vy]
  (swap! state
         update
         :curr-data
         assoc idx [px py vx vy]))

(defn fire-streamer [id from to from-c to-c]
  (swap! state update :automation
         sx/add-streamer
         :id id
         :max-age 30
         :from from
         :to to
         :from-c from-c
         :to-c to-c))

(defn debug-auto [me]
  (.post me (str (:automation @state) \newline)))

(defn draw-streamers [me]
  (let [auto (:automation @state)]
    (doseq [id (range sx/NUM-AUTO-CHANS)]
      (let [samp (tw/sample auto (sx/auto-key id))
            [px py size r g b] samp]
        ;;(.post me (str samp \newline))
        (when size
          (doto me
            (.outlet TO-SKETCH "moveto" px py Z)
            (.outlet TO-SKETCH "glcolor" r g b 1)
            (.outlet TO-SKETCH "circle" size)))))))

(defn track-pattern [me]
  (let [s' (swap! state #(-> % (assoc :prev-data (:curr-data %)
                                      :curr-data nil)))
        hits (:prev-data s')]
    (doseq [[id [px py vx vy]] hits
            ;;:let [px (dec (* px 2))]
            ;;:let [py (dec (* py 2))]
            ;;:let [px (* px 0.75)]       ; 4:3 aspect.
            ;;:let [py (* py 0.75)]       ; 4:3 aspect.
            :let [px (- px)]
            :let [v-abs (g/dist (v/vec2 vx vy) (v/vec2 0 0))]]
      (when (> v-abs 0.001)
        ;;(.post me (str "v-abs " v-abs \newline))
        (let [;;idx (m/clamp (int (* v-abs 250)) 0 (dec NUM-COLOURS))
              rgb (nth shuffled-gradient (mod id NUM-COLOURS))
              ;;rgb (nth my-grad idx)
              [r g b] (map #(m/clamp (int (* % 255)) 0 255) rgb)
              [r g b] [1 1 1]
              ]
          (doto me
            (.outlet TO-SKETCH "moveto" px py Z)
            (.outlet TO-SKETCH "glcolor" r g b 1)
            ;;(.outlet TO-SKETCH "shapeprim" "points")
            (.outlet TO-SKETCH "circle" #_ 0.03 (min 99 #_ 0.1 (* v-abs 3)))
            ;;(.outlet TO-SKETCH "moveto" px (- py))
            ;;(.outlet TO-SKETCH "glcolor" 1 1 1 1)
            ;;(.outlet TO-SKETCH "circle" (* v-abs 1))
            ;;(.post (str "str-x " str-x " str-y " str-y \newline))

            ;;(.outlet TO-BACKGROUND "setall" 255 (int (* dist 255)) (int (* dist 255)))
            ))
        #_ (let [;;idx (m/clamp (int (* v-abs 250)) 0 (dec NUM-COLOURS))
              rgb (nth shuffled-gradient (mod id NUM-COLOURS))
              ;;rgb (nth my-grad idx)
              [r g b] (map #(m/clamp (int (* % 255)) 0 255) rgb)]
          (.outlet me TO-BACKGROUND "setall" r g b)
          (.outlet me TO-BACKGROUND "bang"))))))

(defn blobs-merge [me id1 id2 px py]
  (fire-streamer 0
                 [px py]
                 [(* px 2) (* py 2)]
                 [1 0 0]
                 [1 0 0]
                 ))

(defn blobs-die [me id px py]
  ;;(.post me (str "die " id " " px " " py \newline))
  (fire-streamer 1
                 [px py]
                 [0 0]
                 [1.0 1.0 0]
                 [1.0 0.8 0]
                 ))

(def dispatch {"/blobs/blob"  blobs-blob
               "/blobs/merge" blobs-merge
               "/blobs/die"   blobs-die
               })

(defn osc [me msg & args]
  (when-let [f (dispatch msg)]
    (apply f me args)
    ;;(.error me (str "unknown command: " msg \newline))
    ))

(defn bang [me]
  (clear me)
  (let [s (swap! state #(as-> % S
                          (update S :blank not)
                          (update S :frame inc)
                          (update S :automation tw/locate (:frame S))))]
    (when (:blank s)
      ;;(centre-pattern me)
      (draw-streamers me)
      (track-pattern me))))

(this-as me
         (set! (.-bang me) (partial bang me))
         (set! (.-clear me) (partial clear me))
         (set! (.-osc me) (partial osc me))
         (set! (.-debug_auto me) (partial debug-auto me))
         (set! (.-autowatch me) 1)
         (set! (.-outlets me) 2)
         (.post me (str "fluid-exciter: " (js/Date) \newline)))
