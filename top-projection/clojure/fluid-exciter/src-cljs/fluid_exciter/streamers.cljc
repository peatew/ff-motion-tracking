(ns fluid-exciter.streamers
  (:require [eu.cassiel.twizzle :as tw]
            [eu.cassiel.twizzle.interpolators :as twi]))

(def NUM-AUTO-CHANS 3)

(defn auto-key [k]
  (keyword (str "chan-" k)))

(def initial-auto (tw/initial :init (reduce (fn [m k] (assoc m (auto-key k) [0 0 0
                                                                            0 0 0]))
                                            nil
                                            (range NUM-AUTO-CHANS))
                              :interp (reduce (fn [m k] (assoc m (auto-key k) twi/interp-vectors))
                                              nil
                                              (range NUM-AUTO-CHANS))))

(defn add-streamer [state & {:keys [id max-age from to from-c to-c]}]
  (let [[from-x from-y] from
        [to-x to-y] to
        [fr fg fb] from-c
        [tr tg tb] to-c
        k (auto-key id)]
    (-> state
        (tw/automate-in k 0 0 [from-x from-y 0.2 fr fg fb])
        (tw/automate-in k 1 max-age [to-x to-y 0 tr tg tb])
        (tw/automate-in k (inc max-age) 1 [to-x to-y 0 0 0 0])
        )))
