(defproject fluid-exciter "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/clojurescript "1.9.76"]
                 [com.stuartsierra/component "0.3.1"]
                 [thi.ng/geom "0.0.1158-SNAPSHOT"]
                 [thi.ng/color "0.3.0"]
                 [thi.ng/math "0.2.1"]
                 [eu.cassiel/twizzle "0.6.0"]]
  :plugins [[lein-cljsbuild "1.1.3"]
            [cider/cider-nrepl "0.9.1"]
            [refactor-nrepl "1.1.0"]]
  :main ^:skip-aot fluid-exciter.main
  :target-path "target/%s"
  :profiles {:dev {:dependencies [[org.clojure/tools.namespace "0.2.4"]
                                  [org.clojure/tools.nrepl "0.2.7"]]
                   :source-paths ["dev"]}
             :uberjar {:aot :all}}
  :cljsbuild {:builds [{:id "prod",
                        :compiler {:source-paths ["src-cljs"]
                                   :optimizations :simple,
                                   :output-to "../../max/fluid/code/_main.js",
                                   :pretty-print false}}
                       {:id "dev",
                        :compiler {:source-paths ["src-cljs"]
                                   :optimizations :whitespace,
                                   :output-to "../../max/fluid/code/_main-dev.js",
                                   :pretty-print true}}]})
