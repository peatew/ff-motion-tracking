(ns user
  (:require [thi.ng.geom.core :as g]
            [thi.ng.geom.vector :as v]
            [thi.ng.color.gradients :as grad]
            [thi.ng.math.core :as m]))

(keys grad/cosine-schemes)

(->> :yellow-red
     (grad/cosine-schemes)
     (apply grad/cosine-gradient 100))

(->> :blue-white-red
     (grad/cosine-schemes)
     (apply grad/cosine-gradient 100))

(g/dist (v/vec2 1 1)
        (v/vec2 0 0))

(shuffle (vec (range 10)))


(seq {:A 1 :B 2})

(vals {})
