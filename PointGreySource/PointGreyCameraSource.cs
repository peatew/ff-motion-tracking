﻿using System.Xml;
using Rug.CV;
using Rug.CV.BaseClasses;
using Rug.Host;
using Rug.Host.Loading;
using System;

namespace PointGreySource
{
    [XmlName("PointGrey.Camera")]
    public partial class PointGreyCameraSource : ImageSourceBase, IDebugImageSource
    {
        private PointGeryCameraInfo info;

        public string SerialNumber { get; set; }

        public override event ImageSourceFrameEvent NewFrame;

        public PointGreyCameraSource()
        {
            PointGreyMaster.RegisterInstance();
        }

        public override void Bind(Workspace service)
        {
            if (PointGreyMaster.Instance.Cameras.TryGetValue(SerialNumber, out info) == false)
            {
                Connection?.Reporter?.PrintError("Could not find camera {0}.", SerialNumber);
                return;
            }

            Connect();
        }

        public override void Load(LoadContext context, XmlNode node)
        {
            base.Load(context, node);

            SerialNumber = Helper.GetAttributeValue(node, "SerialNumber", string.Empty);

            if (string.IsNullOrEmpty(SerialNumber) == true)
            {
                context.Error("Missing SerialNumber attribute.");
            }
        }

        public override XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "SerialNumber", SerialNumber);

            return base.Save(context, element);
        }

        public override void Unbind()
        {
            try
            {
                Disconnect();
            }
            catch (Exception ex)
            {
                Connection?.Reporter?.PrintException(ex, "Exception while disconnecting from camera device");
            }
        }
    }
}