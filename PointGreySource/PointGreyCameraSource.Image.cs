﻿using System;
using System.Threading;
using FlyCapture2Managed;

namespace PointGreySource
{
    partial class PointGreyCameraSource
    {
        ManagedCamera camera;

        ManagedImage managedImage;
        ManagedImage convertedImage;

        Thread readThread;

        bool shouldExit = false;

        private void Connect()
        {
            if (info == null)
            {
                return;
            }

            camera = new ManagedCamera();

            // Connect to a camera
            camera.Connect(info.ID);

            // Get embedded image info from camera
            EmbeddedImageInfo embeddedInfo = camera.GetEmbeddedImageInfo();

            //// Enable timestamp collection	
            //if (embeddedInfo.timestamp.available == true)
            //{
            //    embeddedInfo.timestamp.onOff = true;
            //}

            // Set embedded image info to camera
            //camera.SetEmbeddedImageInfo(embeddedInfo);

            // Start capturing images
            camera.StartCapture();

            managedImage = new ManagedImage();
            convertedImage = new ManagedImage();

            readThread = new Thread(ReadLoop);
            readThread.Name = Name; 
            shouldExit = false; 
            readThread.Start(); 

            // Create a converted image
            // ManagedImage convertedImage = new ManagedImage();


            //for (int imageCnt = 0; imageCnt < k_numImages; imageCnt++)
            //{
            //    // Retrieve an image
            //    camera.RetrieveBuffer(managedImage);

            //    // Convert the raw image
            //    managedImage.Convert(PixelFormat.PixelFormatBgr, convertedImage);

            //    // Create a unique filename
            //    string filename = String.Format(
            //        "FlyCapture2Test_CSharp-{0}-{1}.bmp",
            //        camInfo.serialNumber,
            //        imageCnt);

            //    // Get the Bitmap object. Bitmaps are only valid if the
            //    // pixel format of the ManagedImage is RGB or RGBU.
            //    System.Drawing.Bitmap bitmap = convertedImage.bitmap;

            //    // Save the image
            //    bitmap.Save(filename);
            //}

        }

        private void ReadLoop()
        {
            ThreadName = System.Threading.Thread.CurrentThread.Name;

            while (shouldExit == false)
            {
                try
                {
                    camera.RetrieveBuffer(managedImage);

                    if (Enabled.Value == false)
                    {
                        continue;
                    }

                    managedImage.Convert(PixelFormat.PixelFormatMono8, convertedImage);

                    NewDebugFrame?.Invoke(this, convertedImage.bitmap);

                    NewFrame?.Invoke(this, DateTime.Now, convertedImage.bitmap);

                    Connection?.Send(Name + "/frame");
                }
                catch (Exception ex) 
                {
                    Connection?.Send("/error", ex.Message); 
                }
            }
        }

        private void Disconnect()
        {
            shouldExit = true;

            readThread?.Join(2000);
            readThread = null;

            try
            {
                // Stop capturing images
                camera?.StopCapture();
            }
            catch (Exception ex)
            {
                Connection?.Send("/error", ex.Message);
            }

            try
            {
                // Disconnect the camera
                camera?.Disconnect();
            }
            catch (Exception ex)
            {
                Connection?.Send("/error", ex.Message);
            }

            managedImage?.Dispose();
            convertedImage?.Dispose(); 

            camera = null; 
        }
    }
}
