﻿using Rug.Host;

namespace PointGreySource
{
    partial class PointGreyCameraSource
    {	
		public bool DebuggerAttached { get; set; }

        public event DebugFrameEvent NewDebugFrame;
    }
}
