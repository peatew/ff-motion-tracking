﻿using System;
using System.Collections.Generic;
using FlyCapture2Managed;
using Rug.Host;
using Rug.Host.Loading;

namespace PointGreySource
{
    public class PointGeryCameraInfo
    {
        public readonly ManagedPGRGuid ID;
        public readonly string SerialNumber;
        public readonly string ModelName;
        public readonly string VendorName;
        public readonly string SensorInfo;
        public readonly string SensorResolution;

        public bool Reserved { get; internal set; }

        public PointGeryCameraInfo(
            ManagedPGRGuid id,
            string serialNumber,
            string modelName,
            string vendorName,
            string sensorInfo,
            string sensorResolution)
        {
            ID = id;
            SerialNumber = serialNumber;
            ModelName = modelName;
            VendorName = vendorName;
            SensorInfo = sensorInfo;
            SensorResolution = sensorResolution; 
        }
    }

    public class PointGreyMaster : IStaticResource
    {
        public const string ResourceKey = "PointGreyMaster";

        public readonly Dictionary<string, PointGeryCameraInfo> Cameras = new Dictionary<string, PointGeryCameraInfo>();

        private ManagedBusManager managedBusManager;

        public static readonly PointGreyMaster Instance;

        static PointGreyMaster()
        {
            Instance = new PointGreyMaster();

            StaticResourceMaster.RegisterStaticResource(ResourceKey, Instance);
        }

        public static void RegisterInstance()
        {

        }

        private PointGreyMaster()
        {
        }

        public void LoadResources(LoadContext context)
        {
            UnloadResources();

            try
            {
                managedBusManager = new ManagedBusManager();

                uint numberOfCameras = managedBusManager.GetNumOfCameras();

                for (uint i = 0; i < numberOfCameras; i++)
                {
                    try
                    {
                        ManagedPGRGuid guid = managedBusManager.GetCameraFromIndex(i);

                        using (ManagedCamera cam = new ManagedCamera())
                        {
                            // Connect to a camera
                            cam.Connect(guid);

                            // Get the camera information
                            CameraInfo camInfo = cam.GetCameraInfo();

                            PointGeryCameraInfo info = new PointGeryCameraInfo(
                                guid,
                                camInfo.serialNumber.ToString(),
                                camInfo.modelName,
                                camInfo.vendorName,
                                camInfo.sensorInfo,
                                camInfo.sensorResolution);

                            Cameras.Add(info.SerialNumber, info);
                        }
                    }
                    catch (Exception ex)
                    {
                        context.Error("Error acquiring information from camera " + i + ". " + ex.Message); 
                    }
                }
            }
            catch (Exception ex)
            {
                context.Error("Error starting camera manager. " + ex.Message);
            }
        }

        public void UnloadResources()
        {
            managedBusManager?.Dispose();
            managedBusManager = null;

            Cameras.Clear();
        }
    }
}