﻿using Rug.Host.Commands;
using Rug.Osc;

namespace PointGreySource
{
    partial class PointGreyCameraSource
    {
        [OscCommand("/modelName", "Usage: [$basePath]/modelName")]
        private void Command_ModelName(OscMessage message)
        {
            Connection?.Send(Name + "/modelName", info == null ? "" : info.ModelName);
        }

        [OscCommand("/info", "Usage: [$basePath]/info")]
        private void Command_Position(OscMessage message)
        {
            SendSerialNumber();

            Command_ModelName(message);
            Command_SensorInfo(message);
            Command_SensorResolution(message);
            Command_VendorName(message);
        }

        [OscCommand("/sensorInfo", "Usage: [$basePath]/sensorInfo")]
        private void Command_SensorInfo(OscMessage message)
        {
            Connection?.Send(Name + "/sensorInfo", info == null ? "" : info.SensorInfo);
        }

        [OscCommand("/sensorResolution", "Usage: [$basePath]/sensorResolution")]
        private void Command_SensorResolution(OscMessage message)
        {
            Connection?.Send(Name + "/sensorResolution", info == null ? "" : info.SensorResolution);
        }

        [OscCommand("/serialNumber", "Usage: [$basePath]/serialNumber, s")]
        private void Command_SerialNumber(OscMessage message)
        {
            if (message.Count > 0)
            {
                if (message.Count < 1 ||
                    (message[0] is string) == false)
                {
                    Connection?.Send("/usage", OscCommandManager.GetCommandHelpString(typeof(PointGreyCameraSource), Name, "/serialNumber"));

                    return;
                }

                SerialNumber = (string)message[0];
            }

            SendSerialNumber();
        }

        [OscCommand("/vendorName", "Usage: [$basePath]/vendorName")]
        private void Command_VendorName(OscMessage message)
        {
            Connection?.Send(Name + "/vendorName", info == null ? "" : info.VendorName);
        }

        private void SendSerialNumber()
        {
            Connection?.Send(Name + "/serialNumber", SerialNumber);
        }
    }
}
