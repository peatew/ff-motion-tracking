﻿using System.Drawing;
using OpenTK;
using OpenTK.Graphics;

namespace Rug.CV.Data
{
    public class Blob
	{
		public int ID { get; set; }

		public int ExternalID { get; set; } 

		public int LiveFrames { get; set; }

		public int DeadFrames { get; set; }

		public RectangleF Bounds { get; set; }

		public RectangleF DetectionBounds { get; set; }

		public Color4 Color { get; set; }

		public int MergeCount { get; set; }

		public Vector2 Center { get; set; }
		public Vector2 LastCenter { get; set; }
		public Vector3 AvCenter { get; set; }



		//public List<int> Connections = new List<int>();

		//public List<Polygon> Contours = new List<Polygon>();
	}
}

