﻿using System.Collections.Generic;
using Rug.CV.Custom;
using Rug.CV.Data;
using Rug.Host;

namespace Rug.CV
{
    public delegate void BlobSourceEvent(IBlobSource source);

    public interface IBlobSource : IObject
    {
        List<Blob> Blobs { get; }

        event BlobEvent BlobDie;

        event MultiBlobEvent BlobMerge;

        event BlobEvent BlobSpawned;

        event MultiBlobEvent BlobSplit;

        event BlobSourceEvent BlobsProcessed;
    }
}