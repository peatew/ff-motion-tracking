﻿using System;
using System.Drawing;
using Rug.Host;

namespace Rug.CV
{
    public delegate void ImageSourceFrameEvent(IImageSource source, DateTime timestamp, Bitmap bitmap);

    public interface IImageSource : IObject
    {
        event ImageSourceFrameEvent NewFrame;
    }
}