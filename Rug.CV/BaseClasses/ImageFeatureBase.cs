﻿using System.Xml;
using Rug.Host;
using Rug.Host.BaseClasses;
using Rug.Host.Commands;
using Rug.Host.Loading;

namespace Rug.CV.BaseClasses
{
    public abstract class ImageFeatureBase : ObjectBase
    {
        [OscMember]
        public ObjectBinding<IImageSource> Source { get; private set; } 

        public ImageFeatureBase()
        {
            Source = new ObjectBinding<IImageSource>(this, nameof(Source), "/" + nameof(Source).ToLower());

            Source.Bound += ImageSource_Bound;
            Source.Unbound += ImageSource_Unbound;
        }

        public override void Bind(Workspace workspace)
        {
            Source.Bind(workspace);
        }

        public override void Unbind()
        {
            Source.Unbind();
        }

        public override void Load(LoadContext context, XmlNode node)
        {
            base.Load(context, node);
        }

        public override XmlElement Save(LoadContext context, XmlElement element)
        {
            base.Save(context, element);

            return element;
        }

        protected abstract void OnNewSourceFrame(IImageSource source, System.DateTime timestamp, System.Drawing.Bitmap bitmap);

        private void ImageSource_Bound(IOscObjectBinding obj)
        {
            Source.BoundTo.NewFrame += OnNewSourceFrame;
        }

        private void ImageSource_Unbound(IOscObjectBinding obj)
        {
            if (Source.IsBound == false)
            {
                return;
            }

            Source.BoundTo.NewFrame -= OnNewSourceFrame;
        }
    }
}