﻿using Rug.Host.BaseClasses;

namespace Rug.CV.BaseClasses
{
    public abstract class ImageSourceBase : ObjectBase, IImageSource
    {
        public abstract event ImageSourceFrameEvent NewFrame;      
    }
}