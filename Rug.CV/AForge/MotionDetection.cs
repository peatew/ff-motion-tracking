﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Xml;
using AForge.Vision.Motion;
using Rug.CV.BaseClasses;
using Rug.Host;
using Rug.Host.Commands;
using Rug.Host.Loading;

namespace Rug.CV.AForgeCV
{
    [XmlName("AForge.MotionDetection")]
    public class MotionDetection : ImageFeatureBase, IDebugImageSource
    {
        Bitmap debugBitmap; 

        public readonly float[,] MotionData;
        private int detectedObjectsCount;
        private MotionDetector detector;

        private double totalMotionLevel;
        private double totalMotionLevelAtPeriodStart;

        private long totalFrameCount;
        private long totalFrameCountAtPeriodStart;

        public readonly int GridSize = 64;

        private float minThreshold = 0.01f;

        public bool DebuggerAttached { get; set; }

        [OscMember]
        public OscInt FramesPerBackgroundUpdate { get; private set; }

        [OscMember]
        public OscInt MillisecondsPerBackgroundUpdate { get; private set; }

        public event DebugFrameEvent NewDebugFrame;

        public MotionDetection()
        {
            FramesPerBackgroundUpdate = new OscInt(this, nameof(FramesPerBackgroundUpdate), "/" + nameof(FramesPerBackgroundUpdate).ToLower(), 20);
            MillisecondsPerBackgroundUpdate = new OscInt(this, nameof(MillisecondsPerBackgroundUpdate), "/" + nameof(MillisecondsPerBackgroundUpdate).ToLower(), 1000);
            
            detector = new MotionDetector(new SimpleBackgroundModelingDetector(true, true), new GridMotionAreaProcessing(GridSize, GridSize));

            MotionData = new float[GridSize, GridSize];

            debugBitmap = new Bitmap(GridSize, GridSize, PixelFormat.Format32bppArgb); 
        }

        public override void Unbind()
        {
            detector?.Reset();
            debugBitmap.Dispose();

            base.Unbind();
        }

        //public void HighFrequencyPulseOsc(IOscConnection connection, double delta)
        //{
        //    double num = totalMotionLevel - totalMotionLevelAtPeriodStart;
        //    totalMotionLevelAtPeriodStart = totalMotionLevel;

        //    if (float.IsNaN((float)(num / delta)) == true)
        //    {
        //        return; 
        //    }

        //    connection.Send(Name + "/motion", (float)(num / delta));
        //    //connection.Send(new OscMessage(Name + "/objects", (float)detectedObjectsCount));
        //}

        public override void Load(LoadContext context, XmlNode node)
        {
            base.Load(context, node);
        }

        //public void LowFrequencyPulseOsc(IOscConnection connection, double delta)
        //{
        //    double num = totalFrameCount - totalFrameCountAtPeriodStart;
        //    totalFrameCountAtPeriodStart = totalFrameCount;

        //    if (float.IsNaN((float)(num / delta)) == true)
        //    {
        //        return;
        //    }

        //    connection.Send(Name + "/fps", (float)(num / delta));
        //}

        public override XmlElement Save(LoadContext context, XmlElement element)
        {
            base.Save(context, element);

            return element;
        }

        protected override void OnNewSourceFrame(IImageSource source, DateTime timestamp, Bitmap bitmap)
        {
            ThreadName = System.Threading.Thread.CurrentThread.Name;

            lock (this)
            {
                if (detector == null)
                {
                    return;
                }

                GridMotionAreaProcessing grid = (detector.MotionProcessingAlgorithm as GridMotionAreaProcessing);
                grid.HighlightMotionGrid = false;

                SimpleBackgroundModelingDetector background = (detector.MotionDetectionAlgorithm as SimpleBackgroundModelingDetector);

                // expose these 
                background.FramesPerBackgroundUpdate = FramesPerBackgroundUpdate.Value;
                background.MillisecondsPerBackgroundUpdate = MillisecondsPerBackgroundUpdate.Value;

                totalFrameCount++;
                totalMotionLevel += detector.ProcessFrame(bitmap);

                float[,] gridData = grid.MotionGrid;

                for (int i = 0; i < GridSize; i++)
                {
                    for (int j = 0; j < GridSize; j++)
                    {
                        float value = gridData[i, j];

                        MotionData[i, j] = value;

                        if (value < minThreshold)
                        {
                            debugBitmap.SetPixel(i, j, Color.Black);
                        }
                        else
                        {
                            byte intValue = (byte)Math.Min(255, (int)(value * 256f));

                            debugBitmap.SetPixel(j, i, Color.FromArgb(255, 0, intValue, 255 - intValue));
                        }
                    }
                }                

                // check objects' count
                if (detector.MotionProcessingAlgorithm is BlobCountingObjectsProcessing)
                {
                    BlobCountingObjectsProcessing countingDetector = (BlobCountingObjectsProcessing)detector.MotionProcessingAlgorithm;

                    detectedObjectsCount = countingDetector.ObjectsCount;
                }
                else
                {
                    detectedObjectsCount = 0;
                }

                NewDebugFrame?.Invoke(this, debugBitmap);
            }
        }
    }
}