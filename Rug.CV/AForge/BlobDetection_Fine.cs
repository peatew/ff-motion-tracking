﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Xml;
using AForge;
using AForge.Imaging;
using AForge.Imaging.Filters;
using AForge.Math.Geometry;
using AForge.Vision.Motion;
using OpenTK;
using Rug.CV.BaseClasses;
using Rug.Host;
using Rug.Host.Loading;

namespace Rug.CV.AForgeCV
{
    public class BlobShape
    {
        public Vector2[] Points;
        public RectangleF Bounds;

        public BlobShape CreateTransformed(ITransfromSource transformSource)
        {
            BlobShape newShape = new BlobShape() { Bounds = Bounds, Points = (Vector2[])Points.Clone() };

            if (transformSource == null)
            {
                return newShape; 
            }

            Matrix4 matrix = transformSource.Matrix;

            Vector2 min = new Vector2(float.MaxValue, float.MaxValue);
            Vector2 max = new Vector2(float.MinValue, float.MinValue);

            for (int i = 0; i < newShape.Points.Length; i++)
            {
                Vector3 point3 = new Vector3(newShape.Points[i]);

                Vector3.Transform(ref point3, ref matrix, out point3);

                Vector2 point2 = point3.Xy;

                min = Vector2.ComponentMin(min, point2);
                max = Vector2.ComponentMax(max, point2);

                newShape.Points[i] = point2; 
            }

            newShape.Bounds = RectangleF.FromLTRB(min.X, min.Y, max.X, max.Y);

            return newShape;

            //Vector3 boundsMin = new Vector3(newShape.Bounds.X, newShape.Bounds.Y, 0);
            //Vector3 boundsMax = new Vector3(newShape.Bounds.X + newShape.Bounds.Width, newShape.Bounds.Y + newShape.Bounds.Height, 0);

            //Vector3.Transform(ref boundsMin, ref matrix, out boundsMin);
            //Vector3.Transform(ref boundsMax, ref matrix, out boundsMax);

        }
    }

    [XmlName("AForge.BlobDetection.Fine")]
    public class BlobDetection_Fine : ImageFeatureBase, IDebugImageSource, IListSource<BlobShape>
    {
        public readonly float[,] MotionData;
        private int detectedObjectsCount;
        private MotionDetector detector;

        private double totalMotionLevel;
        //private double totalMotionLevelAtPeriodStart;

        private long totalFrameCount;
        //private long totalFrameCountAtPeriodStart;

        public readonly int GridSize = 64;

        //private float minThreshold = 0.01f;        

        public bool DebuggerAttached { get; set; }

        // create convex hull searching algorithm
        GrahamConvexHull hullFinder = new GrahamConvexHull();

        public List<BlobShape> List { get; private set; } = new List<BlobShape>(); 

        public event DebugFrameEvent NewDebugFrame;

        public event EventHandler ListUpdated;

        public BlobDetection_Fine()
        {
            detector = new MotionDetector(new SimpleBackgroundModelingDetector(true, false), new BlobCountingObjectsProcessing_Custom(GridSize, GridSize));

            MotionData = new float[GridSize, GridSize];
        }

        public override void Bind(Workspace workspace)
        {
            base.Bind(workspace);
        }

        public override void Unbind()
        {
            base.Unbind();

            detector?.Reset();
        }

        public override void Load(LoadContext context, XmlNode node)
        {
            base.Load(context, node);
        }

        public override XmlElement Save(LoadContext context, XmlElement element)
        {
            base.Save(context, element);

            return element;
        }

        protected override void OnNewSourceFrame(IImageSource source, DateTime timestamp, Bitmap orginal)
        {
            ThreadName = System.Threading.Thread.CurrentThread.Name; 

            lock (this)
            {
                if (detector == null)
                {
                    return;
                }

                ResizeNearestNeighbor filter = new ResizeNearestNeighbor(orginal.Width / 2, orginal.Height / 2);

                using (Bitmap bitmap = filter.Apply(orginal))
                {
                    BlobCountingObjectsProcessing_Custom blobs = (detector.MotionProcessingAlgorithm as BlobCountingObjectsProcessing_Custom);
                    blobs.HighlightMotionRegions = false;

                    SimpleBackgroundModelingDetector background = (detector.MotionDetectionAlgorithm as SimpleBackgroundModelingDetector);

                    // expose these 
                    background.FramesPerBackgroundUpdate = 20;
                    background.MillisecondsPerBackgroundUpdate = 1000;

                    totalFrameCount++;
                    totalMotionLevel += detector.ProcessFrame(bitmap);

                    //List<RectangleF> blobRegions = new List<RectangleF>();
                    List.Clear();

                    float scaleX = 2f / bitmap.Width;
                    float scaleY = 2f / bitmap.Height;

                    BlobCounter blobCounter = blobs.BlobCounter;

                    AForge.Imaging.Blob[] blobArray = blobCounter.GetObjectsInformation();

                    List<Vector2> shape = new List<Vector2>();

                    for (int i = 0; i < blobArray.Length; i++)
                    {
                        shape.Clear();

                        AForge.Imaging.Blob blob = blobArray[i];

                        List<IntPoint> leftPoints, rightPoints, edgePoints;

                        edgePoints = new List<IntPoint>();

                        // get blob's edge points
                        //blobCounter.GetBlobsLeftAndRightEdges(blob, out leftPoints, out rightPoints);
                        blobCounter.GetBlobsTopAndBottomEdges(blob, out leftPoints, out rightPoints);

                        edgePoints.AddRange(leftPoints);
                        edgePoints.AddRange(rightPoints);

                        // blob's convex hull
                        List<IntPoint> hull = hullFinder.FindHull(edgePoints);

                        foreach (IntPoint point in hull)
                        {
                            shape.Add(new Vector2((float)point.X * scaleX - 1f, (float)point.Y * scaleY - 1f));
                        }

                        Rectangle rect = blob.Rectangle;

                        List.Add(new BlobShape() { Points = shape.ToArray(), Bounds = new RectangleF(rect.X * scaleX - 1f, rect.Y * scaleY - 1f, rect.Width * scaleX, rect.Height * scaleY) });
                    }

                    ListUpdated?.Invoke(this, EventArgs.Empty);

                    // check objects' count
                    if (detector.MotionProcessingAlgorithm is BlobCountingObjectsProcessing)
                    {
                        BlobCountingObjectsProcessing countingDetector = (BlobCountingObjectsProcessing)detector.MotionProcessingAlgorithm;

                        detectedObjectsCount = countingDetector.ObjectsCount;
                    }
                    else
                    {
                        detectedObjectsCount = 0;
                    }
                }
            }
        }
    }    
}
