﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Xml;
using AForge.Imaging;
using AForge.Vision.Motion;
using Rug.CV.BaseClasses;
using Rug.Host;
using Rug.Host.Loading;

namespace Rug.CV.AForgeCV
{
    [XmlName("AForge.BlobDetection")]
    public class BlobDetection : ImageFeatureBase, IDebugImageSource, IListSource<RectangleF>
    {
        public readonly float[,] MotionData;
        private int detectedObjectsCount;
        private MotionDetector detector;

        private double totalMotionLevel;
        private double totalMotionLevelAtPeriodStart;

        private long totalFrameCount;
        private long totalFrameCountAtPeriodStart;

        public readonly int GridSize = 64;

        private float minThreshold = 0.01f;

        public bool DebuggerAttached { get; set; }

        public List<RectangleF> List { get; private set; } = new List<RectangleF>(); 

        public event DebugFrameEvent NewDebugFrame;

        public event EventHandler ListUpdated;

        public BlobDetection()
        {
            detector = new MotionDetector(new SimpleBackgroundModelingDetector(true, false), new BlobCountingObjectsProcessing_Custom(GridSize, GridSize));

            MotionData = new float[GridSize, GridSize];
        }

        public override void Bind(Workspace workspace)
        {
            base.Bind(workspace);
        }

        public override void Unbind()
        {
            base.Unbind();

            detector?.Reset();
        }

        public override void Load(LoadContext context, XmlNode node)
        {
            base.Load(context, node);
        }
       
        public override XmlElement Save(LoadContext context, XmlElement element)
        {
            base.Save(context, element);

            return element;
        }

        protected override void OnNewSourceFrame(IImageSource source, DateTime timestamp, Bitmap bitmap)
        {
            ThreadName = System.Threading.Thread.CurrentThread.Name;

            lock (this)
            {
                if (detector == null)
                {
                    return;
                }

                BlobCountingObjectsProcessing_Custom blobs = (detector.MotionProcessingAlgorithm as BlobCountingObjectsProcessing_Custom);
                blobs.HighlightMotionRegions = false;

                SimpleBackgroundModelingDetector background = (detector.MotionDetectionAlgorithm as SimpleBackgroundModelingDetector);

                // expose these 
                background.FramesPerBackgroundUpdate = 20;
                background.MillisecondsPerBackgroundUpdate = 1000;

                totalFrameCount++;
                totalMotionLevel += detector.ProcessFrame(bitmap);

                //List<RectangleF> blobRegions = new List<RectangleF>();
                List.Clear();

                float scaleX = 2f / bitmap.Width;
                float scaleY = 2f / bitmap.Height;

                //BlobCounter blobCounter = blobs.BlobCounter;

                //AForge.Imaging.Blob[] blobArray = blobCounter.GetObjectsInformation();

                //for (int i = 0; i < blobArray.Length; i++)
                //{
                    
                //}

                foreach (Rectangle rect in blobs.ObjectRectangles)
                {                    
                    List.Add(new RectangleF(rect.X * scaleX - 1f, rect.Y * scaleY - 1f, rect.Width * scaleX, rect.Height * scaleY));
                }

                ListUpdated?.Invoke(this, EventArgs.Empty);

                // check objects' count
                if (detector.MotionProcessingAlgorithm is BlobCountingObjectsProcessing)
                {
                    BlobCountingObjectsProcessing countingDetector = (BlobCountingObjectsProcessing)detector.MotionProcessingAlgorithm;

                    detectedObjectsCount = countingDetector.ObjectsCount;
                }
                else
                {
                    detectedObjectsCount = 0;
                }
            }
        }
    }

    public class BlobCountingObjectsProcessing_Custom : IMotionProcessing
    {
        private BlobCounter blobCounter;
        private Color highlightColor;
        private bool highlightMotionRegions;

        public BlobCounter BlobCounter { get { return blobCounter; } } 

        public BlobCountingObjectsProcessing_Custom() : this(10, 10)
        {
        }

        public BlobCountingObjectsProcessing_Custom(bool highlightMotionRegions) : this(10, 10, highlightMotionRegions)
        {
        }

        public BlobCountingObjectsProcessing_Custom(int minWidth, int minHeight) : this(minWidth, minHeight, Color.Red)
        {
        }

        public BlobCountingObjectsProcessing_Custom(int minWidth, int minHeight, bool highlightMotionRegions) : this(minWidth, minHeight)
        {
            this.highlightMotionRegions = highlightMotionRegions;
        }

        public BlobCountingObjectsProcessing_Custom(int minWidth, int minHeight, Color highlightColor)
        {
            this.blobCounter = new BlobCounter();
            this.highlightColor = Color.Red;
            this.highlightMotionRegions = true;
            this.blobCounter.FilterBlobs = true;
            this.blobCounter.MinHeight = minHeight;
            this.blobCounter.MinWidth = minWidth;
            this.highlightColor = highlightColor;
        }

        public void ProcessFrame(UnmanagedImage videoFrame, UnmanagedImage motionFrame)
        {
            if (motionFrame.PixelFormat != PixelFormat.Format8bppIndexed)
            {
                throw new InvalidImagePropertiesException("Motion frame must be 8 bpp image.");
            }
            if (((videoFrame.PixelFormat != PixelFormat.Format8bppIndexed) && (videoFrame.PixelFormat != PixelFormat.Format24bppRgb)) && ((videoFrame.PixelFormat != PixelFormat.Format32bppRgb) && (videoFrame.PixelFormat != PixelFormat.Format32bppArgb)))
            {
                throw new UnsupportedImageFormatException("Video frame must be 8 bpp grayscale image or 24/32 bpp color image.");
            }
            int width = videoFrame.Width;
            int height = videoFrame.Height;
            if ((motionFrame.Width == width) && (motionFrame.Height == height))
            {
                lock (this.blobCounter)
                {
                    this.blobCounter.ProcessImage(motionFrame);
                }
                if (this.highlightMotionRegions)
                {
                    foreach (Rectangle rectangle in this.blobCounter.GetObjectsRectangles())
                    {
                        Drawing.Rectangle(videoFrame, rectangle, this.highlightColor);
                    }
                }
            }
        }

        public void Reset()
        {
        }

        public Color HighlightColor
        {
            get
            {
                return this.highlightColor;
            }
            set
            {
                this.highlightColor = value;
            }
        }

        public bool HighlightMotionRegions
        {
            get
            {
                return this.highlightMotionRegions;
            }
            set
            {
                this.highlightMotionRegions = value;
            }
        }

        public int MinObjectsHeight
        {
            get
            {
                return this.blobCounter.MinHeight;
            }
            set
            {
                lock (this.blobCounter)
                {
                    this.blobCounter.MinHeight = value;
                }
            }
        }

        public int MinObjectsWidth
        {
            get
            {
                return this.blobCounter.MinWidth;
            }
            set
            {
                lock (this.blobCounter)
                {
                    this.blobCounter.MinWidth = value;
                }
            }
        }

        public Rectangle[] ObjectRectangles
        {
            get
            {
                lock (this.blobCounter)
                {
                    return this.blobCounter.GetObjectsRectangles();
                }
            }
        }

        public int ObjectsCount
        {
            get
            {
                lock (this.blobCounter)
                {
                    return this.blobCounter.ObjectsCount;
                }
            }
        }
    }
}
