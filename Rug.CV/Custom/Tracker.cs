﻿using System;
using System.Collections.Generic;
using System.Drawing;

//using AForge.Imaging;
using OpenTK;
using OpenTK.Graphics;
using Rug.CV.Data;

namespace Rug.CV.Custom
{
    public delegate void BlobEvent(Tracker sender, Blob blob);

    public delegate void MultiBlobEvent(Tracker sender, Blob blobA, Blob blobB);

    public class Tracker
    {
        public readonly Dictionary<int, Blob> BlobLookup = new Dictionary<int, Blob>();

        public readonly List<Blob> Blobs = new List<Blob>();

        private readonly object syncLock = new object();

        private static Color4[] Colors = new Color4[]
        {
            Color4.Black,
            Color4.White,
            Color4.Blue,
            Color4.Yellow,
            Color4.Turquoise,
            Color4.Cyan,
            Color4.Orange,
            Color4.Magenta,
        };

        private int BlobID = 0;

        public int BlobDieFrames { get; set; }

        public float BlobInflateSize { get; set; }

        public float BlobSpawnDistance { get; set; }

        public event BlobEvent BlobDie;

        public event MultiBlobEvent BlobMerge;

        public event BlobEvent BlobSpawned;

        public event MultiBlobEvent BlobSplit;

        public Tracker()
        {
        }

        public void Process(List<RectangleF> initialBlobs)
        {
            List<RectangleF> posibleBlobs = new List<RectangleF>();

            foreach (RectangleF rect in initialBlobs)
            {
                RectangleF rect2 = rect;
                // step 1 inflate all blob rects
                Inflate(ref rect2);
                posibleBlobs.Add(rect2);
            }

            List<Blob> toRemove = new List<Blob>();

            foreach (Blob blob in Blobs)
            {
                RectangleF oldBounds = blob.Bounds;

                RectangleF newBounds = blob.Bounds;

                Inflate(ref newBounds);

                blob.DetectionBounds = newBounds;

                blob.Bounds = RectangleF.Empty;

                for (int i = 0; i < posibleBlobs.Count; i++)
                {
                    RectangleF rect = posibleBlobs[i];

                    if (blob.DetectionBounds.IntersectsWith(rect) == true)
                    {
                        blob.DeadFrames = 0;

                        if (blob.Bounds == RectangleF.Empty)
                        {
                            blob.Bounds = rect;
                        }
                        else
                        {
                            float left, right, top, bottom;

                            left = Math.Min(blob.Bounds.Left, rect.Left);
                            right = Math.Max(blob.Bounds.Right, rect.Right);
                            top = Math.Min(blob.Bounds.Top, rect.Top);
                            bottom = Math.Max(blob.Bounds.Bottom, rect.Bottom);

                            blob.Bounds = new RectangleF(left, top, right - left, bottom - top);
                        }
                    }
                }

                if (blob.Bounds == RectangleF.Empty)
                {
                    blob.Bounds = oldBounds;
                    blob.DeadFrames++;
                    blob.LiveFrames--;

                    blob.LiveFrames = Math.Max(blob.LiveFrames, 0);
                }
                else
                {
                    blob.LiveFrames++;
                }

                if (blob.DeadFrames >= BlobDieFrames)
                {
                    OnBlobDie(blob);
                    toRemove.Add(blob);
                }

                blob.LastCenter = blob.Center;
                blob.Center = GetCenter(blob.Bounds);
            }

            foreach (Blob blob in Blobs)
            {
                if (toRemove.Contains(blob) == true)
                {
                    continue;
                }

                foreach (Blob other in Blobs)
                {
                    if (blob == other)
                    {
                        continue;
                    }

                    if (toRemove.Contains(other) == true)
                    {
                        continue;
                    }

                    if (blob.Bounds.Contains(other.Bounds) == true)
                    {
                        if (blob.LiveFrames > other.LiveFrames)
                        {
                            blob.MergeCount += other.MergeCount + 1;
                            //blob.Color = Colors[blob.Infected ? 1 : 0];

                            OnBlobMerge(blob, other);

                            toRemove.Add(other);
                        }
                        else
                        {
                            other.MergeCount += blob.MergeCount + 1;

                            OnBlobMerge(other, blob);

                            toRemove.Add(blob);
                        }
                    }
                }
            }

            foreach (RectangleF rect in posibleBlobs)
            {
                bool intersected = false;

                foreach (Blob blob in Blobs)
                {
                    if (rect.IntersectsWith(blob.Bounds) == true)
                    {
                        intersected = true;
                    }
                }

                if (intersected == false)
                {
                    Vector2 center = GetCenter(rect);

                    bool infected = false;
                    bool split = false;

                    float closest = float.MaxValue;
                    Blob closetBlob = null;

                    int connectionID = -1;

                    foreach (Blob blob in Blobs)
                    {
                        float dist = (blob.Center - center).Length;

                        if (dist < BlobSpawnDistance)
                        {
                            if (dist < closest)
                            {
                                if (blob.MergeCount > 0)
                                {
                                    closetBlob = blob;
                                    closest = dist;
                                }
                            }
                        }
                    }

                    if (closetBlob != null)
                    {
                        closetBlob.MergeCount--;
                        connectionID = closetBlob.ID;

                        split = true;
                    }

                    Blob newBlob = new Blob()
                    {
                        Color = Colors[infected ? 1 : 0], // [BlobID % Colors.Length],
                        ID = BlobID++,
                        Bounds = rect,
                        Center = center,
                        LastCenter = center,
                        AvCenter = Vector3.Zero,
                    };

                    OnBlobSplit(closetBlob, newBlob);

                    Blobs.Add(newBlob);
                }
            }

            foreach (Blob blob in toRemove)
            {
                Blobs.Remove(blob);
            }

            BlobLookup.Clear();

            foreach (Blob blob in Blobs)
            {
                BlobLookup.Add(blob.ID, blob);
            }
        }

        private Vector2 GetCenter(RectangleF rect)
        {
            return new Vector2(rect.X + (rect.Width * 0.5f), rect.Y + (rect.Height * 0.5f));
        }

        private void Inflate(ref RectangleF rect)
        {
            float centerX, centerY;

            centerX = rect.X + (rect.Width * 0.5f);
            centerY = rect.Y + (rect.Height * 0.5f);

            float sizeX, sizeY;

            sizeX = rect.Width * BlobInflateSize;
            sizeY = rect.Height * BlobInflateSize;

            rect = new RectangleF(centerX - (sizeX * 0.5f), centerY - (sizeY * 0.5f), sizeX, sizeY);
        }

        private void OnBlobDie(Blob blob)
        {
            BlobDie?.Invoke(this, blob);
        }

        private void OnBlobMerge(Blob blobA, Blob blobB)
        {
            BlobMerge?.Invoke(this, blobA, blobB);
        }

        private void OnBlobSpawned(Blob blob)
        {
            BlobSpawned?.Invoke(this, blob);
        }

        private void OnBlobSplit(Blob blobA, Blob blobB)
        {
            BlobSplit?.Invoke(this, blobA, blobB);
        }
    }
}