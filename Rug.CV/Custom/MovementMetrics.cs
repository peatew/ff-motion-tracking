﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Xml;
using OpenTK;
using Rug.CV.AForgeCV;
using Rug.Host;
using Rug.Host.BaseClasses;
using Rug.Host.Commands;
using Rug.Host.Loading;
using Rug.Osc;

namespace Rug.CV.HighLevelFeatures
{
    [XmlName("Custom.MovementMetrics")]
    public class MovementMetrics : ObjectBase
    {
        [OscMember]
        public ObjectBinding<IPulse> Pulse { get; private set; }

        [OscMember]
        public OscObjectList<DataSource> DataSources { get; private set; }

        public MovementMetrics()
        {
            Pulse = new ObjectBinding<IPulse>(this, nameof(Pulse), "/" + nameof(Pulse).ToLower());

            Pulse.Bound += Pulse_Bound;
            Pulse.Unbound += Pulse_Unbound;

            DataSources = new OscObjectList<DataSource>(this, nameof(DataSources), "/" + nameof(DataSources).ToLower());           
        }

        public override void Bind(Workspace workspace)
        {
            Pulse.Bind(workspace);

            DataSources.Bind(workspace);
        }

        public override void Unbind()
        {
            Pulse.Unbind();

            DataSources.Unbind();
        }

        private void OnPulse(object sender, EventArgs e)
        {
            ThreadName = System.Threading.Thread.CurrentThread.Name;

            int count = 0; 
            double sum = 0;
            double min = double.MaxValue;
            double max = double.MinValue;

            //Vector2 center = Vector2.Zero;
            //double weightedTotal = 0; 

            foreach (DataSource source in DataSources)
            {
                if (source.Source.IsBound == false)
                {
                    continue;
                }

                source.SetThreadName(); 

                int gridSize = source.Source.BoundTo.GridSize;
                float[,] data = source.Source.BoundTo.MotionData;

                float sizeFactor = 2f / (float)gridSize;

                Matrix4 transform = source.Transform.IsBound ? source.Transform.BoundTo.Matrix : Matrix4.Identity;  

                for (int i = 0; i < gridSize; i++)
                {
                    for (int j = 0; j < gridSize; j++)
                    {
                        float value = data[i, j];

                        sum += value;
                        min = Math.Min(min, value);
                        max = Math.Max(max, value);

                        count++;  
                    }
                }
            }

            if (count == 0)
            {
                return; 
            }

            double mean = sum / (double)count;

            Connection?.Send(Name + "/sum", (float)sum);
            Connection?.Send(Name + "/min", MathHelper.Clamp((float)min, 0, 1));
            Connection?.Send(Name + "/max", MathHelper.Clamp((float)max, 0, 1));
            Connection?.Send(Name + "/mean", MathHelper.Clamp((float)mean, 0, 1));
        }

        private void Pulse_Bound(IOscObjectBinding obj)
        {
            Pulse.BoundTo.Pulse += OnPulse;
        }

        private void Pulse_Unbound(IOscObjectBinding obj)
        {
            if (Pulse.IsBound == false)
            {
                return;
            }

            Pulse.BoundTo.Pulse -= OnPulse;
        }

        [XmlName("MovementMetrics.DataSource")]
        public class DataSource : ObjectBase, ILoadable
        {
            [OscMember]
            public ObjectBinding<MotionDetection> Source { get; private set; }

            [OscMember]
            public ObjectBinding<ITransfromSource> Transform { get; private set; }

            public DataSource()
            {
                Source = new ObjectBinding<MotionDetection>(this, nameof(Source), "/" + nameof(Source).ToLower());
                Transform = new ObjectBinding<ITransfromSource>(this, nameof(Transform), "/" + nameof(Transform).ToLower(), false);
            }

            public override void Bind(Workspace workspace)
            {
                Source.Bind(workspace);
                Transform.Bind(workspace);
            }

            public override void Unbind()
            {
                Source.Unbind();
                Transform.Unbind();
            }

            internal void SetThreadName()
            {
                ThreadName = Source.BoundTo.ThreadName; 
            }
        }        
    }
}