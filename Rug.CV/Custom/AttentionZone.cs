﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Xml;
using OpenTK;
using Rug.CV.AForgeCV;
using Rug.Host;
using Rug.Host.BaseClasses;
using Rug.Host.Commands;
using Rug.Host.Loading;
using Rug.Osc;

namespace Rug.CV.HighLevelFeatures
{
    [XmlName("Custom.AttentionZone")]
    public class AttentionZone : ObjectBase
    {
        readonly List<Vector3> points = new List<Vector3>(); 

        [OscMember]
        public ObjectBinding<IPulse> Pulse { get; private set; }

        [OscMember]
        public OscObjectList<DataSource> DataSources { get; private set; }

        [OscMember]
        public OscObjectList<Zone> Zones { get; private set; }

        public AttentionZone()
        {
            Pulse = new ObjectBinding<IPulse>(this, nameof(Pulse), "/" + nameof(Pulse).ToLower());

            Pulse.Bound += Pulse_Bound;
            Pulse.Unbound += Pulse_Unbound;

            DataSources = new OscObjectList<DataSource>(this, nameof(DataSources), "/" + nameof(DataSources).ToLower());

            Zones = new OscObjectList<Zone>(this, nameof(Zones), "/" + nameof(Zones).ToLower());
        }

        public override void Bind(Workspace workspace)
        {
            Pulse.Bind(workspace);

            DataSources.Bind(workspace);

            Zones.Bind(workspace);
        }

        public override void Unbind()
        {
            Pulse.Unbind();

            DataSources.Unbind();

            Zones.Unbind();
        }

        private void OnPulse(object sender, EventArgs e)
        {
            ThreadName = System.Threading.Thread.CurrentThread.Name; 

            points.Clear(); 

            foreach (DataSource source in DataSources)
            {
                if (source.Source.IsBound == false)
                {
                    continue;
                }

                source.SetThreadName();

                int gridSize = source.Source.BoundTo.GridSize;
                float[,] data = source.Source.BoundTo.MotionData;

                float sizeFactor = 2f / (float)gridSize;

                Matrix4 transform = source.Transform.IsBound ? source.Transform.BoundTo.Matrix : Matrix4.Identity;  

                for (int i = 0; i < gridSize; i++)
                {
                    for (int j = 0; j < gridSize; j++)
                    {
                        Vector3 position = new Vector3(-1f + (((float)i * sizeFactor)), -1f + (((float)j * sizeFactor)), 0);

                        Vector3.Transform(ref position, ref transform, out position); 

                        position.Z = MathHelper.Clamp(data[i, j], 0f, 1f);

                        points.Add(position); 
                    }
                }
            }

            float delta = Pulse.BoundTo.DeltaTime; 

            foreach (Zone zone in Zones)
            {
                zone.OnPulse(points, delta); 
            }
        }

        [OscCommand("/reset", "Usage: [$basePath]/reset")]
        private void Command_Reset(OscMessage message)
        {
            foreach (Zone zone in Zones)
            {
                zone.Reset(); 
            } 

            Connection?.Send(message.Address);
        }

        //[OscCommand("/radius", "Usage: [$basePath]/radius, f")]
        //private void Command_Radius(OscMessage message)
        //{
        //    if (message.Count > 0)
        //    {
        //        if (message.Count < 1 ||
        //            (message[0] is float) == false)
        //        {
        //            Connection?.Send("/usage", OscCommandManager.GetCommandHelpString(typeof(AttentionZone), Name, "/radius"));

        //            return;
        //        }

        //        Position = new PointF((float)message[0], (float)message[1]);
        //    }

        //    Connection?.Send(message.Address, Position.X, Position.Y);
        //}

        private void Pulse_Bound(IOscObjectBinding obj)
        {
            Pulse.BoundTo.Pulse += OnPulse;
        }

        private void Pulse_Unbound(IOscObjectBinding obj)
        {
            if (Pulse.IsBound == false)
            {
                return;
            }

            Pulse.BoundTo.Pulse -= OnPulse;
        }

        [XmlName("AttentionZone.DataSource")]
        public class DataSource : ObjectBase, ILoadable
        {
            [OscMember]
            public ObjectBinding<MotionDetection> Source { get; private set; }

            [OscMember]
            public ObjectBinding<ITransfromSource> Transform { get; private set; }

            public DataSource()
            {
                Source = new ObjectBinding<MotionDetection>(this, nameof(Source), "/" + nameof(Source).ToLower());
                Transform = new ObjectBinding<ITransfromSource>(this, nameof(Transform), "/" + nameof(Transform).ToLower(), false);
            }

            public override void Bind(Workspace workspace)
            {
                Source.Bind(workspace);
                Transform.Bind(workspace);
            }
            
            public override void Unbind()
            {
                Source.Unbind();
                Transform.Unbind();
            }

            internal void SetThreadName()
            {
                ThreadName = Source.BoundTo.ThreadName;  
            }
        }

        [XmlName("AttentionZone.Zone")]
        public class Zone : ObjectBase, ILoadable
        {
            [OscMember]
            public OscVector2 NeutralPosition { get; private set; }

            [OscMember]
            public OscFloat NeutralAttraction { get; private set; }

            [OscMember]
            public OscVector2 Position { get; private set; }

            [OscMember]
            public OscFloat Radius { get; private set; }

            [OscMember]
            public OscFloat Attraction { get; private set; }

            [OscMember]
            public OscVector2 Velocity { get; private set; }

            [OscMember]
            public OscFloat Dampening { get; private set; }

            public Zone()
            {
                NeutralPosition = new OscVector2(this, nameof(NeutralPosition), "/" + nameof(NeutralPosition).ToLower(), Vector2.Zero);
                NeutralAttraction = new OscFloat(this, nameof(NeutralAttraction), "/" + nameof(NeutralAttraction).ToLower(), 0.1f);
                Position = new OscVector2(this, nameof(Position), "/" + nameof(Position).ToLower(), Vector2.Zero);
                Radius = new OscFloat(this, nameof(Radius), "/" + nameof(Radius).ToLower(), 0.2f);
                Attraction = new OscFloat(this, nameof(Attraction), "/" + nameof(Attraction).ToLower(), 2f);
                Velocity = new OscVector2(this, nameof(Velocity), "/" + nameof(Velocity).ToLower(), Vector2.Zero);
                Dampening = new OscFloat(this, nameof(Dampening), "/" + nameof(Dampening).ToLower(), 0.2f);
            }

            public override void Bind(Workspace workspace)
            {
                Reset();
            }

            public override void Unbind()
            {
                
            }
                                  
            public void OnPulse(List<Vector3> points, float deltaTime)
            {
                ThreadName = System.Threading.Thread.CurrentThread.Name;

                Vector2 direction = Vector2.Zero;
                Vector2 position = Position.Value;
                float radius = Radius.Value;
                float radiusSquared = radius * radius;
                float radiusReciprocal = 1f / radius; 

                foreach (Vector3 point in points)
                {
                    if (point.X < position.X - radius || point.X > position.X + radius ||
                        point.Y < position.Y - radius || point.Y > position.Y + radius)
                    {
                        continue; 
                    }

                    Vector2 vectorToPoint = point.Xy - position;

                    if (vectorToPoint == Vector2.Zero)
                    {
                        continue; 
                    }

                    float lengthSquared = vectorToPoint.LengthSquared;

                    if (lengthSquared > radiusSquared)
                    {
                        continue; 
                    }

                    float length = vectorToPoint.Length;

                    float scaleFactor = 1f - (radiusReciprocal * length);

                    scaleFactor *= scaleFactor;

                    scaleFactor *= point.Z;

                    Vector2 amount = Vector2.Normalize(vectorToPoint) * scaleFactor;

                    direction += amount; 
                }

                if (position - NeutralPosition.Value != Vector2.Zero)
                {
                    direction += Vector2.Normalize(NeutralPosition.Value - position) * NeutralAttraction.Value;
                }

                Vector2 velocity = Velocity.Value; 

                velocity *= (float)Math.Pow(Dampening.Value, deltaTime);

                velocity += direction * Attraction.Value * deltaTime;

                Position.Value += velocity * deltaTime;
                Velocity.Value = velocity; 

                Position.Send(); 
            }

            [OscCommand("/reset", "Usage: [$basePath]/reset")]
            private void Command_Reset(OscMessage message)
            {
                Reset();
                
                Connection?.Send(message.Address);
            }

            public void Reset()
            {
                Position.Value = NeutralPosition.Value;
                Velocity.Value = Vector2.Zero;
            }
        }
    }
}