﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Xml;
using Rug.CV.AForgeCV;
using Rug.Host;
using Rug.Host.BaseClasses;
using Rug.Host.Commands;
using Rug.Host.Comms;
using Rug.Host.Loading;

namespace Rug.CV.Custom
{
    [XmlName("Custom.Consolidator")]
    public class Consolidator<T> : ObjectBase, IListSource<T>
    {
        [OscMember]
        public OscObjectList<BoundSource> Sources { get; private set; }
        
        public event EventHandler ListUpdated;

        [OscMember] public ObjectBinding<IPulse> Pulse { get; private set; }
         
        public List<T> List { get; private set; } = new List<T>();

        public Consolidator()
        {
            Pulse = new ObjectBinding<IPulse>(this, nameof(Pulse), "/" + nameof(Pulse).ToLower());
            
            Pulse.Bound += Pulse_Bound;
            Pulse.Unbound += Pulse_Unbound;

            Sources = new OscObjectList<BoundSource>(this, nameof(Sources), "/" + nameof(Sources).ToLower(), null, "Source");
        }

        private void Pulse_Bound(IOscObjectBinding obj)
        {
            Pulse.BoundTo.Pulse += OnPulse;
        }

        private void Pulse_Unbound(IOscObjectBinding obj)
        {
            if (Pulse.IsBound == false)
            {
                return; 
            }

            Pulse.BoundTo.Pulse -= OnPulse;
        }

        private void OnPulse(object sender, EventArgs e)
        {
            ThreadName = System.Threading.Thread.CurrentThread.Name;

            List.Clear();

            foreach (BoundSource source in Sources)
            {
                source.AddTo(List);
            }

            ListUpdated?.Invoke(this, EventArgs.Empty); 
        }

        public override void Bind(Workspace workspace)
        {
            Pulse.Bind(workspace);

            Sources.Bind(workspace);
        }

        public override void Unbind()
        {            
            Pulse.Unbind();

            Sources.Unbind();
        }
        
        public class BoundSource : ObjectBase
        {
            object syncLock = new object();
            readonly List<T> list = new List<T>();

            [OscMember] public ObjectBinding<IListSource<T>> Source { get; private set; }
            
            public BoundSource() 
            {
                Source = new ObjectBinding<IListSource<T>>(this, nameof(Source), "/" + nameof(Source).ToLower());

                Source.Bound += Source_Bound;
                Source.Unbound += Source_Unbound;
            }

            private void Source_Unbound(IOscObjectBinding obj)
            {
                if (Source.IsBound == false)
                {
                    return;
                }

                Source.BoundTo.ListUpdated -= OnSourceUpdated;
            }

            private void Source_Bound(IOscObjectBinding obj)
            {
                Source.BoundTo.ListUpdated += OnSourceUpdated;
            }
            
            private void OnSourceUpdated(object sender, EventArgs args)
            {
                lock (syncLock)
                {
                    IListSource<T> source;

                    if (Source.TryGet(out source) == false)
                    {
                        return; 
                    }

                    list.Clear();
                    list.AddRange(source.List); 
                }
            }

            public override void Bind(Workspace workspace)
            {
                Source.Bind(workspace);
            }

            public override void Unbind()
            {
                Source.Unbind();
            } 

            public void AddTo(List<T> list)
            {
                lock (syncLock)
                {
                    list.AddRange(this.list);
                }
            }
        }
    }    
}
