﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Xml;
using Rug.CV.AForgeCV;
using Rug.Host;
using Rug.Host.BaseClasses;
using Rug.Host.Commands;
using Rug.Host.Comms;
using Rug.Host.Loading;

namespace Rug.CV.Custom
{
    [XmlName("Custom.BlobConsolidator")]
    public class BlobConsolidator : ObjectBase, IListSource<BlobShape>
    {
        [OscMember]
        public OscObjectList<BlobSource> BlobSources { get; private set; }
        
        public event EventHandler ListUpdated;

        [OscMember] public ObjectBinding<IPulse> Pulse { get; private set; }

        public List<BlobShape> List { get; private set; } = new List<BlobShape>();

        public BlobConsolidator()
        {
            Pulse = new ObjectBinding<IPulse>(this, nameof(Pulse), "/" + nameof(Pulse).ToLower());
            
            Pulse.Bound += Pulse_Bound;
            Pulse.Unbound += Pulse_Unbound;

            BlobSources = new OscObjectList<BlobSource>(this, nameof(BlobSources), "/" + nameof(BlobSources).ToLower());
        }

        private void Pulse_Bound(IOscObjectBinding obj)
        {
            Pulse.BoundTo.Pulse += OnPulse;
        }

        private void Pulse_Unbound(IOscObjectBinding obj)
        {
            if (Pulse.IsBound == false)
            {
                return; 
            }

            Pulse.BoundTo.Pulse -= OnPulse;
        }

        private void OnPulse(object sender, EventArgs e)
        {
            ThreadName = System.Threading.Thread.CurrentThread.Name;

            List.Clear();

            foreach (BlobSource source in BlobSources)
            {
                source.AddBlobs(List);
            }

            ListUpdated?.Invoke(this, EventArgs.Empty); 
        }

        public override void Bind(Workspace workspace)
        {
            Pulse.Bind(workspace);

            BlobSources.Bind(workspace);
        }

        public override void Unbind()
        {            
            Pulse.Unbind();

            BlobSources.Unbind();
        }

        [XmlName("BlobConsolidator.BlobSource")]
        public class BlobSource : ObjectBase
        {
            object syncLock = new object();
            readonly List<BlobShape> list = new List<BlobShape>();

            [OscMember] public ObjectBinding<IListSource<BlobShape>> Source { get; private set; }
            [OscMember] public ObjectBinding<ITransfromSource> Transform { get; private set; }

            public BlobSource() 
            {
                Source = new ObjectBinding<IListSource<BlobShape>>(this, nameof(Source), "/" + nameof(Source).ToLower());

                Source.Bound += Source_Bound;
                Source.Unbound += Source_Unbound;

                Transform = new ObjectBinding<ITransfromSource>(this, nameof(Transform), "/" + nameof(Transform).ToLower(), false);
            }

            private void Source_Unbound(IOscObjectBinding obj)
            {
                if (Source.IsBound == false)
                {
                    return;
                }

                Source.BoundTo.ListUpdated -= OnBlobsProcessed;
            }

            private void Source_Bound(IOscObjectBinding obj)
            {
                Source.BoundTo.ListUpdated += OnBlobsProcessed;
            }
            
            private void OnBlobsProcessed(object sender, EventArgs args)
            {
                ThreadName = System.Threading.Thread.CurrentThread.Name;

                lock (syncLock)
                {
                    IListSource<BlobShape> source;

                    if (Source.TryGet(out source) == false)
                    {
                        return; 
                    }

                    list.Clear();
                    foreach (BlobShape blobShape in source.List)
                    {
                        list.Add(blobShape.CreateTransformed(Transform.BoundTo)); 
                    }
                }
            }

            public override void Bind(Workspace workspace)
            {
                Source.Bind(workspace);
                Transform.Bind(workspace); 
            }

            public override void Unbind()
            {
                Source.Unbind();
                Transform.Unbind();
            } 

            public void AddBlobs(List<BlobShape> list)
            {
                lock (syncLock)
                {
                    list.AddRange(this.list);
                }
            }
        }
    }    
}
