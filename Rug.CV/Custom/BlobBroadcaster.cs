﻿using OpenTK;
using Rug.CV.Data;
using Rug.Host;
using Rug.Host.BaseClasses;
using Rug.Host.Commands;
using Rug.Host.Loading;
using Rug.Osc;

namespace Rug.CV.Custom
{
    [XmlName("Custom.Blobs.Broadcaster")]
    public class BlobBroadcaster : ObjectBase
    {
        private Matrix4 transform = Matrix4.Identity;

        [OscMember] public ObjectBinding<IBlobSource> Source { get; private set; }
        [OscMember] public ObjectBinding<ITransfromSource> Transform { get; private set; }

        public BlobBroadcaster()
        {
            Transform = new ObjectBinding<ITransfromSource>(this, nameof(Transform), "/" + nameof(Transform).ToLower(), false);
            Source = new ObjectBinding<IBlobSource>(this, nameof(Source), "/" + nameof(Source).ToLower());
            
            Transform.Bound += Transform_Bound;
            Transform.Unbound += Transform_Unbound;

            Source.Bound += Source_Bound;
            Source.Unbound += Source_Unbound;
        }

        public override void Bind(Workspace workspace)
        {
            Transform.Bind(workspace);
            Source.Bind(workspace);
        }

        public override void Unbind()
        {
            Transform.Unbind();
            Source.Unbind();
        }

        [OscCommand("/blob", "Usage: [$basePath]/blob, (int)id, (float)centerX, (float)centerY, (float)deltaX, (float)deltaY, (int)mergeCount")]
        private void Command_OnBlob(OscMessage message)
        {
            Connection?.Send("/usage", OscCommandManager.GetCommandHelpString(typeof(BlobBroadcaster), Name, "/blob"));
        }

        [OscCommand("/die", "Usage: [$basePath]/die, (int)id, (float)x, (float)y, (int)mergeCount")]
        private void Command_OnBlobDie(OscMessage message)
        {
            Connection?.Send("/usage", OscCommandManager.GetCommandHelpString(typeof(BlobBroadcaster), Name, "/die"));
        }

        [OscCommand("/merge", "Usage: [$basePath]/merge, (int)id1, (int)id2, (float)x, (float)y, (int)mergeCount1, (int)mergeCount2")]
        private void Command_OnBlobMerge(OscMessage message)
        {
            Connection?.Send("/usage", OscCommandManager.GetCommandHelpString(typeof(BlobBroadcaster), Name, "/merge"));
        }

        [OscCommand("/spawn", "Usage: [$basePath]/spawn, (int)id, (float)x, (float)y, (int)mergeCount")]
        private void Command_OnBlobSpawned(OscMessage message)
        {
            Connection?.Send("/usage", OscCommandManager.GetCommandHelpString(typeof(BlobBroadcaster), Name, "/spawn"));
        }

        [OscCommand("/split", "Usage: [$basePath]/split, (int)id, (float)x, (float)y, (int)mergeCount")]
        private void Command_OnBlobSplit(OscMessage message)
        {
            Connection?.Send("/usage", OscCommandManager.GetCommandHelpString(typeof(BlobBroadcaster), Name, "/split"));
        }

        private void OnBlobDie(Tracker sender, Data.Blob blob)
        {
            Vector3 center = new Vector3(blob.Center.X, blob.Center.Y, 0);

            Vector3.Transform(ref center, ref transform, out center);

            Connection?.Send(Name + "/die", blob.ID, center.X, center.Y, blob.MergeCount);
        }

        private void OnBlobMerge(Tracker sender, Data.Blob blobA, Data.Blob blobB)
        {
            Vector3 centerA = new Vector3(blobA.Center.X, blobA.Center.Y, 0);
            Vector3 centerB = new Vector3(blobB.Center.X, blobB.Center.Y, 0);

            Vector3.Transform(ref centerA, ref transform, out centerA);
            Vector3.Transform(ref centerB, ref transform, out centerB);

            Vector3 @event = (centerA + centerB) * 0.5f;

            Connection?.Send(Name + "/merge", blobA.ID, blobB.ID, @event.X, @event.Y, blobA.MergeCount, blobB.MergeCount);
        }

        private void OnBlobSpawned(Tracker sender, Data.Blob blob)
        {
            Vector3 center = new Vector3(blob.Center.X, blob.Center.Y, 0);

            Vector3.Transform(ref center, ref transform, out center);

            Connection?.Send(Name + "/spawn", blob.ID, center.X, center.Y, blob.MergeCount);
        }

        private void OnBlobSplit(Tracker sender, Data.Blob blobA, Data.Blob blobB)
        {
            Vector3 centerB = new Vector3(blobB.Center.X, blobB.Center.Y, 0);

            Vector3.Transform(ref centerB, ref transform, out centerB);

            Connection?.Send(Name + "/split", blobB.ID, centerB.X, centerB.Y, blobB.MergeCount);
        }

        private void OnBlobsProcessed(IBlobSource source)
        {
            ThreadName = System.Threading.Thread.CurrentThread.Name;

            ITransfromSource transfromSource;

            if (Transform.TryGet(out transfromSource) == true)
            {
                transform = transfromSource.Matrix;
            }
            else
            {
                transform = Matrix4.Identity;
            }

            foreach (Blob blob in source.Blobs)
            {
                Vector3 center = new Vector3(blob.Center.X, blob.Center.Y, 0);
                Vector3 lastCenter = new Vector3(blob.LastCenter.X, blob.LastCenter.Y, 0);

                Vector3.Transform(ref center, ref transform, out center);
                Vector3.Transform(ref lastCenter, ref transform, out lastCenter);

                Connection?.Send(Name + "/blob", blob.ID, center.X, center.Y, center.X - lastCenter.X, center.Y - lastCenter.Y, blob.MergeCount);
            }
        }

        private void Source_Bound(IOscObjectBinding obj)
        {
            Source.BoundTo.BlobsProcessed += OnBlobsProcessed;
            Source.BoundTo.BlobDie += OnBlobDie;
            Source.BoundTo.BlobMerge += OnBlobMerge;
            Source.BoundTo.BlobSpawned += OnBlobSpawned;
            Source.BoundTo.BlobSplit += OnBlobSplit;
        }

        private void Source_Unbound(IOscObjectBinding obj)
        {
            if (Source.IsBound == false)
            {
                return;
            }

            Source.BoundTo.BlobsProcessed -= OnBlobsProcessed;
            Source.BoundTo.BlobDie -= OnBlobDie;
            Source.BoundTo.BlobMerge -= OnBlobMerge;
            Source.BoundTo.BlobSpawned -= OnBlobSpawned;
            Source.BoundTo.BlobSplit -= OnBlobSplit;
        }

        private void Transform_Bound(IOscObjectBinding obj)
        {
            transform = Transform.BoundTo.Matrix;
        }

        private void Transform_Unbound(IOscObjectBinding obj)
        {

        }
    }
}