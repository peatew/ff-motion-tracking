﻿using System;
using System.Collections.Generic;
using System.Drawing;
using OpenTK;
using Rug.CV.AForgeCV;
using Rug.CV.Data;
using Rug.Host;
using Rug.Host.BaseClasses;
using Rug.Host.Commands;
using Rug.Host.Loading;

namespace Rug.CV.Custom
{
    [XmlName("Custom.BlobTracker")]
    public class BlobTracker : ObjectBase, IBlobSource, IDebugImageSource
    {
        Bitmap debugBitmap;

        public Tracker Tracker = new Tracker();
        private Graphics graphics;

        List<DebugEvent> debugEvents = new List<DebugEvent>(); 

        struct DebugEvent
        {
            public Vector2 Center;
            public int Type; 
        }

        public List<Blob> Blobs { get; private set; } = new List<Blob>(); 

        public event BlobSourceEvent BlobsProcessed;

        public event DebugFrameEvent NewDebugFrame;

        public event BlobEvent BlobDie;
        public event MultiBlobEvent BlobMerge;
        public event BlobEvent BlobSpawned;
        public event MultiBlobEvent BlobSplit;

        [OscMember] public ObjectBinding<IListSource<BlobShape>> Source { get; private set; }

        public bool DebuggerAttached { get; set; } 

        public BlobTracker()
        {
            Source = new ObjectBinding<IListSource<BlobShape>>(this, nameof(Source), "/" + nameof(Source).ToLower());

            Source.Bound += Source_Bound;
            Source.Unbound += Source_Unbound;

            Tracker.BlobDieFrames = 5;
            Tracker.BlobInflateSize = 0.6f; //0.5f; // 1.05f;
            Tracker.BlobSpawnDistance = 0.01f;

            Tracker.BlobDie += Tracker_BlobDie;
            Tracker.BlobMerge += Tracker_BlobMerge;
            Tracker.BlobSpawned += Tracker_BlobSpawned;
            Tracker.BlobSplit += Tracker_BlobSplit;
        }

        private void Tracker_BlobSplit(Tracker sender, Blob blobA, Blob blobB)
        {
            debugEvents.Add(new DebugEvent()
            {
                Center = blobB.Center,
                Type = 0
            });

            BlobSplit?.Invoke(sender, blobA, blobB);

        //debugEvents.Add(new DebugEvent()
        //{
        //    Center = (blobA.Center + blobB.Center) * 0.5f, 
        //    Type = 0
        //}); 

            //Connection?.Send(Name + "/split", blobB.ID, blobB.Center.X, blobB.Center.Y);
        }

        private void Tracker_BlobSpawned(Tracker sender, Blob blob)
        {
            debugEvents.Add(new DebugEvent()
            {
                Center = blob.Center,
                Type = 1
            });

            BlobSpawned?.Invoke(sender, blob);
        }

        private void Tracker_BlobMerge(Tracker sender, Blob blobA, Blob blobB)
        {         
            DebugEvent @event = new DebugEvent()
            {
                Center = (blobA.Center + blobB.Center) * 0.5f,
                Type = 2
            };

            debugEvents.Add(@event);

            BlobMerge?.Invoke(sender, blobA, blobB);
        }

        private void Tracker_BlobDie(Tracker sender, Blob blob)
        {
            debugEvents.Add(new DebugEvent()
            {
                Center = blob.Center,
                Type = 3
            });

            BlobDie?.Invoke(sender, blob);
        }

        private void Source_Bound(IOscObjectBinding obj)
        {
            Source.BoundTo.ListUpdated += BoundTo_ListUpdated;
        }

        private void Source_Unbound(IOscObjectBinding obj)
        {
            if (Source.IsBound == false)
            {
                return; 
            }

            Source.BoundTo.ListUpdated -= BoundTo_ListUpdated;
        }

        private void BoundTo_ListUpdated(object sender, EventArgs e)
        {
            if (Source.IsBound == false)
            {
                return;
            }

            ThreadName = System.Threading.Thread.CurrentThread.Name;

            IListSource<BlobShape> listSource;

            if (Source.TryGet(out listSource) == false) 
            {
                return; 
            }

            List<BlobShape> blobShapes = listSource.List;

            List<RectangleF> blobRegions = new List<RectangleF>(); 

            foreach (BlobShape shape in blobShapes)
            {
                blobRegions.Add(shape.Bounds); 
            }

            debugEvents.Clear();

            Tracker.Process(blobRegions);

            graphics.ResetTransform();

            graphics.Clear(Color.Black);

            //graphics.ScaleTransform((float)debugBitmap.Width / (float)bitmap.Width, (float)debugBitmap.Height / (float)bitmap.Height);
            //graphics.ScaleTransform((float)debugBitmap.Width, (float)debugBitmap.Height);
            graphics.TranslateTransform((float)debugBitmap.Width * 0.5f, (float)debugBitmap.Height * 0.5f); 

            float scaleX = (float)debugBitmap.Width * 0.5f;
            float scaleY = (float)debugBitmap.Height * 0.5f;

            Blobs.Clear();
            Blobs.AddRange(Tracker.Blobs);

            List<PointF> points = new List<PointF>();

            foreach (BlobShape blob in blobShapes)
            {
                //graphics.DrawRectangle(Pens.Cyan, Rectangle.Truncate(blob.Bounds));
                //graphics.DrawRectangle(Pens.Yellow, Rectangle.Truncate(blob.DetectionBounds));
                foreach (Vector2 vec in blob.Points)
                {
                    points.Add(new PointF(vec.X * scaleX, vec.Y * scaleY)); 
                }

                graphics.FillPolygon(Brushes.DarkMagenta, points.ToArray());

                points.Clear(); 
            }

            foreach (Blob blob in Tracker.Blobs)
            {
                //graphics.DrawRectangle(Pens.Cyan, Rectangle.Truncate(blob.Bounds));
                //graphics.DrawRectangle(Pens.Yellow, Rectangle.Truncate(blob.DetectionBounds));

                graphics.DrawRectangle(Pens.Cyan, blob.Bounds.X * scaleX, blob.Bounds.Y * scaleY, blob.Bounds.Width * scaleX, blob.Bounds.Height * scaleY);
                graphics.DrawRectangle(Pens.Yellow, blob.DetectionBounds.X * scaleX, blob.DetectionBounds.Y * scaleY, blob.DetectionBounds.Width * scaleX, blob.DetectionBounds.Height * scaleY);

                graphics.DrawLine(Pens.Magenta, blob.LastCenter.X * scaleX, blob.LastCenter.Y * scaleY, blob.Center.X * scaleX, blob.Center.Y * scaleY);
            }

            foreach (RectangleF blobRegion in blobRegions)
            {
                graphics.DrawRectangle(Pens.DarkMagenta, blobRegion.X * scaleX, blobRegion.Y * scaleY, blobRegion.Width * scaleX, blobRegion.Height * scaleY);
            }

            foreach (DebugEvent debugEvent in debugEvents)
            {
                Brush brush; 
                switch (debugEvent.Type)
                {
                    case 0:
                        brush = Brushes.Red;
                        break;
                    case 1:
                        brush = Brushes.Green;
                        break;
                    case 2:
                        brush = Brushes.Blue;
                        break;
                    case 3:
                        brush = Brushes.White;
                        break;
                    default:
                        continue; 
                }

                graphics.FillEllipse(brush,
                    (debugEvent.Center.X * scaleX) - 20,
                    (debugEvent.Center.Y * scaleY) - 20,
                    40, 40); 
            }

            NewDebugFrame?.Invoke(this, debugBitmap);

            BlobsProcessed?.Invoke(this); 
        }

        public override void Bind(Workspace workspace)
        {
            Source.Bind(workspace);

            debugBitmap = new Bitmap(640, 480, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            graphics = Graphics.FromImage(debugBitmap);
        }

        public override void Unbind()
        {
            Source.Unbind();

            graphics?.Dispose();
            debugBitmap?.Dispose();
        }
    }
}
