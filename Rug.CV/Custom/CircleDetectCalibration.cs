﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using AForge;
using AForge.Imaging;
using AForge.Imaging.Filters;
using AForge.Math.Geometry;
using OpenTK;
using Rug.Host;
using Rug.Host.BaseClasses;
using Rug.Host.Commands;
using Rug.Host.Loading;

namespace Rug.CV.Custom
{
    [XmlName("Custom.Calibration.Circle")]
    public class CircleDetectCalibration : ObjectBase, IDebugImageSource, ITransfromSource
    {
        Bitmap debugBitmap;
        Graphics graphics; 

        public event DebugFrameEvent NewDebugFrame;

        [OscMember] public ObjectBinding<IImageSource> Source { get; private set; }

        [OscMember]
        public OscString File { get; private set; } 

        public bool DebuggerAttached { get; set; }

        public bool HasCalibration { get; private set; }

        [OscMember]
        public OscVector2 Center { get; private set; }

        [OscMember]
        public OscFloat Radius { get; private set; }

        [OscMember]
        public OscFloat Rotation { get; private set; }  

        public Matrix4 Matrix { get; private set; } = Matrix4.Identity; 

        public CircleDetectCalibration()
        {
            File = new OscString(this, nameof(File), "/" + nameof(File).ToLower(), string.Empty); 
            Source = new ObjectBinding<IImageSource>(this, nameof(Source), "/" + nameof(Source).ToLower());

            Center = new OscVector2(this, nameof(Center), "/" + nameof(Center).ToLower(), Vector2.Zero);
            Radius = new OscFloat(this, nameof(Radius), "/" + nameof(Radius).ToLower(), 0);
            Rotation = new OscFloat(this, nameof(Rotation), "/" + nameof(Rotation).ToLower(), 0);

            Source.Bound += Source_Bound;
            Source.Unbound += Source_Unbound;
        }

        public override void Bind(Workspace workspace)
        {
            debugBitmap = new Bitmap(640, 480, PixelFormat.Format32bppArgb);
            graphics = Graphics.FromImage(debugBitmap); 

            Source.Bind(workspace);

            if (string.IsNullOrEmpty(File.Value) == false)
            {
                using (Bitmap bitmap = (Bitmap)Bitmap.FromFile(Helper.ResolvePath(File.Value)))
                {
                    CalibrateFromImage(bitmap); 
                }
            }
        }

        public override void Unbind()
        {
            Source.Unbind();

            graphics?.Dispose();
            debugBitmap?.Dispose(); 
        }

        private void OnNewFrame(IImageSource source, DateTime timestamp, System.Drawing.Bitmap bitmap)
        {
            ThreadName = System.Threading.Thread.CurrentThread.Name;

            if (Enabled.Value == false)
            {
                return;
            }

            NewDebugFrame?.Invoke(this, debugBitmap); 
        }

        private void CalibrateFromImage(Bitmap bitmap)
        {
            // lock image
            BitmapData bitmapData = bitmap.LockBits(
                new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadWrite, bitmap.PixelFormat);

            //Threshold filter = new Threshold(100);

            //filter.ApplyInPlace(bitmapData);

            //// step 1 - turn background to black
            ColorFiltering colorFilter = new ColorFiltering();

            colorFilter.Red = new IntRange(0, 64);
            colorFilter.Green = new IntRange(0, 64);
            colorFilter.Blue = new IntRange(0, 64);
            colorFilter.FillOutsideRange = false;

            colorFilter.ApplyInPlace(bitmapData);

            // step 2 - locating objects
            BlobCounter blobCounter = new BlobCounter();

            blobCounter.FilterBlobs = true;
            blobCounter.MinHeight = 100;
            blobCounter.MinWidth = 100;

            blobCounter.ProcessImage(bitmapData);
            Blob[] blobs = blobCounter.GetObjectsInformation();
            bitmap.UnlockBits(bitmapData);

            // step 3 - check objects' type and highlight
            SimpleShapeChecker shapeChecker = new SimpleShapeChecker();

            Pen yellowPen = new Pen(Color.Yellow, 2); // circles
            Pen redPen = new Pen(Color.Red, 2);       // quadrilateral
            Pen brownPen = new Pen(Color.Brown, 2);   // quadrilateral with known sub-type
            Pen greenPen = new Pen(Color.Green, 2);   // known triangle
            Pen bluePen = new Pen(Color.Blue, 2);     // triangle

            float scaleX = (float)debugBitmap.Width / (float)bitmap.Width;
            float scaleY = (float)debugBitmap.Height / (float)bitmap.Height;

            graphics.Clear(Color.Black); 

            for (int i = 0, n = blobs.Length; i < n; i++)
            {
                List<IntPoint> edgePoints = blobCounter.GetBlobsEdgePoints(blobs[i]);

                AForge.Point center;
                float radius;

                // is circle ?
                if (shapeChecker.IsCircle(edgePoints, out center, out radius))
                {
                    //Rectangle bounds = blobs[i].Rectangle;

                    //Vector2 trueCenter = new Vector2((float)bounds.Width * 0.5f + bounds.X, (float)bounds.Height * 0.5f + bounds.Y);
                    //Vector2 size = new Vector2(((float)bounds.Width / (float)bitmap.Width) * 0.5f, ((float)bounds.Height / (float)bitmap.Height) * 0.5f);

                    //Center.Value = new Vector2((trueCenter.X / (float)bitmap.Width) * 2f - 1f, (trueCenter.Y / (float)bitmap.Height) * 2f - 1f);
                    //Radius.Value = radius / ((float)bitmap.Width * 0.5f);

                    //Matrix4 matrix =
                    //    Matrix4.CreateRotationZ((Rotation.Value / 360) * MathHelper.TwoPi) * 
                    //    Matrix4.CreateTranslation(new Vector3(-Center.Value.X, -Center.Value.Y, 0)) *
                    //    Matrix4.CreateScale(1f / size.X, 1f / size.Y, 1f); 

                    Center.Value = new Vector2((center.X / (float)bitmap.Width) * 2f - 1f, (center.Y / (float)bitmap.Height) * 2f - 1f);
                    Radius.Value = radius / ((float)bitmap.Width * 0.5f);

                    Matrix4 matrix =
                        Matrix4.CreateRotationZ((Rotation.Value / 360) * MathHelper.TwoPi) *
                        Matrix4.CreateTranslation(new Vector3(-Center.Value.X, -Center.Value.Y, 0)) *
                        Matrix4.CreateScale(1f / Radius.Value);

                    Matrix = matrix;

                    graphics.DrawEllipse(yellowPen,
                        (float)(center.X - radius) * scaleX, (float)(center.Y - radius) * scaleY,
                        (float)(radius * 2) * scaleX, (float)(radius * 2) * scaleY);
                   
                    HasCalibration = true; 
                }
                else
                {
                    List<IntPoint> corners;

                    // is triangle or quadrilateral
                    if (shapeChecker.IsConvexPolygon(edgePoints, out corners))
                    {
                        // get sub-type
                        PolygonSubType subType = shapeChecker.CheckPolygonSubType(corners);

                        Pen pen;

                        if (subType == PolygonSubType.Unknown)
                        {
                            pen = (corners.Count == 4) ? redPen : bluePen;
                        }
                        else
                        {
                            pen = (corners.Count == 4) ? brownPen : greenPen;
                        }

                        graphics.DrawPolygon(pen, ToPointsArray(corners, scaleX, scaleY));
                    }
                }
            }

            yellowPen.Dispose();
            redPen.Dispose();
            greenPen.Dispose();
            bluePen.Dispose();
            brownPen.Dispose();
        }

        // Conver list of AForge.NET's points to array of .NET points
        private System.Drawing.Point[] ToPointsArray(List<IntPoint> points, float scaleX, float scaleY)
        {
            System.Drawing.Point[] array = new System.Drawing.Point[points.Count];

            for (int i = 0, n = points.Count; i < n; i++)
            {
                array[i] = new System.Drawing.Point((int)((float)points[i].X * scaleX), (int)((float)points[i].Y * scaleY));
            }

            return array;
        }

        private void Source_Bound(IOscObjectBinding obj)
        {
            Source.BoundTo.NewFrame += OnNewFrame;
        }

        private void Source_Unbound(IOscObjectBinding obj)
        {
            if (Source.IsBound == false)
            {
                return;
            }

            Source.BoundTo.NewFrame -= OnNewFrame;
        }
    }
}