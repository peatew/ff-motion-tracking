﻿using System;
using System.Drawing.Imaging;
using System.IO;
using Rug.Host;
using Rug.Host.BaseClasses;
using Rug.Host.Commands;
using Rug.Host.Loading;
using Rug.Osc;

namespace Rug.CV.Misc.Debug
{
    [XmlName("Misc.Debug.VideoRecorder")]
    internal class VideoRecorder : ObjectBase
    {
        private bool recording = false;

        [OscMember]
        public OscString Destination { get; private set; }

        [OscMember]
        public ObjectBinding<IImageSource> Source { get; private set; }

        public VideoRecorder()
        {
            Destination = new OscString(this, nameof(Destination), "/" + nameof(Destination).ToLower(), string.Empty);
            Source = new ObjectBinding<IImageSource>(this, nameof(Source), "/" + nameof(Source).ToLower());

            Source.Bound += ImageSource_Bound;
            Source.Unbound += ImageSource_Unbound;
        }

        public override void Bind(Workspace workspace)
        {
            Source.Bind(workspace);
        }

        public override void Unbind()
        {
            Source.Unbind();
        }

        [OscCommand("/start", "Usage: [$basePath]/start")]
        private void Command_Start(OscMessage message)
        {
            recording = true;

            Connection?.Send(Name + "/start");
        }

        [OscCommand("/stop", "Usage: [$basePath]/stop")]
        private void Command_Stop(OscMessage message)
        {
            recording = false;

            Connection?.Send(Name + "/stop");
        }

        private void ImageSource_Bound(IOscObjectBinding obj)
        {
            Source.BoundTo.NewFrame += ImageSource_NewFrame;
        }

        private void ImageSource_NewFrame(IImageSource source, DateTime timestamp, System.Drawing.Bitmap bitmap)
        {
            ThreadName = System.Threading.Thread.CurrentThread.Name;

            if (recording == false)
            {
                return;
            }

            string path = Helper.ResolvePath(Path.Combine(Destination.Value, timestamp.Ticks.ToString() + ".png"));

            Helper.EnsurePathExists(path);

            bitmap.Save(path, ImageFormat.Png);
        }

        private void ImageSource_Unbound(IOscObjectBinding obj)
        {
            if (Source.IsBound == false)
            {
                return;
            }

            Source.BoundTo.NewFrame -= ImageSource_NewFrame;
        }
    }
}