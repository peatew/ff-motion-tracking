﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using OpenTK;
using Rug.Host;
using Rug.Host.BaseClasses;
using Rug.Host.Commands;
using Rug.Host.Loading;

namespace Rug.CV.Misc.Debug
{
    [XmlName("Misc.Debug.TransformDebugger")]
    public class TransformDebugger : ObjectBase, IDebugImageSource
    {
        Pen[] pens = new Pen[]
        {
            Pens.Cyan, 
            Pens.Yellow, 
            Pens.Magenta, 
            Pens.White, 
        };  

        private Bitmap debugBitmap;
        private Graphics graphics;

        [OscMember]
        public ObjectBinding<IPulse> Pulse { get; private set; }

        [OscMember]
        public OscObjectList<TransformSource> Transforms { get; private set; }

        public bool DebuggerAttached { get; set; } 

        public event DebugFrameEvent NewDebugFrame;

        public TransformDebugger()
        {
            Pulse = new ObjectBinding<IPulse>(this, nameof(Pulse), nameof(Pulse).ToLower());
            Pulse.Bound += Pulse_Bound;
            Pulse.Unbound += Pulse_Unbound;

            Transforms = new OscObjectList<TransformSource>(this, nameof(Transforms), nameof(Transforms).ToLower());

            debugBitmap = new Bitmap(512, 512, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            graphics = Graphics.FromImage(debugBitmap); 
        }

        private void Pulse_Unbound(IOscObjectBinding obj)
        {
            if (Pulse.IsBound == false)
            {
                return;
            }

            Pulse.BoundTo.Pulse -= OnPulse;
        }

        private void Pulse_Bound(IOscObjectBinding obj)
        {
            Pulse.BoundTo.Pulse += OnPulse;
        }

        private void OnPulse(object sender, EventArgs e)
        {
            ThreadName = System.Threading.Thread.CurrentThread.Name;

            if (NewDebugFrame == null)
            {
                return; 
            }

            if (Enabled.Value == false)
            {
                return; 
            }

            graphics.Clear(Color.Black);

            int penIndex = 0; 

            foreach (TransformSource transformSource in Transforms)
            {
                if (transformSource.Source.IsBound == false)
                {
                    continue; 
                }

                Vector3 pt1 = new Vector3(-1, -1, 0);
                Vector3 pt2 = new Vector3( 1, -1, 0);
                Vector3 pt3 = new Vector3( 1,  1, 0);
                Vector3 pt4 = new Vector3(-1,  1, 0);

                Matrix4 transform = transformSource.Source.BoundTo.Matrix;

                pt1 = Vector3.Transform(pt1, transform);
                pt2 = Vector3.Transform(pt2, transform);
                pt3 = Vector3.Transform(pt3, transform);
                pt4 = Vector3.Transform(pt4, transform);

                PointF[] points = new PointF[]
                    {
                        ToPointF(pt1),
                        ToPointF(pt2),
                        ToPointF(pt3),
                        ToPointF(pt4),
                    };

                Pen pen = pens[(penIndex++) % pens.Length]; 

                graphics.DrawPolygon(pen, points);

                PointF corner = ToPointF(pt1);

                graphics.DrawRectangle(pen, corner.X - 10, corner.Y - 10, 20, 20); 
            }

            NewDebugFrame?.Invoke(this, debugBitmap);
        }

        private PointF ToPointF(Vector3 point)
        {
            Vector2 transformed = point.Xy;

            transformed *= 0.5f;
            transformed += new Vector2(0.5f, 0.5f);
            transformed *= (float)debugBitmap.Width;

            return new PointF(transformed.X, transformed.Y); 
        }

        public override void Bind(Workspace workspace)
        {
            Pulse.Bind(workspace);
            Transforms.Bind(workspace); 
        }

        public override void Unbind()
        {
            Pulse.Unbind();
            Transforms.Unbind();
        }

        [XmlName("TransformDebugger.TransformSource")]
        public class TransformSource : ObjectBase
        {
            [OscMember]
            public ObjectBinding<ITransfromSource> Source { get; private set; }

            public TransformSource()
            {
                Source = new ObjectBinding<ITransfromSource>(this, nameof(Source), nameof(Source).ToLower());
            }

            public override void Bind(Workspace workspace)
            {
                Source.Bind(workspace); 
            }

            public override void Unbind()
            {
                Source.Unbind();
            }
        }
    }
}
