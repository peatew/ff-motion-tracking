﻿using System;
using System.Threading;
using System.Xml;
using AForge.Video;
using AForge.Video.DirectShow;
using Rug.CV.BaseClasses;
using Rug.Host;
using Rug.Host.Loading;

namespace Rug.CV.Misc
{
    [XmlName("Misc.VideoFileSource")]
    internal class VideoFileSource : ImageSourceBase, IDebugImageSource
    {
        private FileVideoSource videoSource = null;

        public bool DebuggerAttached { get; set; }

        public string FilePath { get; set; }

        public event DebugFrameEvent NewDebugFrame;

        public override event ImageSourceFrameEvent NewFrame;

        public override void Detach()
        {
            base.Detach();
        }

        public override void Bind(Workspace service)
        {
            videoSource = new FileVideoSource(FilePath);
            
            videoSource.NewFrame += VideoSource_NewFrame;

            videoSource.Start();
        }

        public override void Load(LoadContext context, XmlNode node)
        {
            base.Load(context, node);

            FilePath = Helper.GetAttributeValue(node, "FilePath", string.Empty);
        }

        public override XmlElement Save(LoadContext context, XmlElement element)
        {
            base.Save(context, element);

            Helper.AppendAttributeAndValue(element, "FilePath", FilePath);

            return element;
        }
        
        private void VideoSource_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            ThreadName = System.Threading.Thread.CurrentThread.Name;

            NewDebugFrame?.Invoke(this, eventArgs.Frame);
            NewFrame?.Invoke(this, DateTime.Now, eventArgs.Frame);
        }

        public override void Unbind()
        {
            if (videoSource != null)
            {
                // stop current video source
                videoSource.SignalToStop();

                // wait 2 seconds until camera stops
                for (int i = 0; (i < 50) && (videoSource.IsRunning); i++)
                {
                    Thread.Sleep(100);
                }

                if (videoSource.IsRunning)
                {
                    videoSource.Stop();
                }
            }
        }
    }
}