﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using Rug.CV.BaseClasses;
using Rug.Host;
using Rug.Host.Commands;
using Rug.Host.Loading;

namespace Rug.CV.ImageSources
{
    [XmlName("Misc.DirectorySource")]
    public class DirectoryImageSource : ImageSourceBase, IDebugImageSource
    {
        private List<ImageFile> files = new List<ImageFile>();
        private int index = 0;
        private bool readFiles = false;

        public bool DebuggerAttached { get; set; }

        [OscMember]
        public OscString Path { get; private set; }

        [OscMember] public ObjectBinding<ITimingSource> Sync { get; private set; }

        public event DebugFrameEvent NewDebugFrame;

        public override event ImageSourceFrameEvent NewFrame;

        public DirectoryImageSource()
        {
            Path = new OscString(this, nameof(Path), "/" + nameof(Path).ToLower(), string.Empty); 
            Sync = new ObjectBinding<ITimingSource>(this, nameof(Sync), "/" + nameof(Sync).ToLower());

            Sync.Bound += Sync_Bound;
            Sync.Unbound += Sync_Unbound;
        }

        private void Sync_Unbound(IOscObjectBinding obj)
        {
            if (Sync.IsBound == false)
            {
                return; 
            }

            Sync.BoundTo.Pulse -= SyncSource_Pulse;
        }

        private void Sync_Bound(IOscObjectBinding obj)
        {
            Sync.BoundTo.Pulse += SyncSource_Pulse;
        }

        public override void Unbind()
        {
            Sync.Unbind(); 
        }

        public override void Bind(Workspace workspace)
        {
            Sync.Bind(workspace); 

            if (Directory.Exists(Helper.ResolvePath(Path.Value)) == false)
            {
                return;
            }
            
            foreach (string file in Directory.GetFiles(Helper.ResolvePath(Path.Value)))
            {
                ImageFile image;

                if (ImageFile.TryParseFile(file, out image) == false)
                {
                    continue;
                }

                files.Add(image);
            }

            if (files.Count == 0)
            {
                return;
            }

            files.Sort();

            index = 0;

            ITimingSource source; 

            if (Sync.TryGet(out source) == true)
            {            
                source.StartTime = new DateTime(Math.Min(source.StartTime.Ticks, files[0].Timestamp.Ticks - 1000));
                source.Reset();
            }

            readFiles = true; 
        }

        private void SyncSource_Pulse(object sender, EventArgs e)
        {
            ThreadName = System.Threading.Thread.CurrentThread.Name;

            if (readFiles == false || 
                Sync.IsBound == false)
            {
                return; 
            }

            ITimingSource syncSource = Sync.BoundTo;

            if (syncSource == null)
            {
                return;
            }

            if (syncSource.Time < files[index].Timestamp)
            {
                if (syncSource.Time <= files[0].Timestamp) 
                {
                    index = 0; 
                }

                return;
            }

            while (syncSource.Time > files[index].Timestamp)
            {
                if (Sync.IsBound == false)
                {
                    return;
                }

                index = (index + 1) % files.Count;

                if (index == 0)
                {
                    // syncSource.Reset(); 
                }  
            }

            using (Bitmap bitmap = (Bitmap)Bitmap.FromFile(files[index].FilePath))
            {
                NewDebugFrame?.Invoke(this, bitmap);
                NewFrame?.Invoke(this, DateTime.Now, bitmap);

                Connection?.Send(Name + "/frame");
            }

            index = (index + 1) % files.Count;

            if (index == 0)
            {
                // syncSource.Reset();
            }
        }

        private struct ImageFile : IComparable<ImageFile>
        {
            public string FileName;
            public string FilePath;
            public int Index;
            public DateTime Timestamp;

            public static bool TryParseFile(string filePath, out ImageFile imageFile)
            {
                imageFile = default(ImageFile);

                FileInfo file = new FileInfo(filePath);

                string name = file.Name;
                
                name = name.Substring(0, name.Length - file.Extension.Length);

                string fileName = name;

                //int index;

                //if (int.TryParse(name, out index) == false)
                //{
                //    return false;
                //}

                //imageFile = new ImageFile() { FilePath = filePath, Index = index, Timestamp = file.CreationTime };

                long ticks;

                if (long.TryParse(name, out ticks) == false)
                {
                    return false;
                }

                imageFile = new ImageFile() { FilePath = filePath, FileName = fileName, Index = 0, Timestamp = new DateTime(ticks) };

                return true;
            }

            public int CompareTo(ImageFile other)
            {
                return FileName.CompareTo(other.FileName); // (int)(Timestamp.Ticks - other.Timestamp.Ticks);
            }
        }
    }
}