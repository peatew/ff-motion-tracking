﻿using System;
using System.Linq;
using System.Threading;
using AForge.Video;
using AForge.Video.DirectShow;
using Rug.CV.BaseClasses;
using Rug.Host;
using Rug.Host.Commands;
using Rug.Host.Loading;

namespace Rug.CV.Misc
{
    [XmlName("Misc.LiveVideoSource")]
    internal class LiveVideoSource : ImageSourceBase, IDebugImageSource
    {
        private VideoCaptureDevice videoSource = null;

        public bool DebuggerAttached { get; set; }

        [OscMember]
        public OscString DeviceName { get; private set; }

        public event DebugFrameEvent NewDebugFrame;

        public override event ImageSourceFrameEvent NewFrame;

        public LiveVideoSource()
        {
            DeviceName = new OscString(this, nameof(DeviceName), "/" + nameof(DeviceName).ToLower(), string.Empty);
        }

        public override void Detach()
        {
            base.Detach();

            if (videoSource != null)
            {
                // stop current video source
                videoSource.SignalToStop();

                // wait 2 seconds until camera stops
                for (int i = 0; (i < 50) && (videoSource.IsRunning); i++)
                {
                    Thread.Sleep(100);
                }

                if (videoSource.IsRunning)
                {
                    videoSource.Stop();
                }
            }
        }

        public override void Bind(Workspace service)
        {
            string monikerString = TryToFindDevice();

            if (monikerString == null)
            {
                return;
            }

            // create video source
            videoSource = new VideoCaptureDevice(monikerString);

            // set frame size
            videoSource.VideoResolution = SelectResolution(videoSource);

            videoSource.NewFrame += VideoSource_NewFrame;

            videoSource.Start();
        }

        private static VideoCapabilities SelectResolution(VideoCaptureDevice device)
        {
            foreach (var cap in device.VideoCapabilities)
            {
                if (cap.FrameSize.Height == 240)
                {
                    return cap;
                }

                if (cap.FrameSize.Width == 320)
                {
                    return cap;
                }
            }

            return device.VideoCapabilities.Last();
        }

        private string TryToFindDevice()
        {
            // show device list
            try
            {
                // enumerate video devices
                FilterInfoCollection videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);

                if (videoDevices.Count == 0)
                {
                    return null;
                }

                // add all devices to combo
                foreach (FilterInfo device in videoDevices)
                {
                    if (DeviceName.Value == device.Name)
                    {
                        return device.MonikerString;
                    }
                }
            }
            catch
            {
            }

            return null;
        }

        private void VideoSource_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            ThreadName = System.Threading.Thread.CurrentThread.Name;

            NewDebugFrame?.Invoke(this, eventArgs.Frame);
            NewFrame?.Invoke(this, DateTime.Now, eventArgs.Frame);
        }

        public override void Unbind()
        {

        }
    }
}