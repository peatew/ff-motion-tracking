# Future Fest Motion Tracking Platform #

This tracking platform transforms image information acquired from camera sources into OSC features in realtime. 

** Table of Contents ** 

[TOC]

## Before You Begin

You will need the Point Grey drivers and Fly Capture SDK from the [Point Grey website](https://www.ptgrey.com/flycapture-sdk). An account is required to the access the downloads section.

I believe that the cameras bought for the FF project were Chameleon but the Fly Capture SDK is probably identical for all camera models. 

After installing the Fly Capture SDK and **rebooting your machine** it should be possible to build the code or run the **FFTracking UI.exe** application. 

## FF Motion Tracking Application

### Running the FF Motion Tracking Application

A compiled version of the FF Tracking application is available in the [downloads section](https://bitbucket.org/peatew/ff-motion-tracking/downloads/) of this repository.

* Download and install the Fly Capture SDK detailed above. 
* **Reboot machine** to allow Fly Capture drives and paths to be loaded.
* [Download FF Tracking UI Binaries](https://bitbucket.org/peatew/ff-motion-tracking/downloads/) or compile from sources.
* Unzip to a suitable location. 
* Run the **FFTracking UI.exe**

### Server Output Tab

This tab displays the OSC messages being sent and received by the tracking server. In addition any errors or exceptions encountered by the server will be displayed in this tab. 

![picture](docs/server-output.png)

### OSC Terminal Tab

This tab is your terminal to talk to the server. Notice there is a text input box at the bottom of the tab. It will accept input in the form of OSC messages in Rug.Osc format, see the [Rug.Osc documentation](https://bitbucket.org/rugcode/rug.osc/wiki/Parsing%20messages%20and%20bundles%20from%20strings) for more details on OSC strings.

![picture](docs/terminal.png)


For example sending a **/hello** message will cause the server to print all current values and the usage for all messages that you can send to the server. 

```
#!OSC
TX 0.0.0.0:0 /hello
RX 10.0.75.1:61534 /hello, "Image Feature Service", "1.0.0.0"
RX 10.0.75.1:61534 /pulse
RX 10.0.75.1:61534 /pulse/interval, 100
RX 10.0.75.1:61534 /pulse/enabled, True
RX 10.0.75.1:61534 /pulse/bounds, 260f, 110f, 160f, 30f
RX 10.0.75.1:61534 /transform/stage
RX 10.0.75.1:61534 /transform/stage/eulerangles, 0f, 45f, 0f
RX 10.0.75.1:61534 /transform/stage/translation, -0.5f, 0f, 0f
RX 10.0.75.1:61534 /transform/stage/scale, 0.35f, 0.7f, 1f
RX 10.0.75.1:61534 /transform/stage/enabled, True
RX 10.0.75.1:61534 /transform/stage/bounds, -90f, 320f, 160f, 30f
RX 10.0.75.1:61534 /transform/wide
RX 10.0.75.1:61534 /transform/wide/eulerangles, 0f, 90f, 0f
RX 10.0.75.1:61534 /transform/wide/translation, 0.35f, 0f, 0f
RX 10.0.75.1:61534 /transform/wide/scale, 0.35f, 1f, 1f
RX 10.0.75.1:61534 /transform/wide/enabled, True
RX 10.0.75.1:61534 /transform/wide/bounds, -90f, 220f, 160f, 30f
RX 10.0.75.1:61534 /transform/debugger
RX 10.0.75.1:61534 /transform/debuggerpulse, "/pulse", True
RX 10.0.75.1:61534 /transform/debuggertransforms, 2
RX 10.0.75.1:61534 /transform/debugger/enabled, False
RX 10.0.75.1:61534 /transform/debugger/bounds, 0f, 0f, 160f, 40f
RX 10.0.75.1:61534 /cameras/wide
RX 10.0.75.1:61534 /cameras/wide/enabled, True
RX 10.0.75.1:61534 /cameras/wide/bounds, -90f, 180f, 160f, 30f
RX 10.0.75.1:61534 /usage, "Usage: /cameras/wide/modelName"
RX 10.0.75.1:61534 /usage, "Usage: /cameras/wide/info"
RX 10.0.75.1:61534 /usage, "Usage: /cameras/wide/sensorInfo"
RX 10.0.75.1:61534 /usage, "Usage: /cameras/wide/sensorResolution"
RX 10.0.75.1:61534 /usage, "Usage: /cameras/wide/serialNumber, s"
RX 10.0.75.1:61534 /usage, "Usage: /cameras/wide/vendorName"
RX 10.0.75.1:61534 /cameras/stage
RX 10.0.75.1:61534 /cameras/stage/enabled, True
RX 10.0.75.1:61534 /cameras/stage/bounds, -90f, 280f, 160f, 30f
RX 10.0.75.1:61534 /usage, "Usage: /cameras/stage/modelName"
RX 10.0.75.1:61534 /usage, "Usage: /cameras/stage/info"
RX 10.0.75.1:61534 /usage, "Usage: /cameras/stage/sensorInfo"
RX 10.0.75.1:61534 /usage, "Usage: /cameras/stage/sensorResolution"
RX 10.0.75.1:61534 /usage, "Usage: /cameras/stage/serialNumber, s"
RX 10.0.75.1:61534 /usage, "Usage: /cameras/stage/vendorName"
RX 10.0.75.1:61534 /feature/motion/wide
RX 10.0.75.1:61534 /feature/motion/wide/framesperbackgroundupdate, 1
RX 10.0.75.1:61534 /feature/motion/wide/millisecondsperbackgroundupdate, 10
RX 10.0.75.1:61534 /feature/motion/wide/source, "/cameras/wide", True
RX 10.0.75.1:61534 /feature/motion/wide/enabled, True
RX 10.0.75.1:61534 /feature/motion/wide/bounds, 250f, 480f, 160f, 30f
RX 10.0.75.1:61534 /feature/motion/stage
RX 10.0.75.1:61534 /feature/motion/stage/framesperbackgroundupdate, 1
RX 10.0.75.1:61534 /feature/motion/stage/millisecondsperbackgroundupdate, 10
RX 10.0.75.1:61534 /feature/motion/stage/source, "/cameras/stage", True
RX 10.0.75.1:61534 /feature/motion/stage/enabled, True
RX 10.0.75.1:61534 /feature/motion/stage/bounds, 250f, 520f, 160f, 30f
RX 10.0.75.1:61534 /feature/presence/wide
RX 10.0.75.1:61534 /feature/presence/wide/framesperbackgroundupdate, 20
RX 10.0.75.1:61534 /feature/presence/wide/millisecondsperbackgroundupdate, 1000
RX 10.0.75.1:61534 /feature/presence/wide/source, "/cameras/wide", True
RX 10.0.75.1:61534 /feature/presence/wide/enabled, True
RX 10.0.75.1:61534 /feature/presence/wide/bounds, 250f, 390f, 160f, 30f
RX 10.0.75.1:61534 /feature/presence/stage
RX 10.0.75.1:61534 /feature/presence/stage/framesperbackgroundupdate, 20
RX 10.0.75.1:61534 /feature/presence/stage/millisecondsperbackgroundupdate, 1000
RX 10.0.75.1:61534 /feature/presence/stage/source, "/cameras/stage", True
RX 10.0.75.1:61534 /feature/presence/stage/enabled, True
RX 10.0.75.1:61534 /feature/presence/stage/bounds, 250f, 430f, 160f, 30f
RX 10.0.75.1:61534 /blobsdetect/wide
RX 10.0.75.1:61534 /blobsdetect/wide/source, "/cameras/wide", True
RX 10.0.75.1:61534 /blobsdetect/wide/enabled, True
RX 10.0.75.1:61534 /blobsdetect/wide/bounds, 260f, 30f, 160f, 30f
RX 10.0.75.1:61534 /blobsdetect/stage
RX 10.0.75.1:61534 /blobsdetect/stage/source, "/cameras/stage", True
RX 10.0.75.1:61534 /blobsdetect/stage/enabled, True
RX 10.0.75.1:61534 /blobsdetect/stage/bounds, 260f, 70f, 160f, 30f
RX 10.0.75.1:61534 /calibrate/circle
RX 10.0.75.1:61534 /calibrate/circle/source, "/cameras/wide", True
RX 10.0.75.1:61534 /calibrate/circle/file, "~/Wide Stage Calibrate.png"
RX 10.0.75.1:61534 /calibrate/circle/center, 0.005468726f, 0.2770833f
RX 10.0.75.1:61534 /calibrate/circle/radius, 0.5019531f
RX 10.0.75.1:61534 /calibrate/circle/rotation, 90f
RX 10.0.75.1:61534 /calibrate/circle/enabled, True
RX 10.0.75.1:61534 /calibrate/circle/bounds, 820f, 120f, 160f, 30f
RX 10.0.75.1:61534 /consolidator
RX 10.0.75.1:61534 /consolidator/blobsources, 2
RX 10.0.75.1:61534 /consolidator/pulse, "/pulse", True
RX 10.0.75.1:61534 /consolidator/enabled, True
RX 10.0.75.1:61534 /consolidator/bounds, 610f, 10f, 160f, 102f
RX 10.0.75.1:61534 /tracker
RX 10.0.75.1:61534 /tracker/source, "/consolidator", True
RX 10.0.75.1:61534 /tracker/enabled, True
RX 10.0.75.1:61534 /tracker/bounds, 820f, 10f, 160f, 30f
RX 10.0.75.1:61534 /blobs/stage
RX 10.0.75.1:61534 /blobs/stage/source, "/tracker", True
RX 10.0.75.1:61534 /blobs/stage/transform, "/calibrate/circle", True
RX 10.0.75.1:61534 /blobs/stage/enabled, True
RX 10.0.75.1:61534 /blobs/stage/bounds, 1060f, 100f, 160f, 30f
RX 10.0.75.1:61534 /usage, "Usage: /blobs/stage/blob, (int)id, (float)centerX, (float)centerY, (float)deltaX, (float)deltaY, (int)mergeCount"
RX 10.0.75.1:61534 /usage, "Usage: /blobs/stage/die, (int)id, (float)x, (float)y, (int)mergeCount"
RX 10.0.75.1:61534 /usage, "Usage: /blobs/stage/merge, (int)id1, (int)id2, (float)x, (float)y, (int)mergeCount1, (int)mergeCount2"
RX 10.0.75.1:61534 /usage, "Usage: /blobs/stage/spawn, (int)id, (float)x, (float)y, (int)mergeCount"
RX 10.0.75.1:61534 /usage, "Usage: /blobs/stage/split, (int)id, (float)x, (float)y, (int)mergeCount"
RX 10.0.75.1:61534 /blobs
RX 10.0.75.1:61534 /blobs/source, "/tracker", True
RX 10.0.75.1:61534 /blobs/transform, "", True
RX 10.0.75.1:61534 /blobs/enabled, True
RX 10.0.75.1:61534 /blobs/bounds, 1060f, 50f, 160f, 30f
RX 10.0.75.1:61534 /usage, "Usage: /blobs/blob, (int)id, (float)centerX, (float)centerY, (float)deltaX, (float)deltaY, (int)mergeCount"
RX 10.0.75.1:61534 /usage, "Usage: /blobs/die, (int)id, (float)x, (float)y, (int)mergeCount"
RX 10.0.75.1:61534 /usage, "Usage: /blobs/merge, (int)id1, (int)id2, (float)x, (float)y, (int)mergeCount1, (int)mergeCount2"
RX 10.0.75.1:61534 /usage, "Usage: /blobs/spawn, (int)id, (float)x, (float)y, (int)mergeCount"
RX 10.0.75.1:61534 /usage, "Usage: /blobs/split, (int)id, (float)x, (float)y, (int)mergeCount"
RX 10.0.75.1:61534 /attention
RX 10.0.75.1:61534 /attention/pulse, "/pulse", True
RX 10.0.75.1:61534 /attention/datasources, 2
RX 10.0.75.1:61534 /attention/zones, 4
RX 10.0.75.1:61534 /attention/enabled, True
RX 10.0.75.1:61534 /attention/bounds, 1050f, 420f, 160f, 242f
RX 10.0.75.1:61534 /usage, "Usage: /attention/reset"
RX 10.0.75.1:61534 /attention/stats
RX 10.0.75.1:61534 /attention/stats/pulse, "/pulse", True
RX 10.0.75.1:61534 /attention/stats/datasources, 2
RX 10.0.75.1:61534 /attention/stats/enabled, True
RX 10.0.75.1:61534 /attention/stats/bounds, 790f, 750f, 160f, 102f
RX 10.0.75.1:61534 /presence
RX 10.0.75.1:61534 /presence/pulse, "/pulse", True
RX 10.0.75.1:61534 /presence/datasources, 2
RX 10.0.75.1:61534 /presence/zones, 4
RX 10.0.75.1:61534 /presence/enabled, True
RX 10.0.75.1:61534 /presence/bounds, 1050f, 160f, 160f, 242f
RX 10.0.75.1:61534 /usage, "Usage: /presence/reset"
RX 10.0.75.1:61534 /presence/stats
RX 10.0.75.1:61534 /presence/stats/pulse, "/pulse", True
RX 10.0.75.1:61534 /presence/stats/datasources, 2
RX 10.0.75.1:61534 /presence/stats/enabled, True
RX 10.0.75.1:61534 /presence/stats/bounds, 790f, 630f, 160f, 102f
```

### OSC Filter Tab

The filter tab allows you to easily mute or solo OSC messages via their address strings.  

![picture](docs/filter-tab.png)


### Graph Viewer / Editor

The graph view tab lets you examine the structure of the running configuration. 

![picture](docs/graph-editor.png)


## Creating Content For The FF Tracking System

There is a example/base Unity application is the folder '**FF Tracking Example Unity Content**' see '**/FF Tracking Example Unity Content/Assets/README.txt**' for more details on creating content that can use the data created by the FF Tracking System. 


## Building The FF Motion Tracking Application Code 

Open up the FFTracking UI solution located at '/FFTracking/FFTracking.sln'. 

This is the main solution for the tracking system. All the libraries and sources should be ready for you to compile and run. 


## Configuring The System For A New Space

Configuration of the system should be pretty simple. It should only involve modifying the nodes that govern the transformation from _Camera Space_ to _Blob Space_.  

* _Camera Space_ is a unit box comprising of a single cameras entier image.
* _Blob Space_ is a unit box that should contain the all camera images after they have been transformed. 

 
```
#!XML 
<Misc.Transform Name="/transform/stage" EulerAngles="0,45,0" Translation="-0.5,0,0" Scale="0.35,0.7,1" Enabled="True" Bounds="-90,320,160,30" />
```

Where: 

* Euler Angles is the rotation of the camera in _Blob Space_. 
* Translation is the offset applied to the camera origin in _Blob Space_. 
* Scale is the scalling applied to get from _Camera Space_ to _Blob Space_. 

** Pro Tip: Use the TransformDebugger to see what you are doing ** 

The aim is to make a single _Blob Space_ where the X axis roughly correlates to a position along the projection surface. This is why the *stage* camera transfom is rotatated by 45° for the FF2016 event.


** Additional calibration for downward floor projection ** 

The downward floor or _Stage_ projection callibration uses an image to define the unit space that blobs should be transformed into. Simply replace the image file with one captured from the instilation. Do not use any IR filters and project a circle on the floor. The following node will detect the circle and create the nessisary transform automatically. 

```
#!XML 
<Custom.Calibration.Circle Name="/calibrate/circle" Source="/cameras/wide" File="~/Wide Stage Calibrate.png" Center="0.005468726,0.2770833" Radius="0.5019531" Rotation="90" Enabled="True" Bounds="820,120,160,30" />
```

### This is the listing for XML configuration file used at the Future Fest event 2016

```
#!XML 
<Server>
  <Objects>
    <!-- Pulse defines the rate at which OSC messages are broadcast -->
    <Misc.Pulse Name="/pulse" Interval="100" Enabled="True" Bounds="260,110,160,30" />

    <!-- These trasforms are used to conform the camera images into a single imaging space -->
    <Misc.Transform Name="/transform/stage" EulerAngles="0,45,0" Translation="-0.5,0,0" Scale="0.35,0.7,1" Enabled="True" Bounds="-90,320,160,30" />
    <Misc.Transform Name="/transform/wide" EulerAngles="0,90,0" Translation="0.35,0,0" Scale="0.35,1,1" Enabled="True" Bounds="-90,220,160,30" />

    <!-- Transform debugger, Change Enabled="True" and open 'Image Debuggers -> /transform/debugger' window to observe the transfoms in action -->
    <Misc.Debug.TransformDebugger Name="/transform/debugger" Pulse="/pulse" Enabled="False" >
      <TransformDebugger.TransformSource Name="/transform/debugger/wide" Source="/transform/wide" />
      <TransformDebugger.TransformSource Name="/transform/debugger/stage" Source="/transform/stage" />
    </Misc.Debug.TransformDebugger>

    <!-- Camera sources explicitly mapped to there serial numbers -->
    <PointGrey.Camera SerialNumber="15331850" Name="/cameras/wide" Enabled="True" Bounds="-90,180,160,30" />
    <PointGrey.Camera SerialNumber="15331860" Name="/cameras/stage" Enabled="True" Bounds="-90,280,160,30" />

    <!-- Motion detectors at various octaves of movement -->
    <AForge.MotionDetection Name="/feature/motion/wide" FramesPerBackgroundUpdate="1" MillisecondsPerBackgroundUpdate="10" Source="/cameras/wide" Enabled="True" Bounds="250,480,160,30" />
    <AForge.MotionDetection Name="/feature/motion/stage" FramesPerBackgroundUpdate="1" MillisecondsPerBackgroundUpdate="10" Source="/cameras/stage" Enabled="True" Bounds="250,520,160,30" />
    <AForge.MotionDetection Name="/feature/presence/wide" FramesPerBackgroundUpdate="20" MillisecondsPerBackgroundUpdate="1000" Source="/cameras/wide" Enabled="True" Bounds="250,390,160,30" />
    <AForge.MotionDetection Name="/feature/presence/stage" FramesPerBackgroundUpdate="20" MillisecondsPerBackgroundUpdate="1000" Source="/cameras/stage" Enabled="True" Bounds="250,430,160,30" />

    <!-- Blob detectors for each camera -->
    <AForge.BlobDetection.Fine Name="/blobsdetect/wide" Source="/cameras/wide" Enabled="True" Bounds="260,30,160,30" />
    <AForge.BlobDetection.Fine Name="/blobsdetect/stage" Source="/cameras/stage" Enabled="True" Bounds="260,70,160,30" />

    <!-- Floor circle calibration, from image File="~/Wide Stage Calibrate.png" this is a single frame captured from the camera above the stage -->
    <Custom.Calibration.Circle Name="/calibrate/circle" Source="/cameras/wide" File="~/Wide Stage Calibrate.png" Center="0.005468726,0.2770833" Radius="0.5019531" Rotation="90" Enabled="True" Bounds="820,120,160,30" />

    <!-- Merges blobs from many sources into a single blob space -->
    <Custom.BlobConsolidator Name="/consolidator" Pulse="/pulse" Enabled="True" Bounds="610,10,160,102">
      <BlobConsolidator.BlobSource Name="/consolidator/wide" Source="/blobsdetect/wide" Transform="/transform/wide" Enabled="True" Bounds="0,0,160,40" />
      <BlobConsolidator.BlobSource Name="/consolidator/stage" Source="/blobsdetect/stage" Transform="/transform/stage" Enabled="True" Bounds="0,0,160,40" />
    </Custom.BlobConsolidator>

    <!-- Tracks blobs between frames -->
    <Custom.BlobTracker Name="/tracker" Source="/consolidator" Enabled="True" Bounds="820,10,160,30" />

    <!-- Broadcast blobs via OSC, /blobs/stage is transformed by the circle stage calibration image -->
    <Custom.Blobs.Broadcaster Name="/blobs/stage" Source="/tracker" Transform="/calibrate/circle" Enabled="True" Bounds="1060,100,160,30" />
    <!-- Total blob space broadcast via OSC -->
    <Custom.Blobs.Broadcaster Name="/blobs" Source="/tracker" Enabled="True" Bounds="1060,50,160,30" />

    <!-- Tracks 'Attention', this is an attempt to create some sort of transient area of interest -->
    <Custom.AttentionZone Name="/attention" Pulse="/pulse" Enabled="True" Bounds="1050,420,160,242">
      <AttentionZone.DataSource Name="/attention/wide" Source="/feature/motion/wide" Transform="/transform/wide" Enabled="True" Bounds="0,0,160,40" />
      <AttentionZone.DataSource Name="/attention/stage" Source="/feature/motion/stage" Transform="/transform/stage" Enabled="True" Bounds="0,0,160,40" />
      <AttentionZone.Zone Name="/attention/zone/0" NeutralPosition="-0.5,-0.5" NeutralAttraction="0.1" Position="-0.5,-0.5" Radius="0.2" Attraction="2" Velocity="0,0" Dampening="0.2" Enabled="True" Bounds="0,0,160,40" />
      <AttentionZone.Zone Name="/attention/zone/1" NeutralPosition="0.5,-0.5" NeutralAttraction="0.1" Position="0.0,-0.5" Radius="0.2" Attraction="2" Velocity="0,0" Dampening="0.2" Enabled="True" Bounds="0,0,160,40" />
      <AttentionZone.Zone Name="/attention/zone/2" NeutralPosition="-0.5,0.5" NeutralAttraction="0.1" Position="-0.3,0.5" Radius="0.2" Attraction="2" Velocity="0,0" Dampening="0.2" Enabled="True" Bounds="0,0,160,40" />
      <AttentionZone.Zone Name="/attention/zone/3" NeutralPosition="0.5,0.5" NeutralAttraction="0.1" Position="0.3,0.5" Radius="0.2" Attraction="2" Velocity="0,0" Dampening="0.2" Enabled="True" Bounds="0,0,160,40" />
    </Custom.AttentionZone>

    <!-- Broadcasts statistics about 'Attention' -->
    <Custom.MovementMetrics Name="/attention/stats" Pulse="/pulse" Enabled="True" Bounds="790,750,160,102">
      <MovementMetrics.DataSource Name="/attention/stats/wide" Source="/feature/motion/wide" Enabled="True" Bounds="0,0,160,40" />
      <MovementMetrics.DataSource Name="/attention/stats/stage" Source="/feature/motion/stage" Enabled="True" Bounds="0,0,160,40" />
    </Custom.MovementMetrics>

    <!-- Tracks 'Presence', this is an attempt to create some indicator of presence within the space -->
    <Custom.AttentionZone Name="/presence" Pulse="/pulse" Enabled="True" Bounds="1050,160,160,242">
      <AttentionZone.DataSource Name="/presence/wide" Source="/feature/presence/wide" Transform="/transform/wide" Enabled="True" Bounds="0,0,160,40" />
      <AttentionZone.DataSource Name="/presence/stage" Source="/feature/presence/stage" Transform="/transform/stage" Enabled="True" Bounds="0,0,160,40" />
      <AttentionZone.Zone Name="/presence/zone/0" NeutralPosition="-0.5,-0.5" NeutralAttraction="0.2" Position="-0.6,-0.6" Radius="0.2" Attraction="2" Velocity="0,0" Dampening="0.4" Enabled="True" Bounds="0,0,160,40" />
      <AttentionZone.Zone Name="/presence/zone/1" NeutralPosition="0.5,-0.5" NeutralAttraction="0.2" Position="0.6,-0.4" Radius="0.2" Attraction="2" Velocity="0,0" Dampening="0.4" Enabled="True" Bounds="0,0,160,40" />
      <AttentionZone.Zone Name="/presence/zone/2" NeutralPosition="-0.5,0.5" NeutralAttraction="0.2" Position="-0.6,0.4" Radius="0.2" Attraction="2" Velocity="0,0" Dampening="0.4" Enabled="True" Bounds="0,0,160,40" />
      <AttentionZone.Zone Name="/presence/zone/3" NeutralPosition="0.5,0.5" NeutralAttraction="0.2" Position="0.6,0.6" Radius="0.2" Attraction="2" Velocity="0,0" Dampening="0.4" Enabled="True" Bounds="0,0,160,40" />
    </Custom.AttentionZone>

    <!-- Broadcasts statistics about 'Presence' -->
    <Custom.MovementMetrics Name="/presence/stats" Pulse="/pulse" Enabled="True" Bounds="790,630,160,102">
      <MovementMetrics.DataSource Name="/presence/stats/wide" Source="/feature/presence/wide" Enabled="True" Bounds="0,0,160,40" />
      <MovementMetrics.DataSource Name="/presence/stats/stage" Source="/feature/presence/stage" Enabled="True" Bounds="0,0,160,40" />
    </Custom.MovementMetrics>
  </Objects>
</Server>
```


### Attention / Presence Outputs 

```
#!OSC

/attention/stats/sum, (float)value
/attention/stats/min, (float)value
/attention/stats/max, (float)value
/attention/stats/mean, (float)value

/presence/stats/sum, (float)value
/presence/stats/min, (float)value
/presence/stats/max, (float)value
/presence/stats/mean, (float)value

/attention/zone/0/position, (float)x, (float)y
/attention/zone/1/position, (float)x, (float)y
/attention/zone/2/position, (float)x, (float)y
/attention/zone/3/position, (float)x, (float)y

/presence/zone/0/position, (float)x, (float)y
/presence/zone/1/position, (float)x, (float)y
/presence/zone/2/position, (float)x, (float)y
/presence/zone/3/position, (float)x, (float)y
```


### Blob Detection Outputs 

** These blobs are for the downward projection ** 
```
#!OSC
/blobs/stage/blob, (int)id, (float)centerX, (float)centerY, (float)deltaX, (float)deltaY, (int)mergeCount
/blobs/stage/die, (int)id, (float)x, (float)y, (int)mergeCount
/blobs/stage/merge, (int)id1, (int)id2, (float)x, (float)y, (int)mergeCount1, (int)mergeCount2
/blobs/stage/spawn, (int)id, (float)x, (float)y, (int)mergeCount
/blobs/stage/split, (int)id, (float)x, (float)y, (int)mergeCount
```

** These blobs are for the total blobs space ** 
```
#!OSC
/blobs/blob, (int)id, (float)centerX, (float)centerY, (float)deltaX, (float)deltaY, (int)mergeCount
/blobs/die, (int)id, (float)x, (float)y, (int)mergeCount
/blobs/merge, (int)id1, (int)id2, (float)x, (float)y, (int)mergeCount1, (int)mergeCount2
/blobs/spawn, (int)id, (float)x, (float)y, (int)mergeCount
/blobs/split, (int)id, (float)x, (float)y, (int)mergeCount
```


## Licence 

Copyright (C) 2016 Interactive Scientific LTD (info@interactivescientific.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.


THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.